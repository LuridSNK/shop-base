﻿namespace ShopBase.DesktopServices.Infrastructure
{
    using System.Windows;
    
    public class DisplayOptions
    {
        public Point Resolution { get; set; }

        public WindowState State { get; set; }
    }
}
