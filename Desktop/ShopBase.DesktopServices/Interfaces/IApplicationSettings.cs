﻿namespace ShopBase.DesktopServices.Interfaces
{
    using System.Configuration;
    using ShopBase.DesktopServices.Infrastructure;
    using ShopBase.Messaging.Dto;

    public interface IApplicationSettings
    {
        [UserScopedSetting]
        DisplayOptions DisplayOptions { get; set; }

        [UserScopedSetting]
        UserDto CurrentUser { get; set; }

        [UserScopedSetting]
        string LastUsedViewModel { get; set; }
    }
}