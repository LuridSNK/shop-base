﻿namespace ShopBase.DesktopServices.Interfaces
{
    public interface IPrinter
    {
        void Print(string pathToTheDocument, string printerName = null, short copies = 1);
    }
}