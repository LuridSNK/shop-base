﻿namespace ShopBase.DesktopServices.Impl
{
    using System.Diagnostics;
    using System.IO;
    using PdfiumViewer;
    using ShopBase.DesktopServices.Interfaces;

    public class DesktopPrinter : IPrinter
    {
        public void Print(string pathToTheDocument, string printerName = null, short copies = 1)
        {
            var ext = Path.GetExtension(pathToTheDocument);

            if (ext == ".pdf")
            {
                using (var pdfDoc = PdfDocument.Load(pathToTheDocument))
                {
                    using (var printDoc = pdfDoc.CreatePrintDocument())
                    {
                        if (!string.IsNullOrEmpty(printerName))
                        {
                            printDoc.PrinterSettings.PrinterName = printerName;
                        }

                        printDoc.PrinterSettings.Copies = copies;
                        printDoc.Print();
                    }
                }
            }

            if (ext == ".doc" || ext == ".docx")
            {
                // print doc
                Debug.WriteLine($"Tried to print {ext}");
            }

        }
    }
}
