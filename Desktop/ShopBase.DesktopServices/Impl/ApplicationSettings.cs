﻿namespace ShopBase.DesktopServices.Impl
{
    using System.Configuration;
    using ShopBase.DesktopServices.Infrastructure;
    using ShopBase.DesktopServices.Interfaces;
    using ShopBase.Messaging.Dto;

    public class ApplicationSettings : ApplicationSettingsBase, IApplicationSettings
    {

        public static IApplicationSettings Instance { get; } = new ApplicationSettings();

        [UserScopedSetting]
        public DisplayOptions DisplayOptions
        {
            get => (DisplayOptions)this[nameof(this.DisplayOptions)];
            set => this[nameof(this.DisplayOptions)] = value;
        }

        [UserScopedSetting]
        public UserDto CurrentUser
        {
            get => (UserDto)this[nameof(this.CurrentUser)];
            set => this[nameof(this.CurrentUser)] = value;
        }

        [UserScopedSetting]
        public string LastUsedViewModel
        {
            get => (string)this[nameof(this.LastUsedViewModel)];
            set => this[nameof(this.LastUsedViewModel)] = value;
        }
        
        // AN EXAMPLE!
        //[UserScopedSetting()]
        //[DefaultSettingValueAttribute("LightGray")]
        //public Color FormBackColor
        //{
        //    get { return (Color)this["FormBackColor"]; }
        //    set { this["FormBackColor"] = value; }
        //}
    }
}