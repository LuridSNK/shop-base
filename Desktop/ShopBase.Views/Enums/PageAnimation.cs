﻿namespace ShopBase.Views.Enums
{
    public enum PageAnimation
    {
        None,
        FadeIn,
        FadeOut
    }
}
