﻿namespace ShopBase.Views.Controls
{
    using System;
    using System.Windows;
    using System.Windows.Controls;
    using System.Windows.Controls.Primitives;

    /// <summary>
    /// Interaction logic for SystemBarControl.xaml
    /// </summary>
    public partial class SystemBarControl : UserControl
    {
        public static readonly DependencyProperty UsernameProperty = DependencyProperty.Register(
            nameof(Username),
            typeof(string),
            typeof(UserControl),
            new FrameworkPropertyMetadata(
                string.Empty,
                FrameworkPropertyMetadataOptions.AffectsMeasure
                | FrameworkPropertyMetadataOptions.AffectsRender /*,new PropertyChangedCallback(OnTextChanged)*/));


        public static readonly DependencyProperty HamburgerProperty = DependencyProperty.Register(
            nameof(Hamburger),
            typeof(ToggleButton),
            typeof(UserControl));

        public SystemBarControl()
        {
            InitializeComponent();
            Hamburger = HamburgerToggleButton;
        }
        

        public string Username
        {
            get => (string)GetValue(UsernameProperty);
            set => SetValue(UsernameProperty, value);
        }


        public ToggleButton Hamburger
        {
            get => (ToggleButton)GetValue(HamburgerProperty);
            set => SetValue(HamburgerProperty, value);
        }

        //private static void OnTextChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        //{
        //    if (e.OldValue != e.NewValue)
        //    {
        //        d.SetValue(e.Property, e.NewValue);
        //    }
        //}

        private void Minimize_OnClick(object sender, RoutedEventArgs e)
        {
            var parentWindow = Window.GetWindow(this);
            if (parentWindow != null)
            {
                parentWindow.WindowState = WindowState.Minimized;
            }
        }
        
        private void Maximize_OnClick(object sender, RoutedEventArgs e)
        {
            var parentWindow = Window.GetWindow(this);
            if (parentWindow != null)
            {
                parentWindow.WindowState ^= WindowState.Maximized;
            }
        }

        private void Close_OnClick(object sender, RoutedEventArgs e)
        {
            Window.GetWindow(this)?.Close();
        }
    }
}
