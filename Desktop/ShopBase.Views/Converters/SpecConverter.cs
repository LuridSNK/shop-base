﻿namespace ShopBase.Views.Converters
{
    using System;
    using System.Globalization;
    using System.Windows.Data;
    using ShopBase.Shared.Extensions;

    public class SpecConverter : IValueConverter
    {
        object IValueConverter.Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            Enum myEnum = (Enum)value;
            string description = myEnum.GetSpecAttribute();
            return description;
        }

        object IValueConverter.ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
