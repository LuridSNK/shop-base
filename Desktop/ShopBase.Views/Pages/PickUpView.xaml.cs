﻿namespace ShopBase.Views.Pages
{
    using ShopBase.Views.Extensions;
    using System.Windows.Controls;

    /// <summary>
    /// Interaction logic for PickUpView.xaml
    /// </summary>
    public partial class PickUpView : UserControl
    {
        public PickUpView()
        {
            this.InitializeComponent();
            this.Loaded += PickUpView_Loaded;
        }

        private async void PickUpView_Loaded(object sender, System.Windows.RoutedEventArgs e)
        {
            await this.SlideFromTop(.25f);
        }
    }
}
