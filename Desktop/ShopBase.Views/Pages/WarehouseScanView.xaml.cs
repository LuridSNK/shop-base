﻿using ShopBase.Views.Extensions;

namespace ShopBase.Views.Pages
{
    using System;
    using System.Windows;
    using System.Windows.Controls;
    using System.Windows.Input;

    /// <summary>
    /// Interaction logic for WarehouseScanView.xaml
    /// </summary>
    public partial class WarehouseScanView : UserControl
    {
        public WarehouseScanView()
        {
            this.InitializeComponent();
            this.FocusOnTextBox();
            Loaded += WarehouseScanView_Loaded;
            BarCodeTextBox.TextChanged += BarCodeTextBox_TextChanged;
            PreviewKeyDown += View_PreviewKeyDown;
            PrintButton.Click += Button_Click;
            ShipButton.Click += Button_Click;
            ClearInputButton.Click += Button_Click;
        }

        private void BarCodeTextBox_TextChanged(object sender, TextChangedEventArgs e)
        {
            var tb = BarCodeTextBox;
            if (tb.Text == string.Empty)
                BarCodeTextBox?.Dispatcher.Invoke(() => BarCodeTextBox.Focus());
        }

        private async void WarehouseScanView_Loaded(object sender, RoutedEventArgs e)
        {
            await this.SlideFromTop(.25f);
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            this.FocusOnTextBox();
        }

        private void View_PreviewKeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Escape)
            {
                BarCodeTextBox.Text = string.Empty;
                FocusOnTextBox();
            }
        }

        private void FocusOnTextBox()
        {
            BarCodeTextBox.Dispatcher.BeginInvoke(new Action(() => BarCodeTextBox.Focus()));
        }
    }
}
