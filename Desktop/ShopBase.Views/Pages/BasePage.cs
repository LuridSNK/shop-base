﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using ShopBase.Views.Enums;
using ShopBase.Views.Extensions;

namespace ShopBase.Views.Pages
{
    public class BasePage : Page
    {
        public PageAnimation PageLoadAnimation => PageAnimation.FadeIn;

        public PageAnimation PageUnloadAnimation =>  PageAnimation.FadeOut;

        public float AnimationSeconds { get; set; } = 0.3f;




        public BasePage()
        {
            if (PageLoadAnimation == PageAnimation.None)
                Visibility = Visibility.Collapsed;

            Loaded += BasePage_Loaded;
        }

        private async void BasePage_Loaded(object sender, System.Windows.RoutedEventArgs e)
        {
            await AnimateIn();
        }
        

        private async Task AnimateIn()
        {
           // Debug.WriteLine("AnimatingIn");

            if (PageLoadAnimation != PageAnimation.FadeIn) return;
            await this.SlideFromDrawerAndFadeIn(AnimationSeconds);
        }

       
    }
}
