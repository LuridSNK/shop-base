/*
  In App.xaml:
  <Application.Resources>
      <vm:ViewModelLocator xmlns:vm="clr-namespace:ShopBase.Desktop"
                           x:Key="Locator" />
  </Application.Resources>
  
  In the View:
  DataContext="{Binding Source={StaticResource Locator}, Path=ViewModelName}"

  You can also use Blend to do all this with the tool's support.
  See http://www.galasoft.ch/mvvm
*/

namespace ShopBase.Views
{
    using CommonServiceLocator;
    using GalaSoft.MvvmLight.Ioc;
    using ShopBase.DesktopServices.Impl;
    using ShopBase.DesktopServices.Interfaces;
    using ShopBase.External.Handlers.Impl;
    using ShopBase.External.Handlers.Interfaces;
    using ShopBase.Messaging.Http.Interfaces;
    using ShopBase.Messaging.Http.Stubs;
    using ShopBase.Services.PdfGeneration.Impl;
    using ShopBase.Services.PdfGeneration.Interfaces;
    using ShopBase.ViewModels.Impl;
    using ShopBase.ViewModels.Interfaces;

    /// <summary>
    /// This class contains static references to all the view models in the
    /// application and provides an entry point for the bindings.
    /// </summary>
    public class ViewModelLocator
    {
        /// <summary>
        /// Initializes a new instance of the ViewModelLocator class.
        /// </summary>
        public ViewModelLocator()
        {
            ServiceLocator.SetLocatorProvider(() => SimpleIoc.Default);

            //if (ViewModelBase.IsInDesignModeStatic)
            //{
            //    // Create design time view services and models
            //    SimpleIoc.Default.Register<IDataService, DesignDataService>();
            //}
            //else
            //{
            //    // Create run time view services and models
            //    SimpleIoc.Default.Register<IDataService, DataService>();
            //}


            // windows
            SimpleIoc.Default.Register<IMainViewModel, MainViewModel>();

            //pages
            SimpleIoc.Default.Register<ILoginViewModel, LoginViewModel>();

            SimpleIoc.Default.Register<ICatalogViewModel, CatalogViewModel>();
            SimpleIoc.Default.Register<IClientsViewModel, ClientsViewModel>();
            SimpleIoc.Default.Register<IOrdersViewModel, OrdersViewModel>();
            SimpleIoc.Default.Register<IPurchasesViewModel, PurchasesViewModel>();
            SimpleIoc.Default.Register<IWarehouseViewModel, WarehouseViewModel>();
            SimpleIoc.Default.Register<ILogisticsViewModel, LogisticsViewModel>();
            SimpleIoc.Default.Register<IRefundsViewModel, RefundsViewModel>();
            SimpleIoc.Default.Register<ILogsViewModel, LogsViewModel>();
            

            // warehouse
            SimpleIoc.Default.Register<IPickUpViewModel, PickUpViewModel>();
            SimpleIoc.Default.Register<IWarehouseScanViewModel, WarehouseScanViewModel>();

            //services
            SimpleIoc.Default.Register<IDeliveryServiceHandlerFactory, DeliveryServiceHandlerFactory>();
#if !DEBUG
            SimpleIoc.Default.Register<IApplicationApiHandler, ApplicationApiHandlerStub>();
#else
            SimpleIoc.Default.Register<IApplicationApiHandler, ApplicationApiHandler>();

#endif
            SimpleIoc.Default.Register<IPrinter, DesktopPrinter>();
            SimpleIoc.Default.Register<IPdfGenerator, DefaultPdfGenerator>();

            //CQRS commands

        }



        public ILoginViewModel LoginViewModel => ServiceLocator.Current.GetInstance<ILoginViewModel>();

        public IMainViewModel MainViewModel => ServiceLocator.Current.GetInstance<IMainViewModel>();

        public ICatalogViewModel CatalogViewModel => ServiceLocator.Current.GetInstance<ICatalogViewModel>();
        
        public IPickUpViewModel PickUpViewModel => ServiceLocator.Current.GetInstance<IPickUpViewModel>();

        public IWarehouseViewModel WarehouseViewModel => ServiceLocator.Current.GetInstance<IWarehouseViewModel>();

        public IWarehouseScanViewModel WarehouseScanViewModel => ServiceLocator.Current.GetInstance<IWarehouseScanViewModel>();

        public static void Cleanup()
        {
            // TODO Clear the ViewModels
        }
        
    }
}