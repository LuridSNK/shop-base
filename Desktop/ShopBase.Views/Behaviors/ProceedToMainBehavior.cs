﻿namespace ShopBase.Views.Behaviors
{
    using System.Windows;
    using System.Windows.Interactivity;
    using GalaSoft.MvvmLight.Ioc;
    using ShopBase.ViewModels.Interfaces;

    public class ProceedToMainBehavior : Behavior<Window>
    {
        protected override void OnAttached()
        {
            AssociatedObject.IsEnabledChanged += IsEnabledChanged;
        }
        
        protected override void OnDetaching()
        {
            AssociatedObject.IsEnabledChanged -= IsEnabledChanged;
        }

        private void IsEnabledChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
            var currentWindow = Application.Current.MainWindow;
            if (currentWindow is LoginView)
            {
                var mainWindow = new MainView { DataContext = SimpleIoc.Default.GetInstance<IMainViewModel>() };
                Application.Current.MainWindow = mainWindow;
                currentWindow.Close();
                mainWindow.Show();
            }
        }
    }
}
