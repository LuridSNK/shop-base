﻿using System;
using System.Runtime.CompilerServices;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media.Animation;

namespace ShopBase.Views.Extensions
{




    internal static class StoryboardExtensions
    {
        internal static void AddSlideFromDrawer(this Storyboard sb, float seconds, double xOffset, double yOffset, float deceleration = 0.8f)
        {
            var animation = new ThicknessAnimation
            {
                Duration = new Duration(TimeSpan.FromSeconds(seconds)),
                From = new Thickness(-xOffset, yOffset / 3, xOffset, 0),
                To = new Thickness(0),
                DecelerationRatio = deceleration,
                AccelerationRatio = 0.999 - deceleration
            };

            Storyboard.SetTargetProperty(animation, new PropertyPath("Margin"));
            sb.Children.Add(animation);
            
        }

        internal static void AddSlideBackToDrawer(this Storyboard sb, float seconds, double xOffset, double yOffset, float deceleration = 0.2f)
        {
            var animation = new ThicknessAnimation
            {
                Duration = new Duration(TimeSpan.FromSeconds(seconds)),
                From = new Thickness(0),
                To = new Thickness(-xOffset, yOffset / 3, xOffset, 0),
                DecelerationRatio = deceleration,
                AccelerationRatio = 0.999 - deceleration
            };

            Storyboard.SetTargetProperty(animation, new PropertyPath("Margin"));
            sb.Children.Add(animation);

        }

        internal static void AddFadeIn(this Storyboard sb, float seconds)
        {
            var animation = new DoubleAnimation
            {
                Duration = new Duration(TimeSpan.FromSeconds(seconds)),
                From = 0,
                To = 1,
            };

            Storyboard.SetTargetProperty(animation, new PropertyPath("Opacity"));
            sb.Children.Add(animation);
        }

        internal static void AddFadeOut(this Storyboard sb, float seconds)
        {
            var animation = new DoubleAnimation
            {
                Duration = new Duration(TimeSpan.FromSeconds(seconds)),
                From = 1,
                To = 0,
            };

            Storyboard.SetTargetProperty(animation, new PropertyPath("Opacity"));
            sb.Children.Add(animation);
        }

        internal static void AddSlideFromBottom(this Storyboard sb, float seconds, double yOffset, float deceleration = 0.4f)
        {
            var animation = new ThicknessAnimation
            {
                Duration = new Duration(TimeSpan.FromSeconds(seconds)),
                From = new Thickness(0, yOffset, 0, 0),
                To = new Thickness(0),
                DecelerationRatio = deceleration,
                AccelerationRatio = 0.999 - deceleration
            };

            Storyboard.SetTargetProperty(animation, new PropertyPath("Margin"));
            sb.Children.Add(animation);
        }

        internal static void AddSlideFromTop(this Storyboard sb, float seconds, double yOffset, float deceleration = 0.4f)
        {
            var animation = new ThicknessAnimation
            {
                Duration = new Duration(TimeSpan.FromSeconds(seconds)),
                From = new Thickness(0, 0, 0, yOffset),
                To = new Thickness(0),
                DecelerationRatio = deceleration,
                AccelerationRatio = 0.999 - deceleration
            };

            Storyboard.SetTargetProperty(animation, new PropertyPath("Margin"));
            sb.Children.Add(animation);
        }
    }

    internal static class PageAnimationExtensions
    {
        internal static async Task SlideFromDrawerAndFadeIn(this Page p, float seconds)
        {
            var sb = new Storyboard();

            sb.AddSlideFromDrawer(seconds, p.WindowWidth, p.WindowHeight);
            sb.AddFadeIn(seconds);
            sb.Begin(p);

            p.Visibility = Visibility.Visible;
            await Task.Delay((int) seconds * 990);
        }
    }

    public static class ControlAnimationExtensions
    {
        internal static async Task FadeIn(this Control c, float seconds)
        {
            var sb = new Storyboard();
            sb.AddFadeIn(seconds);
            sb.Begin(c);
            await Task.Delay((int)seconds * 990);
        }

        internal static async Task FadeOut(this Control c, float seconds)
        {
            var sb = new Storyboard();
            sb.AddFadeOut(seconds);
            sb.Begin(c);
            await Task.Delay((int)seconds * 990);
        }

        internal static async Task SlideFromBottom(this Control c, float seconds)
        {
            var sb = new Storyboard();
            sb.AddSlideFromBottom(seconds, c.ActualHeight);
            sb.Begin(c);
            await Task.Delay((int)seconds * 990);
        }

        internal static async Task SlideFromTop(this Control c, float seconds)
        {
            var sb = new Storyboard();
            sb.AddSlideFromTop(seconds, c.ActualHeight);
            sb.Begin(c);
            await Task.Delay((int)seconds * 990);
        }
    }
}
