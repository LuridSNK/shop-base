﻿namespace ShopBase.Views
{
    using System;
    using System.Windows;
    using System.Windows.Controls;
    using System.Windows.Input;
    using GalaSoft.MvvmLight.Messaging;
    using Extensions;
    using ViewModels.Messages;

    /// <summary>
    /// Interaction logic for MainView.xaml
    /// </summary>
    public partial class MainView : Window
    {
        public MainView()
        {
            InitializeComponent();
            Visibility = Visibility.Collapsed;
            Loaded += MainView_Loaded;
            Messenger.Default.Register<SnackMsg>(this, SendToSnackBar);
        }

        private async void MainView_Loaded(object sender, RoutedEventArgs e)
        {
            Visibility = Visibility.Visible;
            await this.FadeIn(0.5f);
        }
        
        void SendToSnackBar(SnackMsg msg)
        {
            var message = msg.Message.Replace(Environment.NewLine, " ");
            MainSnackbar.Dispatcher.BeginInvoke(new Action(() => MainSnackbar.MessageQueue.Enqueue(message)));
        }

        private void ScrollViewer_OnPreviewMouseWheel(object sender, MouseWheelEventArgs e)
        {
            ScrollViewer scv = (ScrollViewer)sender;
            scv.ScrollToVerticalOffset(scv.VerticalOffset - (e.Delta / 2.5));
            e.Handled = true;
        }

        private void ButtonListBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            InnerToggleButton.IsChecked = false;
        }
    }
}
