﻿namespace ShopBase.Views.Validation
{
    using System.Globalization;
    using System.Windows.Controls;

    public class ValueNotEmptyValidationRule : ValidationRule
    {
        public override ValidationResult Validate(object value, CultureInfo cultureInfo)
        {
            return string.IsNullOrWhiteSpace((value ?? "").ToString())
                       ? new ValidationResult(false, "Поле не должно быть пустым")
                       : ValidationResult.ValidResult;
        }
    }
}