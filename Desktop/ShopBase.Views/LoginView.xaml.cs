﻿namespace ShopBase.Views
{
    using System.Windows;
    using GalaSoft.MvvmLight.Ioc;
    using GalaSoft.MvvmLight.Messaging;
    using ShopBase.ViewModels.Interfaces;
    using ShopBase.ViewModels.Messages;
    using ShopBase.Views.Extensions;

    public partial class LoginView : Window
    {
        public LoginView()
        {
            this.InitializeComponent();
            Loaded += LoginView_Loaded;
        }


        private async void LoginView_Loaded(object sender, RoutedEventArgs e)
        {
            Visibility = Visibility.Visible;
            await this.FadeIn(0.5f);
        }
        
        private void LoginWindow_OnLoaded(object sender, RoutedEventArgs e)
        {
            this.UnBox.Focus();
            this.UnBox.SelectAll();
            Messenger.Default.Register<string>(
                recipient: this,
                token: "error",
                action: s => ErrorSnackbar.Dispatcher.Invoke(() => ErrorSnackbar.MessageQueue.Enqueue(s)));
            Messenger.Default.Register<AuthMessage>(
                recipient: this,
                token: null,
                action: GoToMain);
        }

        private async void GoToMain(AuthMessage msg)
        {
            var main = new MainView { DataContext = SimpleIoc.Default.GetInstance<IMainViewModel>() };
            Application.Current.MainWindow = main;
            await this.FadeOut(0.8f);
            Visibility = Visibility.Collapsed;
            main.Show();
            this.Close();
        }

        private void CloseButton_OnClick(object sender, RoutedEventArgs e)
        {
            this.Close();
        }
    }
}
