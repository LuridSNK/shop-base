﻿namespace ShopBase.Views.AttachedProperties
{
    using System;
    using System.Windows;
    using System.Windows.Controls;

    public static class FocusExtension
    {
        private static void OnEnsureFocusChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            if (!(bool)e.NewValue)
            {
                var control = d as Control;
                control?.Dispatcher.BeginInvoke(new Action(() => control.Focus()));
            }
        }

        public static bool GetEnsureFocus(DependencyObject obj)
        {
            return (bool)obj.GetValue(EnsureFocusProperty);
        }

        public static void SetEnsureFocus(DependencyObject obj, bool value)
        {
            obj.SetValue(EnsureFocusProperty, value);
        }

        public static readonly DependencyProperty EnsureFocusProperty =
            DependencyProperty.RegisterAttached(
                "EnsureFocus",
                typeof(bool),
                typeof(FocusExtension),
                new PropertyMetadata(OnEnsureFocusChanged));
    }
}
