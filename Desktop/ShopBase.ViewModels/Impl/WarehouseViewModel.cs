﻿namespace ShopBase.ViewModels.Impl
{
    using System.Windows.Input;
    using GalaSoft.MvvmLight;
    using GalaSoft.MvvmLight.Command;
    using GalaSoft.MvvmLight.Ioc;
    using Interfaces;

    public class WarehouseViewModel : ViewModelBase, IWarehouseViewModel
    {

        private IChildViewModel _childViewModel;

        public WarehouseViewModel()
        {
            this.ProceedToWarehouseScanningCommand = new RelayCommand(() => this.ChildViewModel = SimpleIoc.Default.GetInstance<IWarehouseScanViewModel>());
            this.ProceedToPickUpCommand = new RelayCommand(() => this.ChildViewModel = SimpleIoc.Default.GetInstance<IPickUpViewModel>());
        }

        public IChildViewModel ChildViewModel
        {
            get => _childViewModel;
            set => Set(ref _childViewModel, value, nameof(this.ChildViewModel));
        }
        
        public ICommand ProceedToWarehouseScanningCommand { get; }

        public ICommand ProceedToPickUpCommand { get; }
    }
}