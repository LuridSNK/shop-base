namespace ShopBase.ViewModels.Impl
{
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Reflection;
    using System.Windows.Input;
    using GalaSoft.MvvmLight;
    using GalaSoft.MvvmLight.Command;
    using GalaSoft.MvvmLight.Ioc;
    using ShopBase.DesktopServices.Impl;
    using ShopBase.Domain;
    using ShopBase.Shared.Enums;
    using ShopBase.ViewModels.Interfaces;

    public class MainViewModel : ViewModelBase, IMainViewModel
    {
        private double _frameWidth;

        private double _frameHeight;

        private string _version;

        private INavigationPage _currentPage;

        public MainViewModel()
        {
            this.CurrentPage = SimpleIoc.Default.GetInstance<IWarehouseViewModel>();
            this.Version = Assembly.GetExecutingAssembly().GetName().Version.ToString();
            this.SelectSectionCommand = new RelayCommand<string>(SetPage);
            this.SectionNames = GetSectionsBasedOnRole(ApplicationSettings.Instance.CurrentUser.Role);
        }

        public string Version
        {
            get => _version;
            set => Set(ref _version, value, nameof(this.Version));
        }

        public double MainFrameWidth
        {
            get => _frameWidth;
            set => Set(ref _frameWidth, value, nameof(this.MainFrameWidth));
        }

        public double MainFrameHeight
        {
            get => _frameHeight;
            set => Set(ref _frameHeight, value, nameof(this.MainFrameHeight));
        }

        public string Username => ApplicationSettings.Instance.CurrentUser.Name;

        public INavigationPage CurrentPage
        {
            get => _currentPage;
            set => Set(ref _currentPage, value, nameof(this.CurrentPage));
        }

        public ObservableCollection<string> SectionNames { get; }

        public ICommand SelectSectionCommand { get; }

        private void SetPage(string key)
        {
            var keyTypeStorage = new Dictionary<string, Type>
                                     {
                                         { "Catalog", typeof(ICatalogViewModel) },
                                         { "Orders", typeof(IOrdersViewModel) },
                                         { "Clients", typeof(IClientsViewModel) },
                                         { "Purchases", typeof(IPurchasesViewModel) },
                                         { "Warehouse", typeof(IWarehouseViewModel) },
                                         { "Logistics", typeof(ILogisticsViewModel) },
                                         { "Refunds", typeof(IRefundsViewModel) },
                                         { "Logs", typeof(ILogsViewModel) }
                                     };

                CurrentPage = SimpleIoc.Default.GetInstance(keyTypeStorage[key]) as INavigationPage;
        }

        private ObservableCollection<string> GetSectionsBasedOnRole(RoleType role)
        {
            if (role == RoleType.SeniorEmployee)
            {
                return new ObservableCollection<string> { "Warehouse" };
            }

            if (role == RoleType.Employee)
            {
                return new ObservableCollection<string> { "Warehouse" };
            }

            if (role == RoleType.HeadOfDepartment)
            {
                return new ObservableCollection<string>
                           {
                               "Catalog",
                               "Orders",
                               "Clients",
                               "Purchases",
                               "Warehouse",
                               "Logistics",
                               "Refunds"
                           };
            }

            if (role == RoleType.Administrator)
            {
                return new ObservableCollection<string>
                           {
                               "Catalog",
                               "Orders",
                               "Clients",
                               "Purchases",
                               "Warehouse",
                               "Logistics",
                               "Refunds",
                               "Logs"
                           };
            }

            if (role == RoleType.Developer)
            {
                return new ObservableCollection<string>
                           {
                               "Catalog",
                               "Orders",
                               "Clients",
                               "Purchases",
                               "Warehouse",
                               "Logistics",
                               "Refunds",
                               "Logs"
                           };
            }

            throw new ArgumentOutOfRangeException(nameof(role), role, null);
        }
    }
}