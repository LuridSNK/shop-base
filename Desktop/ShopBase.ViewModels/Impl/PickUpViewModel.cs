﻿namespace ShopBase.ViewModels.Impl
{
    using System.Collections.ObjectModel;
    using System.Text.RegularExpressions;
    using System.Windows.Input;
    using DesktopServices.Interfaces;
    using GalaSoft.MvvmLight;
    using GalaSoft.MvvmLight.CommandWpf;
    using Interfaces;
    using Messaging.Dto.Warehouse;

    using ShopBase.Domain;
    using ShopBase.Messaging.Http.Interfaces;
    using ShopBase.ViewModels.Messages;

    public class PickUpViewModel : ViewModelBase, IPickUpViewModel
    {
        private readonly IPrinter _printer;

        private readonly IApplicationApiHandler _apiHandler;

        private PickUpOrderDto _selectedOrder;

        private ObservableCollection<Order> _orders;

        private ObservableCollection<ContainerDto> _selectedOrderContainers;

        public PickUpViewModel(IPrinter printer, IApplicationApiHandler apiHandler)
        {
            _printer = printer;
            _apiHandler = apiHandler;
            SearchUsersOrders = new RelayCommand<string>(
                s =>
                    {
                        var dataType = CheckoutDataType(s);
                        if (string.IsNullOrEmpty(dataType))
                        {
                            MessengerInstance.Send(new SnackMsg("Неверный формат данных по заказу"));
                            return;
                        }

                        var orders = _apiHandler.GetOrdersByUserData(dataType, s);

                    });
        }

        public PickUpOrderDto SelectedOrder
        {
            get => _selectedOrder;
            set
            {
                Set(ref _selectedOrder, value, nameof(SelectedOrder));
                Set(ref _selectedOrderContainers, new ObservableCollection<ContainerDto>(value.Containers), nameof(SelectedOrderContainers));
            }

        }

        public ObservableCollection<Order> Orders
        {
            get => _orders;
            set => Set(ref _orders, value, nameof(Orders));
        }
        
        public ObservableCollection<ContainerDto> SelectedOrderContainers
        {
            get => _selectedOrderContainers;
            set => Set(ref _selectedOrderContainers, value, nameof(SelectedOrderContainers));
        }

        public ICommand SearchUsersOrders { get; set; }


        private string CheckoutDataType(string s)
        {
            if (Regex.IsMatch(s, @"^\d{3}-\d{4}-\d{4}$") || Regex.IsMatch(s, @"^\d{4}-\d{4}$"))
            {
                return "order";
            }

            if (Regex.IsMatch(s, @"[^@]+@[^\.]+\..+"))
            {
                return "email";
            }

            if ((s.StartsWith("+7") || s.StartsWith("8") || s.StartsWith("9")) && s.Length >= 10)
            {
                return "phone";
            }

            return null;
        }
    }
}