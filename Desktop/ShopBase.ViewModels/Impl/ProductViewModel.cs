﻿namespace ShopBase.ViewModels.Impl
{
    using System;

    using GalaSoft.MvvmLight;

    public class ProductViewModel : ViewModelBase
    {
        #region private fields
        private Guid _id;

        private string _name;

        private string _sku;

        private decimal _price;

        private int _amount;

        private int _reservedAmount;

        private bool _isPublished;

        private bool _hasPhotos;

        private bool _canBeAGift;

        private bool _isPriceLocked;

        private bool _isThereComments;

        #endregion

        public Guid Id
        {
            get => _id;
            set => this.Set(ref _id, value, nameof(this.Id));
        }

        public string Name
        {
            get => _name;
            set => this.Set(ref _name, value, nameof(this.Name));
        }

        public string Sku
        {
            get => _sku;
            set => this.Set(ref _sku, value, nameof(this.Sku));
        }

        public decimal Price
        {
            get => _price;
            set => this.Set(ref _price, value, nameof(this.Price));
        }

        public int Amount
        {
            get => _amount;
            set => this.Set(ref _amount, value, nameof(this.Amount));
        }

        public int ReservedAmount
        {
            get => _reservedAmount;
            set => this.Set(ref _reservedAmount, value, nameof(this.ReservedAmount));
        }

        public bool IsPublished
        {
            get => _isPublished;
            set => this.Set(ref _isPublished, value, nameof(this.IsPublished));
        }

        public bool HasPhotos
        {
            get => _hasPhotos;
            set => this.Set(ref _hasPhotos, value, nameof(this.HasPhotos));
        }

        public bool CanBeAGift
        {
            get => _canBeAGift;
            set => this.Set(ref _canBeAGift, value, nameof(this.CanBeAGift));
        }

        public bool IsPriceLocked
        {
            get => _isPriceLocked;
            set => this.Set(ref _isPriceLocked, value, nameof(this.IsPriceLocked));
        }

        public bool IsThereComments
        {
            get => _isThereComments;
            set => this.Set(ref _isThereComments, value, nameof(this.IsThereComments));
        }
    }
}
