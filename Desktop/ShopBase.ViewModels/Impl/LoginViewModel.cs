﻿namespace ShopBase.ViewModels.Impl
{
    using System;
    using System.Threading.Tasks;
    using System.Windows.Input;
    using GalaSoft.MvvmLight;
    using GalaSoft.MvvmLight.Command;
    using ShopBase.DesktopServices.Impl;
    using ShopBase.Messaging.Http.Interfaces;
    using ShopBase.Shared.Models;
    using ShopBase.ViewModels.Interfaces;
    using ShopBase.ViewModels.Messages;

    public class LoginViewModel : ViewModelBase, ILoginViewModel
    {
        private readonly IApplicationApiHandler _apiHandler;

        private string _username;

        private string _password;

        public LoginViewModel(IApplicationApiHandler apiHandler)
        {
            _apiHandler = apiHandler;

            this.LoginCommand = new RelayCommand(
                async () =>
                    {
                        var isAuthenticated = await this.LogIn(this.Username, this.Password);
                        if (isAuthenticated) MessengerInstance.Send(new AuthMessage(true));
                    });
        }
       
        public string Username
        {
            get => _username;
            set => Set(ref _username, value, nameof(this.Username));
        }

        public string Password
        {
            get => _password;
            set => Set(ref _password, value, nameof(this.Password));
        }
      
        public ICommand LoginCommand { get; set; }

        private async Task<bool> LogIn(string username, string password)
        {
            try
            {
                var model = new LoginModel { Username = username, Password = password };
                this.ValidateCredentials(model);
                var user = await _apiHandler.Authenticate(model);
                ApplicationSettings.Instance.CurrentUser = user ?? throw new ArgumentException($"Авторизация прошла неудачно!");
                return true;
            }
            catch (Exception e)
            {
                MessengerInstance.Send(e.Message, "error");
                return false;
            }
        }

        private void ValidateCredentials(LoginModel model)
        {
            this.CheckField(nameof(model.Username), model.Username);
            this.CheckField(nameof(model.Password), model.Password);
        }

        private void CheckField(string fieldName, string fieldValue)
        {
            if (string.IsNullOrEmpty(fieldValue) || string.IsNullOrWhiteSpace(fieldValue))
            {
                throw new ArgumentException($"Поля не должны быть пустыми");
            }
        }
    }
}