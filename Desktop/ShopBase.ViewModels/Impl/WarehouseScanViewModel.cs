﻿namespace ShopBase.ViewModels.Impl
{
    using System;
    using System.Collections.ObjectModel;
    using System.ComponentModel;
    using System.IO;
    using System.Linq;
    using System.Threading.Tasks;
    using System.Windows.Input;
    using DesktopServices.Interfaces;
    using Domain;
    using GalaSoft.MvvmLight;
    using GalaSoft.MvvmLight.CommandWpf;
    using Interfaces;
    using Messages;
    using Messaging.Http.Interfaces;
    using Services.PdfGeneration.Interfaces;
    using Utils;

    public class WarehouseScanViewModel : ViewModelBase, IWarehouseScanViewModel
    {
        private readonly IPrinter _printer;

        private readonly IPdfGenerator _pdfGenerator;

        private readonly IApplicationApiHandler _apiHandler;

        private string _inputBarcode;

        private Order _order;

        private double _packageWeight;

        private Stream _labelStream;

        private ObservableCollection<CheckedContainer> _orderContainers;

        public WarehouseScanViewModel(
            IPrinter printer,
            IPdfGenerator pdfGen, 
            IApplicationApiHandler apiHandler)
        {
            _printer = printer;
            _pdfGenerator = pdfGen;
            _apiHandler = apiHandler;

            ClearInputCommand = new RelayCommand(() => this.InputBarcode = string.Empty);
            ShipCommand = new RelayCommand(async () => { await SendDataToWebService(); PrintingAvaliable = true; });
            PrintCommand = new RelayCommand(async () => await PrintInternalDocuments());
            PropertyChanged += this.OnBarcodeChanged;
            PrintingAvaliable = false;
        }

        public double PackageWeight
        {
            get => _packageWeight; 
            set => Set(ref _packageWeight, value, nameof(PackageWeight));
        }

        public string InputBarcode
        {
            get => _inputBarcode;
            set => Set(ref _inputBarcode, value, nameof(InputBarcode));
        }

        public Order Order
        {
            get => _order;
            set
            {
                if (value == null)
                {
                    return;
                }

                Set(ref _order, value, nameof(Order));
                var containers = new ObservableCollection<CheckedContainer>(
                    value.Containers.Select(
                        c => new CheckedContainer { IsChecked = false, Cell = c.Cell, Name = c.Name }));
                Set(ref _orderContainers, containers, nameof(OrderContainers));
            }
        }

        public ObservableCollection<CheckedContainer> OrderContainers
        {
            get => _orderContainers;
            set => Set(ref _orderContainers, value, nameof(OrderContainers));
        }

        public bool PrintingAvaliable { get; set; }

        public ICommand ClearInputCommand { get; }

        public ICommand ShipCommand { get; }

        public ICommand PrintCommand { get; }


        private async Task SendDataToWebService()
        {
            try
            {
                _labelStream = await _apiHandler.SendOrderToWms(this.Order.Id);
            }
            catch (Exception e)
            {
               e.HandleException();
            }
        }
        
        private async Task PrintInternalDocuments()
        {
            var path = await Task.Factory.StartNew(() =>
                Path.Combine(
                    Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData),
                    $"{Order.PublicNumber}_{DateTime.Now:ss-ffff}.pdf"));
            _pdfGenerator.GenerateFor(Order).Create(path);
            _printer.Print(path);
        }
        

        // event handling
        private async void OnBarcodeChanged(object sender, PropertyChangedEventArgs e)
        {
            var barcode = InputBarcode.Trim();
            if (e.PropertyName == nameof(InputBarcode) && !string.IsNullOrEmpty(barcode))
            {
                if (OrderContainers == null || OrderContainers.All(c => c.IsChecked) || OrderContainers.All(c => c.Name != barcode))
                {
                    try
                    {
                        Order = await _apiHandler.GetOrderByContainer(barcode);
                        PrintingAvaliable = false;
                    }
                    catch (Exception ex)
                    {
                        ex.HandleException();
                        InputBarcode = string.Empty;
                        return;
                    }
                }

                if (OrderContainers != null)
                {
                    foreach (var container in OrderContainers.Where(c => !c.IsChecked && c.Name == barcode))
                        container.IsChecked = true;
                    InputBarcode = string.Empty;
                    return;
                }

                InputBarcode = string.Empty;
                MessengerInstance.Send(new SnackMsg("Указан неверный номер контейнера!"));
            }
        }
    }
}