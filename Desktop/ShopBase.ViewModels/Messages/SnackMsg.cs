﻿namespace ShopBase.ViewModels.Messages
{
    public class SnackMsg
    {
        public SnackMsg()
        {
        }

        public SnackMsg(string msg)
        {
            Message = msg;
        }

        public string Message { get; set; }
    }
}
