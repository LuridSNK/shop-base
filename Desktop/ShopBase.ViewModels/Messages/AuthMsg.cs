﻿namespace ShopBase.ViewModels.Messages
{
    public class AuthMessage
    {
        public AuthMessage(bool isAuthenticated)
        {
            IsAuthenticated = isAuthenticated;
        }

        public bool IsAuthenticated { get; set; }
    }
}
