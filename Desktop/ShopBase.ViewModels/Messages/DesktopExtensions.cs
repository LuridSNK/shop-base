﻿namespace ShopBase.ViewModels.Messages
{
    using System;
    using GalaSoft.MvvmLight.Messaging;
    using ShopBase.Messaging.Http.Impl;

    internal static class DesktopExtensions
    {
        /// <summary>
        /// Sends snackbar message on exception
        /// </summary>
        /// <param name="e">
        /// <see cref="Exception"/> of any type
        /// </param>
        public static void HandleException(this Exception e)
        {
            Messenger.Default.Send(new SnackMsg { Message = e.InnerException?.Message ?? e.Message });
        }

        /// <summary>
        /// Sends snackbar message if response is not successful
        /// </summary>
        /// <param name="resp">
        /// <see cref="ApiResponse"/> of any generic type
        /// </param>
        public static void HandleApiResponse(this ApiResponse resp)
        {
            if (!resp.IsSuccessful)
            {
                Messenger.Default.Send(new SnackMsg { Message = resp.ErrorMessage });
            }
        }
    }
}
