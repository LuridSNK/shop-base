﻿
namespace ShopBase.ViewModels.Interfaces
{
    using System.Collections.ObjectModel;
    using System.Windows.Input;
    using ShopBase.Domain;
    using ShopBase.ViewModels.Utils;

    public interface IWarehouseScanViewModel : IChildViewModel
    {
        double PackageWeight { get; set; }

        string InputBarcode { get; set; }

        Order Order { get; set; }

        ObservableCollection<CheckedContainer> OrderContainers { get; set; }

        bool PrintingAvaliable { get; set; }
        

        ICommand ClearInputCommand { get; }

        ICommand ShipCommand { get; }

        ICommand PrintCommand { get; }
    }
}
