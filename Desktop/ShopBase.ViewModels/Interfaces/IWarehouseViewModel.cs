﻿namespace ShopBase.ViewModels.Interfaces
{
    using System.Windows.Input;

    public interface IWarehouseViewModel : INavigationPage
    {
        IChildViewModel ChildViewModel { get; set; }

        ICommand ProceedToWarehouseScanningCommand { get; }

        ICommand ProceedToPickUpCommand { get; }
    }
}
