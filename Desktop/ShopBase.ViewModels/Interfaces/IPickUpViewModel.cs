﻿namespace ShopBase.ViewModels.Interfaces
{
    using System.Collections.ObjectModel;
    using System.Windows.Input;
    using Messaging.Dto.Warehouse;
    using ShopBase.Domain;

    public interface IPickUpViewModel : IChildViewModel
    {
        PickUpOrderDto SelectedOrder { get; set; }

        ObservableCollection<Order> Orders { get; set; }

        ICommand SearchUsersOrders { get; set; }
    }
}
