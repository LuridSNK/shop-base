﻿namespace ShopBase.ViewModels.Interfaces
{
    using System.Collections.ObjectModel;
    using System.Windows.Input;

    /// <summary>
    /// MainViewModel is a system/navigation ViewModel, that contains all the data to manipulate MainWindow
    /// </summary>
    public interface IMainViewModel
    {
        /// <summary>
        /// Width of the MainWindow
        /// </summary>
        double MainFrameWidth { get; set; }

        /// <summary>
        /// Height of the MainWindow
        /// </summary>
        double MainFrameHeight { get; set; }

        /// <summary>
        /// Gets the version of the program, default (fallback value) is 4.0.0
        /// </summary>
        string Version { get; set; }

        /// <summary>
        /// Gets the username of the current user
        /// </summary>
        string Username { get; }

        /// <summary>
        /// Gets/sets the current page of the main viewport
        /// </summary>
        INavigationPage CurrentPage { get; set; }

        /// <summary>
        /// Gets/sets a collection of items, represented by buttons in View
        /// </summary>
        ObservableCollection<string> SectionNames { get; }

        /// <summary>
        /// Sets selected section name to CurrentFrame
        /// </summary>
        ICommand SelectSectionCommand { get; }
    }
}
