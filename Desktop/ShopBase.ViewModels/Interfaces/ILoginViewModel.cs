﻿namespace ShopBase.ViewModels.Interfaces
{
    using System.Windows.Input;

    public interface ILoginViewModel
    {
         string Username { get; set; }

         string Password { get; set; }

         ICommand LoginCommand { get; set; }
    }
}
