﻿namespace ShopBase.ViewModels.Utils
{
    using GalaSoft.MvvmLight;

    public class CheckedContainer : ViewModelBase
    {
        private bool _isChecked;

        private string _name;

        private string _cell;

        public bool IsChecked
        {
            get => _isChecked;
            set => Set(ref _isChecked, value, nameof(IsChecked));
        }

        public string Name
        {
            get => _name;
            set => Set(ref _name, value, nameof(Name));
        }

        public string Cell
        {
            get => _cell;
            set => Set(ref _cell, value, nameof(Cell));
        }


    }

}
