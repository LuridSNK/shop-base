﻿namespace ShopBase.Desktop.Controls
{
    using System.Windows.Controls;

    /// <summary>
    /// Interaction logic for ProductControl.xaml
    /// </summary>
    public partial class ProductControl : UserControl
    {
        public ProductControl()
        {
            this.InitializeComponent();
        }
    }
}
