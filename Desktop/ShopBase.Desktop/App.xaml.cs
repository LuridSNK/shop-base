﻿namespace ShopBase.Desktop
{
    using System.Net.Http;
    using System.Windows;
    using Settings;

    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        public App()
        {
        }

        // ToDo:
        // Make 'Login' first Window to load
        // Use it for fetching credentials
        // Run MainWindow & set it as 'real main' via calling Application.Current.MainWindow property 
        // Close login window & continue


        protected override void OnStartup(StartupEventArgs e)
        {
            //using (var mgr = new UpdateManager(Path.Combine("D:\\Squirrel\\SB\\Releases")))
            //{
            //    mgr.UpdateApp().GetAwaiter().GetResult();
            //}

            //using (var mgr = new UpdateManager(@"D:\_sb_v2.0\Releases"))
            //{
            //    mgr.UpdateApp().GetAwaiter().GetResult();
            //}
            
            base.OnStartup(e);
        }
    }
}
