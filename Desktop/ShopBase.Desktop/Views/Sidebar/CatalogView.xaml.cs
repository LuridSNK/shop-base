﻿using System.Windows.Controls;
using ShopBase.Desktop.Infrastructure;
using ShopBase.Desktop.Interfaces;

namespace ShopBase.Desktop.Views.Sidebar
{
    /// <summary>
    /// Interaction logic for CatalogView.xaml
    /// </summary>
    public partial class CatalogView : UserControl
    {
        public CatalogView()
        {
            InitializeComponent();
        }
    }
}
