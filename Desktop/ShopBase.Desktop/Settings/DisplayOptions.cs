﻿namespace ShopBase.Desktop.Settings
{
    using System.Windows;

    public class DisplayOptions
    {
        public Point Resolution { get; set; }

        public WindowState State { get; set; }
    }
}
