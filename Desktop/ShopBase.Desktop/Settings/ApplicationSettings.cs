﻿namespace ShopBase.Desktop.Settings
{
    using System;
    using System.Configuration;
    using Domain;

    using ShopBase.Messaging.Dto;

    public class ApplicationSettings : ApplicationSettingsBase
    {

        public static ApplicationSettings Instance { get; } = new ApplicationSettings();

        [UserScopedSetting]
        public string Token
        {
            get => ((UserDto)this[nameof(this.CurrentUser)]).Token;
            set => ((UserDto)this[nameof(CurrentUser)]).Token = value;
        }


        [UserScopedSetting]
        public DisplayOptions DisplayOptions
        {
            get => (DisplayOptions)this[nameof(this.DisplayOptions)];
            set => this[nameof(this.DisplayOptions)] = value;
        }

        [UserScopedSetting]
        public UserDto CurrentUser
        {
            get => (UserDto)this[nameof(this.CurrentUser)];
            set => this[nameof(this.CurrentUser)] = value;
        }

        [UserScopedSetting]
        public string LastUsedViewModel
        {
            get => (string)this[nameof(this.LastUsedViewModel)];
            set => this[nameof(this.LastUsedViewModel)] = value;
        }
        
        // AN EXAMPLE!
        //[UserScopedSetting()]
        //[DefaultSettingValueAttribute("LightGray")]
        //public Color FormBackColor
        //{
        //    get { return (Color)this["FormBackColor"]; }
        //    set { this["FormBackColor"] = value; }
        //}
    }
}