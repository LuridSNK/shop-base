﻿using GalaSoft.MvvmLight.Ioc;

namespace ShopBase.Desktop.Windows
{
    using System.Windows;
    using Interfaces;
    using Settings;

    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window, ICloseable, ICollapsible
    {
        public MainWindow()
        {
            this.InitializeComponent();
            this.Loaded += MainWindow_Loaded;
            this.Closed += MainWindow_Closed;
        }



        private void MainWindow_Closed(object sender, System.EventArgs e)
        {
            var settings = ApplicationSettings.Instance;
            settings.DisplayOptions = new DisplayOptions { State = this.WindowState, Resolution = new Point(this.Width, this.Height) };
            settings.Save();
        }


        private void MainWindow_Loaded(object sender, RoutedEventArgs e)
        {
            var settings = ApplicationSettings.Instance;
            var options = settings.DisplayOptions;
            if (options != null)
            {
                this.Width = options.Resolution.X;
                this.Height = options.Resolution.Y;
                this.WindowState = options.State;
            }
        }

        public void Collapse()
        {
            this.WindowState = WindowState.Minimized;
        }

        public void Expand()
        {
            this.WindowState ^= WindowState.Maximized;
        }

    }
}
