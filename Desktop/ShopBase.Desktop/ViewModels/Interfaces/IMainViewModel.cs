﻿namespace ShopBase.Desktop.ViewModels.Interfaces
{
    using System.Collections.ObjectModel;
    using System.Windows.Input;
    using ShopBase.Desktop.Infrastructure;

    /// <summary>
    /// MainViewModel is a system/navigation ViewModel, that contains all the data to manipulate MainWindow
    /// </summary>
    public interface IMainViewModel
    {
        /// <summary>
        /// Username of the current user
        /// </summary>
        string Username { get; }

        /// <summary>
        /// Current ViewModel, that is on the viewport
        /// </summary>
        IChildViewModel CurrentViewModel { get; set; }

        /// <summary>
        /// Collection of items, represented by buttons in View
        /// </summary>
        ObservableCollection<NavItem> SidebarItems { get; }

        /// <summary>
        /// Closes MainWindow
        /// </summary>
        ICommand CloseCommand { get; }

        /// <summary>
        /// Minimize MainWindow
        /// </summary>
        ICommand CollapseCommand { get; }

        /// <summary>
        /// Expand/Normalize MainWindow
        /// </summary>
        ICommand ExpandCommand { get; }

        /// <summary>
        /// Sets selected NavItem to CurrentViewModel
        /// </summary>
        ICommand SelectVmCommand { get; }
    }
}
