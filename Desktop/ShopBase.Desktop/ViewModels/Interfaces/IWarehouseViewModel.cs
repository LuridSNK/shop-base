﻿namespace ShopBase.Desktop.ViewModels.Interfaces
{
    using System.Windows.Input;

    public interface IWarehouseViewModel : IChildViewModel
    {
        IChildViewModel ChildViewModel { get; set; }

        ICommand ProceedToWarehouseScanningCommand { get; }

        ICommand ProceedToPickUpCommand { get; }
    }
}
