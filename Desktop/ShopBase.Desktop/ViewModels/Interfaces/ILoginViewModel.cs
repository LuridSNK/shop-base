﻿namespace ShopBase.Desktop.ViewModels.Interfaces
{
    using System.Windows.Input;

    public interface ILoginViewModel
    {
         string Username { get; set; }

         string Password { get; set; }

         string ErrorMessage { get; set; }

         ICommand LoginCommand { get; set; }

         ICommand CloseCommand { get; set; }
    }
}
