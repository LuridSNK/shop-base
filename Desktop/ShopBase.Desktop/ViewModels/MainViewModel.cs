namespace ShopBase.Desktop.ViewModels
{
    using System;
    using System.Collections.ObjectModel;
    using System.Linq;
    using System.Windows.Input;
    using GalaSoft.MvvmLight;
    using GalaSoft.MvvmLight.Command;
    using GalaSoft.MvvmLight.Ioc;
    using ShopBase.Desktop.Infrastructure;
    using ShopBase.Desktop.Interfaces;
    using ShopBase.Desktop.Settings;
    using ShopBase.Desktop.ViewModels.Interfaces;

    public class MainViewModel : ViewModelBase, IMainViewModel
    {
        private IChildViewModel _currentViewModel;

        public MainViewModel()
        {
            CurrentViewModel = GetLastUsedViewModel();
            
            CloseCommand = new RelayCommand<ICloseable>(Close);
            CollapseCommand = new RelayCommand<ICollapsible>(Collapse);
            ExpandCommand = new RelayCommand<ICollapsible>(Expand);
            SelectVmCommand = new RelayCommand<Type>(SetViewModel);

            SidebarItems = new ObservableCollection<NavItem>
                               {
                                   new NavItem { Name = "Catalog", VmType = typeof(ICatalogViewModel) },
                                   new NavItem { Name = "Orders", VmType = typeof(IOrdersViewModel) },
                                   new NavItem { Name = "Warehouse", VmType = typeof(IWarehouseViewModel) },
                                   new NavItem { Name = "Clients", VmType = typeof(IClientsViewModel) },
                                   new NavItem { Name = "Purchases", VmType = typeof(IPurchasesViewModel) },
                                   new NavItem { Name = "Logistics", VmType = typeof(ILogisticsViewModel) },
                                   new NavItem { Name = "Refunds", VmType = typeof(IRefundsViewModel) },
                                   new NavItem { Name = "Logs", VmType = typeof(ILogsViewModel) },
                               };
        }

        public string Username => ApplicationSettings.Instance.CurrentUser.Name;

        public IChildViewModel CurrentViewModel
        {
            get => _currentViewModel;
            set => Set(ref _currentViewModel, value, nameof(CurrentViewModel));
        }

        public ObservableCollection<NavItem> SidebarItems { get; }

        public ICommand CloseCommand { get; }

        public ICommand CollapseCommand { get; }

        public ICommand ExpandCommand { get; }

        public ICommand SelectVmCommand { get; }

        private IChildViewModel GetLastUsedViewModel()
        {
            if (string.IsNullOrEmpty(ApplicationSettings.Instance.LastUsedViewModel))
            {
                return SimpleIoc.Default.GetInstance<ICatalogViewModel>();
            }
            
            return SimpleIoc.Default.GetInstance(Type.GetType(ApplicationSettings.Instance.LastUsedViewModel)) as IChildViewModel;
        }

        private void Close(ICloseable closable)
        {
            var instance = ApplicationSettings.Instance;
            instance.LastUsedViewModel = CurrentViewModel
                .GetType()
                .GetInterfaces()
                .FirstOrDefault(i => i.Name.EndsWith("viewmodel", StringComparison.InvariantCultureIgnoreCase))
                ?.FullName;
            instance.Save();
            closable.Close();
        }

        private void Collapse(ICollapsible collapsible)
        {
            collapsible.Collapse();
        }

        private void Expand(ICollapsible collapsible)
        {
            collapsible.Expand();
        }

        private void SetViewModel(Type vm)
        {
            CurrentViewModel = SimpleIoc.Default.GetInstance(vm) as IChildViewModel;
        }
    }
}