/*
  In App.xaml:
  <Application.Resources>
      <vm:ViewModelLocator xmlns:vm="clr-namespace:ShopBase.Desktop"
                           x:Key="Locator" />
  </Application.Resources>
  
  In the View:
  DataContext="{Binding Source={StaticResource Locator}, Path=ViewModelName}"

  You can also use Blend to do all this with the tool's support.
  See http://www.galasoft.ch/mvvm
*/

namespace ShopBase.Desktop.ViewModels
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using Castle.MicroKernel.Registration;
    using Castle.Windsor;
    using CommonServiceLocator;
    using GalaSoft.MvvmLight.Ioc;
    using ShopBase.Desktop.ViewModels.ChildViewModels;
    using ShopBase.Desktop.ViewModels.Interfaces;
    using ShopBase.Messaging.Http.Interfaces;
    using ShopBase.Messaging.Http.Stubs;

    /// <summary>
    /// This class contains static references to all the view models in the
    /// application and provides an entry point for the bindings.
    /// </summary>
    public class ViewModelLocator
    {
        /// <summary>
        /// Initializes a new instance of the ViewModelLocator class.
        /// </summary>
        public ViewModelLocator()
        {
            ServiceLocator.SetLocatorProvider(() => /*new WindsorServiceLocator(WindsorIoc.WindsorContainer)*/ SimpleIoc.Default);

            //if (ViewModelBase.IsInDesignModeStatic)
            //{
            //    // Create design time view services and models
            //    SimpleIoc.Default.Register<IDataService, DesignDataService>();
            //}
            //else
            //{
            //    // Create run time view services and models
            //    SimpleIoc.Default.Register<IDataService, DataService>();
            //}

            //////HttpClient client = new HttpClient();

            //////SimpleIoc.Default.Register(() => client);
            //////SimpleIoc.Default.Register<IApplicationHttpClient, ApplicationHttpClient>();

#if DEBUG
             SimpleIoc.Default.Register<IApplicationApiHandler, ApplicationHttpHandlerStub>();
#else
             WindsorIoc.Instance.Register<IApplicationApiHandler, ApplicationHttpHandler>();
#endif

            //WindsorIoc.Instance.Register<>();


            // windows
            SimpleIoc.Default.Register<ILoginViewModel, LoginViewModel>();
            SimpleIoc.Default.Register<IMainViewModel, MainViewModel>();

            //sidebar
            SimpleIoc.Default.Register<ICatalogViewModel, CatalogViewModel>();
            SimpleIoc.Default.Register<IClientsViewModel, ClientsViewModel>();
            SimpleIoc.Default.Register<IOrdersViewModel, OrdersViewModel>();
            SimpleIoc.Default.Register<IPurchasesViewModel, PurchasesViewModel>();
            SimpleIoc.Default.Register<IWarehouseViewModel, WarehouseViewModel>();
            SimpleIoc.Default.Register<ILogisticsViewModel, LogisticsViewModel>();
            SimpleIoc.Default.Register<IRefundsViewModel, RefundsViewModel>();
            SimpleIoc.Default.Register<ILogsViewModel, LogsViewModel>();
            

            // warehouse
            SimpleIoc.Default.Register<IPickUpViewModel, PickUpViewModel>();
            SimpleIoc.Default.Register<IWarehouseScanViewModel, WarehouseScanViewModel>();
        }



        public ILoginViewModel LoginVm => ServiceLocator.Current.GetInstance<ILoginViewModel>();

        public IMainViewModel MainVm => ServiceLocator.Current.GetInstance<IMainViewModel>();

        public ICatalogViewModel CatalogVm => ServiceLocator.Current.GetInstance<ICatalogViewModel>();
        
        public IPickUpViewModel PickUpVm => ServiceLocator.Current.GetInstance<IPickUpViewModel>();

        public IWarehouseViewModel WarehouseVm => ServiceLocator.Current.GetInstance<IWarehouseViewModel>();

        public static void Cleanup()
        {
            // TODO Clear the ViewModels
        }
        
    }

    public class WindsorServiceLocator : IServiceLocator
    {
        private readonly IWindsorContainer _container;

        public WindsorServiceLocator(IWindsorContainer container)
        {
            _container = container;
        }
        public object GetService(Type serviceType)
        {
           return _container.Resolve(serviceType);
        }

        public object GetInstance(Type serviceType)
        {
            return _container.Resolve(serviceType);
        }

        public object GetInstance(Type serviceType, string key)
        {
            return _container.Resolve(key, serviceType);
        }

        public IEnumerable<object> GetAllInstances(Type serviceType)
        {
            return _container.ResolveAll(serviceType).Cast<object>();
        }

        public TService GetInstance<TService>()
        {
            return _container.Resolve<TService>();
        }

        public TService GetInstance<TService>(string key)
        {
            return _container.Resolve<TService>(key);
        }

        public IEnumerable<TService> GetAllInstances<TService>()
        {
            return _container.ResolveAll<TService>();
        }

    }

    public class WindsorIoc
    {
        public static WindsorContainer WindsorContainer { get; } = new WindsorContainer();

        public static WindsorIoc Default { get; } = new WindsorIoc();

        public object GetService(Type serviceType)
        {
            return WindsorContainer.Resolve(serviceType);
        }

        public object GetInstance(Type serviceType)
        {
            return WindsorContainer.Resolve(serviceType);
        }

        public object GetInstance(Type serviceType, string key)
        {
            return WindsorContainer.Resolve(key, serviceType);
        }

        public IEnumerable<object> GetAllInstances(Type serviceType)
        {
            return WindsorContainer.ResolveAll(serviceType).Cast<object>();
        }

        public TService GetInstance<TService>()
        {
            return WindsorContainer.Resolve<TService>();
        }

        public TService GetInstance<TService>(string key)
        {
            return WindsorContainer.Resolve<TService>(key);
        }

        public IEnumerable<TService> GetAllInstances<TService>()
        {
            return WindsorContainer.ResolveAll<TService>();
        }

        public bool ContainsCreated<TClass>()
        {
            return WindsorContainer.Kernel.HasComponent(typeof(TClass));
        }

        public bool ContainsCreated<TClass>(string key)
        {
            return WindsorContainer.Kernel.HasComponent(key);
        }

        public bool IsRegistered<T>()
        {
            return WindsorContainer.Kernel.HasComponent(typeof(T));
        }

        public bool IsRegistered<T>(string key)
        {
            return WindsorContainer.Kernel.HasComponent(key);
        }

        public void Register<TInterface, TClass>()
            where TInterface : class where TClass : class, TInterface
        {
            WindsorContainer.Register(Component.For<TInterface>().ImplementedBy<TClass>());
        }

        public void Register<TInterface, TClass>(bool createInstanceImmediately)
            where TInterface : class where TClass : class, TInterface
        {
            WindsorContainer.Register(Component.For<TInterface>().ImplementedBy<TClass>());
        }

        public void Register<TClass>()
            where TClass : class
        {
            WindsorContainer.Register(Component.For<TClass>());
        }

        public void Register<TClass>(bool createInstanceImmediately)
            where TClass : class
        {
            WindsorContainer.Register(Component.For<TClass>().LifestyleSingleton());
        }

        public void Register<TClass>(Func<TClass> factory)
            where TClass : class
        {
            WindsorContainer.Register(Component.For<TClass>().UsingFactoryMethod(factory));
        }

        public void Register<TClass>(Func<TClass> factory, bool createInstanceImmediately)
            where TClass : class
        {
            WindsorContainer.Register(Component.For<TClass>().UsingFactoryMethod(factory));
        }

        public void Register<TClass>(Func<TClass> factory, string key)
            where TClass : class
        {
            WindsorContainer.Register(Component.For<TClass>().UsingFactoryMethod(factory).Named(key));
        }

        public void Register<TClass>(Func<TClass> factory, string key, bool createInstanceImmediately)
            where TClass : class
        {
            WindsorContainer.Register(Component.For<TClass>().UsingFactoryMethod(factory).Named(key));
        }

        public void Reset()
        {
            throw new NotImplementedException();
        }

        public void Unregister<TClass>()
            where TClass : class
        {
            throw new NotImplementedException();
        }

        public void Unregister<TClass>(TClass instance)
            where TClass : class
        {
            throw new NotImplementedException();
        }

        public void Unregister<TClass>(string key)
            where TClass : class
        {
            throw new NotImplementedException();
        }
    }
}