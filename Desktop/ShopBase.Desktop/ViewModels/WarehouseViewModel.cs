﻿namespace ShopBase.Desktop.ViewModels
{
    using System.Windows.Input;
    using GalaSoft.MvvmLight;
    using GalaSoft.MvvmLight.CommandWpf;
    using GalaSoft.MvvmLight.Ioc;
    using ShopBase.Desktop.ViewModels.Interfaces;

    public class WarehouseViewModel : ViewModelBase, IWarehouseViewModel
    {

        private IChildViewModel _childViewModel;

        public WarehouseViewModel()
        {

            ProceedToWarehouseScanningCommand = new RelayCommand(() => ChildViewModel = SimpleIoc.Default.GetInstance<IWarehouseScanViewModel>() as IChildViewModel);
            ProceedToPickUpCommand = new RelayCommand(() => ChildViewModel = SimpleIoc.Default.GetInstance<IPickUpViewModel>() as IChildViewModel);
        }

        public IChildViewModel ChildViewModel
        {
            get => _childViewModel;
            set => Set(ref _childViewModel, value, nameof(ChildViewModel));
        }
        
        public ICommand ProceedToWarehouseScanningCommand { get; }

        public ICommand ProceedToPickUpCommand { get; }
    }
}