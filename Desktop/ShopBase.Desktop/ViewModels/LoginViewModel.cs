﻿namespace ShopBase.Desktop.ViewModels
{
    using System;
    using System.Threading.Tasks;
    using System.Windows;
    using System.Windows.Input;
    using GalaSoft.MvvmLight;
    using GalaSoft.MvvmLight.CommandWpf;
    using GalaSoft.MvvmLight.Ioc;
    using ShopBase.Desktop.Interfaces;
    using ShopBase.Desktop.Settings;
    using ShopBase.Desktop.ViewModels.Interfaces;
    using ShopBase.Desktop.Windows;
    using ShopBase.Domain;
    using ShopBase.Messaging.Http.Interfaces;
    using ShopBase.Shared.Models;

    public class LoginViewModel : ViewModelBase, ILoginViewModel
    {
        private readonly IApplicationApiHandler _apiHandler;

        private string _username;

        private string _password;

        private string _errorMessage;

        public LoginViewModel(IApplicationApiHandler apiHandler)
        {
            _apiHandler = apiHandler;

            LoginCommand = new RelayCommand(
                async () =>
                    {
                        if (await LogIn(Username, Password))
                        {
                            var thisWindow = Application.Current.MainWindow;
                            Application.Current.MainWindow = new MainWindow
                            {
                                DataContext = SimpleIoc.Default.GetInstance<IMainViewModel>()
                            };
                            Application.Current.MainWindow.Show();
                            thisWindow?.Close();
                        }
                    });
            CloseCommand = new RelayCommand<ICloseable>(Close);
        }

        public string Username
        {
            get => _username;
            set => Set(ref _username, value, nameof(Username));
        }

        public string Password
        {
            get => _password;
            set => Set(ref _password, value, nameof(Password));
        }

        public string ErrorMessage
        {
            get => _errorMessage;
            set => Set(ref _errorMessage, value, nameof(ErrorMessage));
        }

        public ICommand LoginCommand { get; set; }

        public ICommand CloseCommand { get; set; }

        private async Task<bool> LogIn(string username, string password)
        {
            try
            {
                ErrorMessage = null;
                var model = new LoginModel { Username = username, Password = password };
                ValidateCredentials(model);
                var user = await _apiHandler.Authenticate(model);
                ApplicationSettings.Instance.CurrentUser = user ?? throw new ArgumentNullException(null, $"User with name '{model.Username}' was not found");
                return true;
            }
            catch (Exception e)
            {
                ErrorMessage = $"{e.Message}";
                return false;
            }
        }

        private void ValidateCredentials(LoginModel model)
        {
            this.CheckField(nameof(model.Username), model.Username);
            this.CheckField(nameof(model.Password), model.Password);
        }

        private void CheckField(string fieldName, string fieldValue)
        {
            if (string.IsNullOrEmpty(fieldValue) || string.IsNullOrWhiteSpace(fieldValue))
            {
                throw new ArgumentNullException(null, $"'{fieldName}' should not be null");
            }
        }

        private void Close(ICloseable closable)
        {
            closable.Close();
        }
    }
}