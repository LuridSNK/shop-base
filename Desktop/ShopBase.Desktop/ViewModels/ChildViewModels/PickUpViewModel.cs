﻿namespace ShopBase.Desktop.ViewModels.ChildViewModels
{
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using GalaSoft.MvvmLight;
    using ShopBase.Desktop.ViewModels.Interfaces;
    using ShopBase.Domain.Dto;
    using ShopBase.Messaging.Dto;
    using ShopBase.Messaging.Http.Interfaces;

    public class PickUpViewModel : ViewModelBase, IPickUpViewModel
    {
        //private readonly IPrinter _printer;

        private readonly IApplicationHttpClient _client;

        public PickUpViewModel(/*IPrinter printer,*/ IApplicationHttpClient client)
        {
            //_printer = printer;
            _client = client;
        }

        public PickUpOrderDto SelectedOrder { get; set; }

        public ObservableCollection<PickUpOrderDto> Orders { get; set; } = new ObservableCollection<PickUpOrderDto>
                                                                               {
                                                                                   new
                                                                                   PickUpOrderDto
                                                                                       {
                                                                                           Id = Guid.Empty,
                                                                                           CustomerFullname = "memem",
                                                                                           DeliveryType = "Me",
                                                                                           CustomerEmail = "bbb",
                                                                                           CustomerPhone = "sbsdb",
                                                                                           Status = "aada",
                                                                                           Containers = new List<ContainerDto> 
                                                                                           {
                                                                                                                new ContainerDto
                                                                                                                    {
                                                                                                                        Name = "123",
                                                                                                                        Cell = "A"
                                                                                                                    }
                                                                                                            }
                                                                                       }
                                                                               };
    }
}