﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ShopBase.Desktop.Interfaces
{
    using System.Windows;

    public interface IDialog
    {
        object DataContext { get; set; }
        bool? DialogResult { get; set; }
        Window Owner { get; set; }

        void Close();

        bool? Show();
    }

    public interface IDialogService
    {

    }
}
