﻿namespace ShopBase.Desktop.Interfaces
{
    using System.Configuration;
    using ShopBase.Desktop.Settings;
    using ShopBase.Domain;

    public interface IApplicationSettings
    {
        [UserScopedSetting]
        string Token { get; set; }

        [UserScopedSetting]
        DisplayOptions DisplayOptions { get; set; }

        [UserScopedSetting]
        User CurrentUser { get; set; }

        [UserScopedSetting]
        string LastUsedViewModel { get; set; }
    }
}