﻿namespace ShopBase.Desktop.Interfaces
{
    public interface ICollapsible
    {
        void Collapse();

        void Expand();
    }
}