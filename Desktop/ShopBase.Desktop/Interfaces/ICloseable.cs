﻿namespace ShopBase.Desktop.Interfaces
{
    public interface ICloseable
    {
        void Close();
    }
}