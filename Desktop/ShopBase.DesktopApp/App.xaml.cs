﻿namespace ShopBase.DesktopApp
{
    using System;
    using System.Threading;
    using System.Windows;

    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        private static readonly Mutex Mutex = new Mutex(true, "{8F6F0AC4-B9A1-45fd-A8CF-72F04E6BDE8F}");

        public App()
        {
        }

        protected override async void OnStartup(StartupEventArgs e)
        {
            this.CheckForMultipleInstances();
            base.OnStartup(e);
        }

        protected override void OnExit(ExitEventArgs e)
        {
            Mutex?.ReleaseMutex();
            base.OnExit(e);
        }

        private void CheckForMultipleInstances()
        {
            if (Mutex.WaitOne(TimeSpan.Zero, true))
            {
                Mutex.ReleaseMutex();
            }
            else
            {
                MessageBox.Show("Приложение может быть запущено только в одном экземпляре!");
                Current.Shutdown();
            }
        }
    }

}
