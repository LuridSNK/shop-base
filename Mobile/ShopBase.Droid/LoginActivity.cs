﻿namespace ShopBase.Droid
{
    using Android.App;
    using Android.OS;
    using Android.Views;
    using Android.Widget;
    using ShopBase.Droid.Views;

    [Activity(Label = "LoginActivity")]
    public class LoginActivity : Activity, ILoginView
    {
        private Button _btnSignIn;
        private EditText _inpLogin;
        private EditText _inpPassword;
        private TextView _txtErrMessage;

        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            _btnSignIn = FindViewById<Button>(Droid.Resource.Id.btnSignIn);
            _inpLogin = FindViewById<EditText>(Droid.Resource.Id.inpLogin);
            _inpPassword = FindViewById<EditText>(Droid.Resource.Id.inpPassword);
            _txtErrMessage = FindViewById<TextView>(Droid.Resource.Id.txtErrorMsg);
            _txtErrMessage.Visibility = ViewStates.Invisible;

            _txtErrMessage.TextChanged += (s, a) =>
                {
                    OnErrorMessageChanged(a.Text.ToString());
                };

            _btnSignIn.Click += (s, a) =>
                {
                    /*
                     Send http request to http://my-ip-address/api/auth/
                     with LoginViewModel json message and 
                     store JWT-token OR handle the error message                     
                     */
                };

            // _inpLogin.TextChanged += (s, a) => { };
            // _inpPassword.TextChanged += (s, a) => { };

            // Create your application here
        }

        private void OnErrorMessageChanged(string newText)
        {
            if (string.IsNullOrEmpty(newText))
            {
                _txtErrMessage.Visibility = ViewStates.Invisible;
                return;
            }

            _txtErrMessage.Visibility = ViewStates.Visible;
        }
    }
}