﻿namespace ShopBase.Domain
{
    using System;

    public class Address
    {
        public long Id { get; set; }

        public Guid ClientId { get; set; }

        public string Zip { get; set; } // 344016

        public string Country { get; set; } // Russia

        public string Region { get; set; } // Rostov obl.

        public string District { get; set; }

        public string Locality { get; set; } // Rostov-na-Donu

        public string City { get; set; } // Rostov-na-Donu

        public string Street { get; set; } // Vyatskaya

        public string House { get; set; }

        public string Block { get; set; }

        public string Flat { get; set; }

        public string Subway { get; set; } // none

        public virtual Client Client { get; set; }

        public override string ToString()
        {
            var addr = string.Empty;
            addr += string.IsNullOrEmpty(Zip) ? string.Empty : $"{Zip}, ";
            addr += string.IsNullOrEmpty(Country) ? string.Empty : $"{Country}, ";
            addr += string.IsNullOrEmpty(District) ? string.Empty : $"{District}, ";
            addr += string.IsNullOrEmpty(Region) ? string.Empty : $"{Region}, ";
            addr += string.IsNullOrEmpty(City) ? string.Empty : $"{City}, ";
            addr += string.IsNullOrEmpty(Street) ? string.Empty : $"{Street}, ";
            addr += string.IsNullOrEmpty(House) ? string.Empty : $"{House}, ";
            addr += string.IsNullOrEmpty(Block) ? string.Empty : $"{Block}, ";
            addr += string.IsNullOrEmpty(Flat) ? string.Empty : $"{Flat}, ";
            addr += string.IsNullOrEmpty(Subway) ? string.Empty : $"Метро: {Subway}";
            return addr;
        }

        /// <summary>
        ///     Returns the string with represented format
        /// </summary>
        /// <param name="format">
        /// <para/>
        /// > "short"  represents only <see cref="Address.Street"/> and <see cref="Address.House"/>
        /// <para/>
        /// > "short-list" represents only <see cref="Address.Street"/> and <see cref="Address.House"/>, but as list
        /// <para/>
        /// > "list" represents all fields of <see cref="Address"/> (like with '<see cref="Address.ToString()"/>') but as list
        /// </param>
        /// <returns></returns>
        public string ToString(string format)
        {
            var addr = string.Empty;
            if (format == "short")
            {
                addr += string.IsNullOrEmpty(Street) ? string.Empty : $"{Street}, ";
                addr += string.IsNullOrEmpty(House) ? string.Empty : $"{House}";
                addr += string.IsNullOrEmpty(Block) ? string.Empty : $"{Block}";
                addr += string.IsNullOrEmpty(Flat) ? string.Empty : $"{Flat}";
                return addr;
            }
            if (format == "short-list")
            {
                addr += string.IsNullOrEmpty(Street) ? string.Empty : $"{Street}\n";
                addr += string.IsNullOrEmpty(House) ? string.Empty : $"{House}";
                addr += string.IsNullOrEmpty(Block) ? string.Empty : $"{Block}";
                addr += string.IsNullOrEmpty(Flat) ? string.Empty : $"{Flat}";
                return addr;
            }
            if (format == "list")
            {
                addr += string.IsNullOrEmpty(Zip) ? string.Empty : $"{Zip}\n";
                addr += string.IsNullOrEmpty(Country) ? string.Empty : $"{Country}\n";
                addr += string.IsNullOrEmpty(District) ? string.Empty : $"{District}\n";
                addr += string.IsNullOrEmpty(Region) ? string.Empty : $"{Region}\n";
                addr += string.IsNullOrEmpty(City) ? string.Empty : $"{City}\n";
                addr += string.IsNullOrEmpty(Street) ? string.Empty : $"{Street}\n";
                addr += string.IsNullOrEmpty(House) ? string.Empty : $"{House}";
                addr += string.IsNullOrEmpty(Block) ? string.Empty : $"{Block}";
                addr += string.IsNullOrEmpty(Flat) ? string.Empty : $"{Flat}";
                addr += string.IsNullOrEmpty(Subway) ? string.Empty : $"Метро: {Subway}";
                return addr;
            }

            return this.ToString();
        }
    }
}
