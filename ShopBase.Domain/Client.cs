﻿namespace ShopBase.Domain
{
    using System;
    using System.Collections.Generic;

    public class Client
    {
        public Client()
        {
            Addresses = new HashSet<Address>();
        }

        public Guid Id { get; set; }

        public string Name { get; set; }

        public string Surname { get; set; }

        public string LastName { get; set; }

        public float Discount { get; set; }

        /// <summary>
        ///     Represents gender (sex) of the client. 'null' = unidentified, '0' is FEMALE, '1' is MALE
        /// </summary>
        public bool? Gender { get; set; } // currently 2 : female / 1 : male

        public DateTime DateOfBirth { get; set; }

        public string Phone { get; set; }

        public string Email { get; set; }

        public bool IsEmployee { get; set; }

        public int OrderCount { get; set; }

        public int RejectedCount { get; set; }

        public DateTime CreatedAt { get; set; }

        public DateTime ChangedAt { get; set; }
        
        public virtual ICollection<Address> Addresses { get; set; }

        public string GetFullName()
        {
            return $"{Surname} {Name} {LastName}";
        }
    }
}
