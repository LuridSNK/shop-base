﻿namespace ShopBase.Domain
{
    using System;

    public class Site
    {
        public Guid Id { get; set; }

        public string Name { get; set; }
    }
}