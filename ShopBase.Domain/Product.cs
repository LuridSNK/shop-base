﻿namespace ShopBase.Domain
{
    using System;

    public class Product
    {
        public Guid Id { get; set; }

        public Guid? OrderId { get; set; }

        public string Sku { get; set; }

        public string Name { get; set; }

        public decimal Price { get; set; }

        public int Amount { get; set; }
    }
}
