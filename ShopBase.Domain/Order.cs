﻿namespace ShopBase.Domain
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using ShopBase.Shared.Enums;

    public class Order
    {
        public Guid Id { get; set; }
        
        public string PublicNumber { get; set; }

        public string InnerNumber { get; set; }

        public DateTime OrderedAt { get; set; }

        public string Comment { get; set; }
        
        public OrderState State { get; set; }

        public DateTime ChangedAt { get; set; }

        public DeliveryServiceType ServiceType { get; set; } // same? v

        public string DeliveryType { get; set; }             // same? ^

        public decimal DeliveryCost { get; set; }

        public DateTime ApprovedAt { get; set; }

        public PaymentType PaymentType { get; set; }

        public string PostamatNumber { get; set; }

        public string Approver { get; set; }

        public decimal ComissionCost { get; set; }

        public decimal AfterDiscountCost { get; set; }

        public decimal TotalCost { get; set; }
        
        public int ProductsCount => this.Products.Sum(p => p.Amount);

        public decimal ProductsCost => this.Products.Sum(p => p.Price * p.Amount);
        
        public Guid SiteId { get; set; }

        public Guid ClientId { get; set; }

        public long AddressId { get; set; }

        public Guid? CreatorId { get; set; }


        public ICollection<Product> Products { get; set; }

        public ICollection<Container> Containers { get; set; }

        public virtual Address Address { get; set; }

        public virtual Client Client { get; set; }

        public virtual User Creator { get; set; }

        public virtual Site Site { get; set; }

        public virtual Logistics Logistics { get; set; }
    }

    public class Logistics
    {
        public Guid OrderId { get; set; }

        public DateTime From { get; set; }

        public DateTime To { get; set; }

        public string Subway { get; set; }

        public string SubwayBranch { get; set; }

        public string SubwayColor { get; set; }
    }

}