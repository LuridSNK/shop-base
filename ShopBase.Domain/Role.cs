﻿namespace ShopBase.Domain
{
    using System.ComponentModel.DataAnnotations.Schema;

    [Table("AppRoles")]
    public class Role
    {
        public int Id { get; set; }

        public string Name { get; set; }
    }
}
