﻿namespace ShopBase.Domain
{
    using System;

    public class Container
    {
        public string Name { get; set; }

        public string Cell { get; set; }

        public Guid OrderId { get; set; }


        public virtual Order Order { get; set; }
    }
}
