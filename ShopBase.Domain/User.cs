﻿namespace ShopBase.Domain
{
    using System;
    using System.ComponentModel.DataAnnotations.Schema;

    [Table("AppUsers")]
    public class User
    {
        public Guid Id { get; set; }

        public string Name { get; set; }

        public string PasswordHash { get; set; }

        public int RoleId { get; set; }

        public virtual Role Role { get; set; }
    }
}
