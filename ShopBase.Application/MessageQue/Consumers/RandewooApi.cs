﻿namespace ShopBase.Application.MessageQue.Consumers
{
    using System;
    using System.Net.Http;
    using System.Threading;
    using System.Threading.Tasks;
    using MassTransit;
    using ShopBase.Domain;

    public class RandewooApiConsumer : IConsumer<Product>
    {
        public async Task Consume(ConsumeContext<Product> context)
        {
            var client = new HttpClient();
            var cancellationTokenSource = new CancellationTokenSource(TimeSpan.FromSeconds(10));
            var result = await client.GetAsync("http://randewoo1232.ru/api/faulty", cancellationTokenSource.Token);
            result.EnsureSuccessStatusCode();
        }
    }
}
