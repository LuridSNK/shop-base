﻿namespace ShopBase.Application.Exceptions
{
    using System;

    public class AlreadyExistsException : Exception
    {
        public AlreadyExistsException(string entityName, object key)
            : base($"Объект \"{entityName}\" с ключом '{key}' уже существует.")
        {
        }
    }
}
