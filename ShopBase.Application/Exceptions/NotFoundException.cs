﻿namespace ShopBase.Application.Exceptions
{
    using System;

    public class NotFoundException : Exception
    {
        public NotFoundException(string entityName, object key)
            : base($"Объект \"{entityName}\" не был найден по указанному ключу ({key}).")
        {
        }
    }
}
