﻿namespace ShopBase.Application.Exceptions
{
    using System;

    public class ApiCallException : Exception
    {
        public ApiCallException(string message)
            : base(message)
        {
        }
    }
}
