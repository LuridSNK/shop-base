﻿namespace ShopBase.Application.Models.Order.GetOrderQuery
{
    using System;

    using MediatR;

    using ShopBase.Domain;

    public class GetOrderQuery : IRequest<Order>
    {
        public Guid OrderId { get; set; }
    }
}
