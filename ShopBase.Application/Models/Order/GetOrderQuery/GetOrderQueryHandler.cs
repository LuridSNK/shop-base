﻿namespace ShopBase.Application.Models.Order.GetOrderQuery
{
    using System.Threading;
    using System.Threading.Tasks;

    using MediatR;

    using ShopBase.Domain;

    public class GetOrderQueryHandler : IRequestHandler<GetOrderQuery, Order>
    {
        public async Task<Order> Handle(GetOrderQuery request, CancellationToken cancellationToken)
        {
            // todo: find order by Id in database
            return null;
        }
    }
}
