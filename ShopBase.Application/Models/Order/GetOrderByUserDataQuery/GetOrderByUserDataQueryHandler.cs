﻿namespace ShopBase.Application.Models.Order.GetOrderByUserDataQuery
{
    using System.Collections.Generic;
    using System.Data;
    using System.Threading;
    using System.Threading.Tasks;
    using MediatR;
    using ShopBase.Application.Exceptions;
    using ShopBase.Domain;

    public class GetOrderByUserDataQueryHandler : IRequestHandler<GetOrderByUserDataQuery, IEnumerable<Order>>
    {
        private readonly IDbConnection _connection;

        public GetOrderByUserDataQueryHandler(IDbConnection connection) => _connection = connection;

        public Task<IEnumerable<Order>> Handle(GetOrderByUserDataQuery request, CancellationToken cancellationToken)
        {
            switch (request.DataType)
            {
                case "phone":
                    return null;

                case "email":
                    return null;

                case "order":
                    return null;

                default:
                    throw new NotFoundException(nameof(Order), request.Value);
            }
        }
    }
}
