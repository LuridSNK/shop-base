﻿
namespace ShopBase.Application.Models.Order.GetOrderByUserDataQuery
{
    using System.Collections.Generic;
    using MediatR;
    using ShopBase.Domain;

    public class GetOrderByUserDataQuery : IRequest<IEnumerable<Order>>
    {
        public string DataType { get; set; }

        public string Value { get; set; }
    }
}
