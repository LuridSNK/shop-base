﻿namespace ShopBase.Application.Models.Order.ChangeOrderStateCommand
{
    using System;
    using MediatR;

    public class ChangeOrderStateCommand : IRequest
    {
        public Guid OrderId { get; set; }
    }
}
