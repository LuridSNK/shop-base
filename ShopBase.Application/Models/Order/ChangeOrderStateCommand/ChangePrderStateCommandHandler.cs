﻿namespace ShopBase.Application.Models.Order.ChangeOrderStateCommand
{
    using System.Data;
    using System.Threading;
    using System.Threading.Tasks;
    using Dapper;
    using MediatR;
    
    public class ChangeOrderStateCommandHandler : IRequestHandler<ChangeOrderStateCommand>
    {
        private readonly IDbConnection _connection;

        private readonly IMediator _mediator;

        public ChangeOrderStateCommandHandler(IDbConnection connection, IMediator mediator)
        {
            _connection = connection;
            _mediator = mediator;
        }

        public async Task<Unit> Handle(ChangeOrderStateCommand request, CancellationToken cancellationToken)
        {
            await _connection.ExecuteAsync(
                sql: "ShipOrderInWMS",
                param: new { request.OrderId },
                commandType: CommandType.StoredProcedure);
            return Unit.Value;
        }
    }
}
