﻿namespace ShopBase.Application.Models.Order.ChangeOrderStateCommand
{
    using System;
    using System.IO;
    using MediatR;

    public class OrderShippedEvent : IRequest<Stream>
    {
        public Guid OrderId { get; set; }
    }
}
