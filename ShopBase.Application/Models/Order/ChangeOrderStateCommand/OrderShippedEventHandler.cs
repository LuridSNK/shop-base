﻿namespace ShopBase.Application.Models.Order.ChangeOrderStateCommand
{
    using System.Data;
    using System.IO;
    using System.Linq;
    using System.Threading;
    using System.Threading.Tasks;
    using Castle.Core.Configuration;
    using Dapper;
    using Domain;
    using MediatR;
    using ShopBase.Application.Exceptions;
    using ShopBase.External.Handlers.Interfaces;
    using ShopBase.External.Models.Requests;

    public class OrderShippedEventHandler : IRequestHandler<OrderShippedEvent, Stream>
    {
        private readonly IDeliveryServiceHandlerFactory _dsFactory;

        private readonly IDbConnection _connection;

        private readonly IConfiguration _config;

        public OrderShippedEventHandler(
            IDeliveryServiceHandlerFactory httpClientFactory, 
            IDbConnection connection,
            IConfiguration config)
        {
            _dsFactory = httpClientFactory;
            _connection = connection;
            _config = config;
        }

        public async Task<Stream> Handle(OrderShippedEvent notification, CancellationToken cancellationToken)
        {
            Order order;
            Credentials credentials;
            using (var multiple = await _connection.QueryMultipleAsync(
                                      sql: "GetOrderByIdToSendToDs",
                                      param: new { notification.OrderId },
                                      commandType: CommandType.StoredProcedure))
            {
                order = await multiple.ReadSingleOrDefaultAsync<Order>();
                // todo: fill order
                credentials = await multiple.ReadSingleOrDefaultAsync<Credentials>();
            }

            var client = _dsFactory.BuildFor(order.ServiceType);

            if (credentials.Token == null)
            {
                await client.Authenticate(credentials.Login, credentials.Password);
            }

            var createdOrdersResp = await client.CreateOrders(
                new[]
                    {
                        new OrderCreationRequest
                            {
                                CustomerName = order.Client.GetFullName(),
                                CustomerEmail = order.Client.Email,
                                CustomerPhone = order.Client.Phone,
                                Description = "Parfume",
                                Id = order.Id,
                                Number = order.PublicNumber,
                                PostamatNumber = order.PostamatNumber,
                                SiteId = order.SiteId,
                                PaymentSum = 0,
                                PrepaidSum = 0
                            }
                    });
            if (!createdOrdersResp.IsSuccessful)
            {
                throw new ApiCallException(createdOrdersResp.ErrorMessage);
            }


            var streamResponse = await client.DownloadLabels(new LabelRequest { ExternalIdList = createdOrdersResp.Value.Select(r => r.ExternalId) });
            if (!streamResponse.IsSuccessful)
            {
                throw new ApiCallException(createdOrdersResp.ErrorMessage);
            }

            return streamResponse.Value;
        }

        private class Credentials
        {
            public string Login { get; set; }

            public string Password { get; set; }

            public string Token { get; set; }
        }
    }
}
