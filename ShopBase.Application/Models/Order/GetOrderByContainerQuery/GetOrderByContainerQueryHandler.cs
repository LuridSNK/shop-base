﻿namespace ShopBase.Application.Models.Order.GetOrderByContainerQuery
{
    using System;
    using System.Data;
    using System.Linq;
    using System.Text.RegularExpressions;
    using System.Threading;
    using System.Threading.Tasks;
    using Dapper;
    using MediatR;

    using ShopBase.Application.Exceptions;
    using ShopBase.Domain;

    public class GetOrderByContainerQueryHandler : IRequestHandler<GetOrderByContainerQuery, Order>
    {
        private readonly IDbConnection _connection;

        public GetOrderByContainerQueryHandler(IDbConnection connection)
        {
            _connection = connection;
        }

        public async Task<Order> Handle(GetOrderByContainerQuery request, CancellationToken cancellationToken)
        {
            using (_connection)
            {
                using (var multiple = await _connection.QueryMultipleAsync(
                                          sql: "OrderByContainerNumberView_v2",
                                          param: new { request.ContainerNumber },
                                          commandType: CommandType.StoredProcedure))
                {
                    var order = await multiple.ReadFirstOrDefaultAsync<Order>();
                    if (order == null) throw new NotFoundException(nameof(Order), request.ContainerNumber);

                    order.Client = (await multiple.ReadAsync<Client>()).SingleOrDefault(c => c.Id == order.ClientId);
                    order.Address = (await multiple.ReadAsync<Address>()).SingleOrDefault(a => a.Id == order.AddressId);
                    order.Products = (await multiple.ReadAsync<Product>()).Where(p => p.OrderId == order.Id).ToList();
                    order.Logistics = await multiple.ReadSingleOrDefaultAsync<Logistics>();
                    order.Containers = (await multiple.ReadAsync<Container>()).Where(c => c.OrderId == order.Id).ToList();
                    order.Site = (await multiple.ReadAsync<Site>()).SingleOrDefault(u => u.Id == order.SiteId);
                    //order.Creator = await multiple.ReadSingleOrDefaultAsync<User>();

                    // костыль на очистку json объекта в валидный формат (прости меня господи)
                    order.PostamatNumber = Regex.Replace(order.PostamatNumber, "(\"?)(({|})?)", string.Empty);
                    order.PostamatNumber = Regex.Replace(order.PostamatNumber, @".*ПВЗ:(\s?)", string.Empty);

                    return order;
                }
            }
        }
    }
}
