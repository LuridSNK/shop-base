﻿namespace ShopBase.Application.Models.Order.GetOrderByContainerQuery
{
    using MediatR;

    using ShopBase.Domain;

    public class GetOrderByContainerQuery : IRequest<Order>
    {
        public string ContainerNumber { get; set; }
    }
}
