﻿namespace ShopBase.Application.Models.Order.GetOrderByContainerQuery
{
    using FluentValidation;

    public class GetOrderByContainerValidator : AbstractValidator<GetOrderByContainerQuery>
    {
        public GetOrderByContainerValidator()
        {
            this.RuleFor(o => o.ContainerNumber)
                .NotEmpty()
                .NotNull();
        }
    }
}
