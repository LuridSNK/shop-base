﻿namespace ShopBase.Application.Models.Product.CreateProductCommand
{
    using MediatR;
    using ShopBase.Domain;

    public class ProductCreatedEvent : INotification
    {
        public Product Product { get; set; }
    }
}
