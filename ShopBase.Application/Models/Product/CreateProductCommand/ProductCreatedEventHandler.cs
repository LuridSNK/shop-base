﻿namespace ShopBase.Application.Models.Product.CreateProductCommand
{
    using System;
    using System.Threading;
    using System.Threading.Tasks;
    using MassTransit;
    using MediatR;
    using ShopBase.Shared;

    public class ProductCreatedEventHandler : INotificationHandler<ProductCreatedEvent>
    {
        private readonly IBusControl _busControl;

        public ProductCreatedEventHandler(IBusControl busControl)
        {
            this._busControl = busControl;
        }

        public async Task Handle(ProductCreatedEvent notification, CancellationToken cancellationToken)
        {
            var endpoint = await _busControl.GetSendEndpoint(new Uri($"{Routes.RabbitMQ}/product-feed"));
            await endpoint.Send(notification.Product, cancellationToken);
        }
    }
}

