﻿namespace ShopBase.Application.Models.Product.CreateProductCommand
{
    using System.Collections.Generic;
    using System.Threading;
    using System.Threading.Tasks;
    using MediatR;
    using ShopBase.Domain;

    public class CreateProductCommandHandler : IRequestHandler<CreateProductCommand>
    {
        private readonly IList<Product> _products;

        private readonly IMediator _mediator;

        #warning @tntd: there is a stub for dbcontext which is '_products'
        public CreateProductCommandHandler(IMediator mediator)
        {
            _products = new List<Product>();
            _mediator = mediator;
        }

        public async Task<Unit> Handle(CreateProductCommand request, CancellationToken cancellationToken)
        {
            var product = new Product
                              {
                                  Name = request.Name,
                                  Sku = request.Sku,
                                  Price = request.Price,
                              };
            _products.Add(product);
            // SaveChanges

            await _mediator.Publish(new ProductCreatedEvent { Product = product }, cancellationToken);
            return Unit.Value;
        }
    }
}
