﻿namespace ShopBase.Application.Models.Product.CreateProductCommand
{
    using MediatR;

    public class CreateProductCommand : IRequest
    {
        public string Name { get; set; }

        public string Sku { get; set; }

        public decimal Price { get; set; }
    }
}
