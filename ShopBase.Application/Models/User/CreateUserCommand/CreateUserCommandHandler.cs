﻿namespace ShopBase.Application.Models.User.CreateUserCommand
{
    using System.Threading;
    using System.Threading.Tasks;
    using MediatR;
    using Microsoft.EntityFrameworkCore;
    using ShopBase.Application.Exceptions;
    using ShopBase.Domain;
    using ShopBase.Persistence.DbContext;

    public class CreateUserCommandHandler : IRequestHandler<CreateUserCommand>
    {
        private ApplicationDbContext _context;

        public CreateUserCommandHandler(ApplicationDbContext context)
        {
            _context = context;
        }

        public async Task<Unit> Handle(CreateUserCommand request, CancellationToken cancellationToken)
        {
            var exists = await _context.Users.AnyAsync(u => u.Name == request.Name, cancellationToken);
            if (exists) throw new AlreadyExistsException(nameof(User), request.Name);
            var user = new User { Name = request.Name, PasswordHash = request.Password, RoleId = request.RoleId };
            await _context.Users.AddAsync(user, cancellationToken);
            await _context.SaveChangesAsync(cancellationToken);
            return Unit.Value;
        }
    }
}
