﻿namespace ShopBase.Application.Models.User.CreateUserCommand
{
    using MediatR;

    public class CreateUserCommand : IRequest
    {
        public string Name { get; set; }

        public string Password { get; set; }

        public int RoleId { get; set; }
    }
}
