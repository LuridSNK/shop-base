﻿namespace ShopBase.Application.Models.User.CreateUserCommand
{
    using FluentValidation;

    class CreateUserCommandValidator : AbstractValidator<CreateUserCommand>
    {
        public CreateUserCommandValidator()
        {
            RuleFor(e => e.Name)
                .NotEmpty()
                .NotNull();

            RuleFor(e => e.Password)
                .NotEmpty()
                .NotNull();

            RuleFor(e => e.RoleId)
                .NotEqual(0)
                .NotEmpty()
                .NotNull();
        }
    }
}
