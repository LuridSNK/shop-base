﻿namespace ShopBase.Application.Models.User.ChangeUserPasswordCommand
{
    using System.Threading;
    using System.Threading.Tasks;
    using MediatR;
    using ShopBase.Domain;
    using ShopBase.Persistence.DbContext;

    public class ChangeUserPasswordCommandHandler : IRequestHandler<ChangeUserPasswordCommand>
    {
        private readonly ApplicationDbContext _context;

        public ChangeUserPasswordCommandHandler(ApplicationDbContext context) => _context = context;

        public async Task<Unit> Handle(ChangeUserPasswordCommand request, CancellationToken cancellationToken)
        {
            var user = new User { Id = request.UserId };
            var entry = _context.Users.Attach(user);
            user.PasswordHash = request.NewPassword;
            entry.Property(u => u.PasswordHash).IsModified = true;
            await _context.SaveChangesAsync(cancellationToken);

            return Unit.Value;
        }
    }
}
