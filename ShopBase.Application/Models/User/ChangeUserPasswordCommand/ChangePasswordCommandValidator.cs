﻿namespace ShopBase.Application.Models.User.ChangeUserPasswordCommand
{
    using FluentValidation;

    public class ChangePasswordCommandValidator : AbstractValidator<ChangeUserPasswordCommand>
    {
        public ChangePasswordCommandValidator()
        {
            RuleFor(c => c.UserId)
                .NotNull()
                .NotEmpty();

            RuleFor(c => c.NewPassword)
                .NotNull()
                .NotEmpty()
                .MinimumLength(6);
        }
    }
}
