﻿namespace ShopBase.Application.Models.User.ChangeUserPasswordCommand
{
    using System;
    using MediatR;

    public class ChangeUserPasswordCommand : IRequest
    {
        public Guid UserId { get; set; }

        public string NewPassword { get; set; }
    }
}
