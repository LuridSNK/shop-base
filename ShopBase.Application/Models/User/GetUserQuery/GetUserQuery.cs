﻿namespace ShopBase.Application.Models.User.GetUserQuery
{
    using System;
    using MediatR;
    using Domain;

    public class GetUserQuery : IRequest<User>
    {
        public Guid UserId { get; set; }
    }
}
