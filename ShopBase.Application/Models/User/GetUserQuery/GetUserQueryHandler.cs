﻿namespace ShopBase.Application.Models.User.GetUserQuery
{
    using System.Threading;
    using System.Threading.Tasks;
    using MediatR;
    using ShopBase.Application.Exceptions;
    using ShopBase.Domain;
    using ShopBase.Persistence.DbContext;

    public class GetUserQueryHandler : IRequestHandler<GetUserQuery, User>
    {
        private readonly ApplicationDbContext _context;

        public GetUserQueryHandler(ApplicationDbContext context) => _context = context;

        public async Task<User> Handle(GetUserQuery request, CancellationToken cancellationToken)
        {
            var user = await _context.Users.FindAsync(request.UserId);
            if (user == null)
            {
                throw new NotFoundException(nameof(User), request.UserId);
            }

            return user;
        }
    }
}
