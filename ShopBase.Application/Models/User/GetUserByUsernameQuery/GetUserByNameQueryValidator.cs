﻿namespace ShopBase.Application.Models.User.GetUserByUsernameQuery
{
    using FluentValidation;

    public class GetUserByNameQueryValidator : AbstractValidator<GetUserByNameQuery>
    {
        public GetUserByNameQueryValidator()
        {
            this.RuleFor(a => a.Username)
                .MaximumLength(40)
                .NotEmpty()
                .NotNull();

            this.RuleFor(a => a.Username)
                .MaximumLength(100)
                .NotEmpty()
                .NotNull();
        }
    }
}
