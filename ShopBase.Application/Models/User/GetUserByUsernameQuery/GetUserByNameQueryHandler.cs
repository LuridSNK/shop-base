﻿namespace ShopBase.Application.Models.User.GetUserByUsernameQuery
{
    using System.Threading;
    using System.Threading.Tasks;
    using MediatR;
    using Microsoft.EntityFrameworkCore;
    using ShopBase.Application.Exceptions;
    using ShopBase.Domain;
    using ShopBase.Persistence.DbContext;

    public class GetUserByNameQueryHandler : IRequestHandler<GetUserByNameQuery, User>
    {
        private readonly ApplicationDbContext _context;
        //private readonly IDbConnection _connection;

        public GetUserByNameQueryHandler(/*IDbConnection connection*/ApplicationDbContext dbContext)
        {
            _context = dbContext;
            //_connection = connection;
        }

        public async Task<User> Handle(GetUserByNameQuery request, CancellationToken cancellationToken)
        {
            var entity = await _context.Users
                             .Include(u => u.Role)
                             .FirstOrDefaultAsync(u => u.Name == request.Username, cancellationToken);
            if (entity == null) throw new NotFoundException(nameof(Models.User), request.Username);
            return entity;

            ///using (_connection)
            ///{
            ///    var user = await _connection.QuerySingleOrDefaultAsync<User>(
            ///                   @"SELECT * FROM AppUsers JOIN AppRoles 
            ///                     ON AppUsers.RoleId = AppRoles.Id 
            ///                     WHERE Username = @Username",
            ///                   new { Username = request.Username });
            ///    if (user == null)
            ///    {
            ///        throw new NotFoundException(nameof(request.Username), request.Username);
            ///    }
            ///    return user;
            ///}
        }
    }
}