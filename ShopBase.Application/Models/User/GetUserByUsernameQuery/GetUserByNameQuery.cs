﻿namespace ShopBase.Application.Models.User.GetUserByUsernameQuery
{
    using MediatR;
    using ShopBase.Domain;

    public class GetUserByNameQuery : IRequest<User>
    { 
        public string Username { get; set; }

        public string Password { get; set; }
    }
}
