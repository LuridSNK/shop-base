﻿namespace ShopBase.Application.Models.Role.GetRolesQuery
{
    using System.Collections.Generic;
    using ShopBase.Domain;

    public class RoleList
    {
        public IEnumerable<Role> Roles { get; set; }
    }
}
