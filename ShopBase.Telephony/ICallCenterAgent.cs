﻿namespace ShopBase.Telephony
{
    public interface ICallCenterAgent
    {
        int Id { get; set; }

        string Name { get; set; }

        AgentStatus Status { get; set; }

        void MakeACall(string phoneNumber);
        
    }

    public interface ICallCenterSupervisor : ICallCenterAgent
    {
        
    }

    public interface ICallCenterAdministrator : ICallCenterSupervisor
    {
        
    }

    public enum AgentStatus
    {
        Online, // в сети, отсчет времени пошел
        Available, // доступен для звонка
        InTalk,
        OnABriefing, // на инструктаже/обучении 
        AtTheBreak, // на перерыве (посчет времени)
        Offline // вне сети, отсчет времени завершен
    }
}

