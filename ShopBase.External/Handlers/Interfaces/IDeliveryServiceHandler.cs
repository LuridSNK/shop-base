﻿namespace ShopBase.External.Handlers.Interfaces
{
    using System.IO;
    using System.Threading.Tasks;

    using ShopBase.External.Models.Requests;
    using ShopBase.External.Models.Responses;
    using ShopBase.Messaging.Http.Impl;

    public interface IDeliveryServiceHandler
    {
        Task<string> Authenticate(string login, string password);

        Task<ApiResponse<OrderCreationResponse[]>> CreateOrders(OrderCreationRequest[] orderRequests);

        Task<ApiResponse<OrderGroupingResponse[]>> GroupOrders(OrderGroupingRequest groupingRequest);

        Task<ApiResponse<OrderUngroupingResponse>> UngroupOrders(OrderUngroupingRequest ungroupingRequest);

        Task<ApiResponse<OrderCancellationResponse>> CancelOrder(OrderCancellationRequest orderRequest);

        Task<ApiResponse<OrderCancellationResponse>> CancelOrderFromGroup(OrderCancellationRequest orderRequest);

        Task<ApiResponse<CourierCallResponse>> CallCourier();

        Task<ApiResponse<OrderTrackingResponse>> TrackOrder(OrderTrackingRequest trackingRequest);

        Task<ApiResponse<LabelDownloadResponse>> DownloadLabels(LabelRequest labelRequest, string pathToSaveLabels);

        Task<ApiResponse<Stream>> DownloadLabels(LabelRequest labelRequest);

        Task<ApiResponse<CitiesListResponse>> ListCities();

        Task<ApiResponse<TerminalListResponse>> ListTerminals();

    }
}