﻿namespace ShopBase.External.Handlers.Interfaces
{
    using ShopBase.Shared.Enums;

    public interface IDeliveryServiceHandlerFactory
    {
        IDeliveryServiceHandler BuildFor(DeliveryServiceType dsType);
    }
}
