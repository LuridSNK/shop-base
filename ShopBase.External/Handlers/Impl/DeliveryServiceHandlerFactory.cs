﻿namespace ShopBase.External.Handlers.Impl
{
    using System;

    using ShopBase.External.Handlers.Interfaces;
    using ShopBase.External.PickPoint.HttpClient;
    using ShopBase.Shared.Enums;

    public class DeliveryServiceHandlerFactory : IDeliveryServiceHandlerFactory
    {
        public IDeliveryServiceHandler BuildFor(DeliveryServiceType dsType)
        {
            switch (dsType)
            {
                case DeliveryServiceType.Undefined:
                    return null;
                case DeliveryServiceType.PickPoint:
                    return new PickPointHandler(new PickPointClient());
                case DeliveryServiceType.CDEK:
                    return null;
                case DeliveryServiceType.Dalli:
                    return null;
                case DeliveryServiceType.RussianPost:
                    return null;
                default:
                    throw new ArgumentOutOfRangeException(nameof(dsType), dsType, string.Empty);
            }
        }
    }
}
