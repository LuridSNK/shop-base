﻿namespace ShopBase.External.Handlers.Impl
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Threading.Tasks;

    using ShopBase.External.Handlers.Interfaces;
    using ShopBase.External.Models.Requests;
    using ShopBase.External.Models.Responses;
    using ShopBase.External.PickPoint.HttpClient;
    using ShopBase.External.PickPoint.Models;
    using ShopBase.Messaging.Http.Impl;

    public class PickPointHandler : IDeliveryServiceHandler
    {
        private readonly PickPointClient _client;

        public PickPointHandler(PickPointClient client)
        {
            _client = client;
        }

        public async Task<string> Authenticate(string login, string password)
        {
           return await _client.Authenticate(login, password);
        }

        public async Task<ApiResponse<OrderCreationResponse[]>> CreateOrders(OrderCreationRequest[] orderRequests)
        {
            try
            {
                var sendings = orderRequests.Select(o => new PickPointSending
                                                             {
                                                                 EDTN = Guid.NewGuid().ToString("N"),
                                                                 IKN = this.GetIKNFromSiteId(o.SiteId),
                                                                 Invoice = new PickPointInvoice
                                                                               {
                                                                                   SenderCode = o.Number,
                                                                                   Email = o.CustomerEmail,
                                                                                   RecipientName = o.CustomerName,
                                                                                   MobilePhone = o.CustomerPhone,
                                                                                   Sum = o.PaymentSum,
                                                                                   PrepaymentSum = o.PrepaidSum,
                                                                                   SenderCity = new PickPointSenderCity { RegionName = "Московская обл.", CityName = "Москва" },
                                                                                   PayType = 1,
                                                                                   PostageType = o.PaymentSum - o.PrepaidSum == 0 ? 10001 : 10003,
                                                                                   GettingType = 101,
                                                                                   PostamatNumber = o.PostamatNumber,
                                                                                   DeliveryMode = 1,
                                                                                   Description = "Парфюмерия",
                                                                               }
                                                             });

                var result = await _client.RegisterShipment(new PickPointShipment { Sendings = sendings });

                var createdSendings = result.CreatedSendings.Select(s => new OrderCreationResponse
                                                       {
                                                           Id = orderRequests.First(o => o.Number == s.SenderCode).Id,
                                                           CreatedAt = DateTime.Now, 
                                                           ExternalId = s.InvoiceNumber,
                                                           TrackNumber = s.InvoiceNumber
                                                       });

                return new ApiResponse<OrderCreationResponse[]> { IsSuccessful = true, Content = createdSendings, ErrorMessage = string.Empty };
            }
            catch (Exception e)
            {
                return new ApiResponse<OrderCreationResponse[]> { IsSuccessful = false, Content = null, ErrorMessage = e.Message };
            }
        }

        public async Task<ApiResponse<OrderGroupingResponse[]>> GroupOrders(OrderGroupingRequest groupingRequest)
        {
            try
            {
                var groups = groupingRequest.Sendings.GroupBy(o => o.SiteId);

                var resps = new List<OrderGroupingResponse>();
                foreach (var g in groups)
                {
                    var groupingResult = new OrderGroupingResponse();
                    var registry = new PickPointRegistry { Invoices = g.Select(o => o.ExternalId) };
                    var resp = await _client.CreateARegistry(registry);
                    groupingResult.GroupIdentifier = resp.Numbers.First();
                    groupingResult.Group.AddRange(g);
                    resps.Add(groupingResult);
                }

                return new ApiResponse<OrderGroupingResponse[]> { IsSuccessful = true, Content = resps, ErrorMessage = string.Empty };
            }
            catch (Exception e)
            {
                return new ApiResponse<OrderGroupingResponse[]> { IsSuccessful = false, Content = null, ErrorMessage = e.Message };
            }
        }

        public async Task<ApiResponse<OrderUngroupingResponse>> UngroupOrders(OrderUngroupingRequest ungroupingRequest)
        {
            throw new System.NotImplementedException();
        }

        public async Task<ApiResponse<OrderCancellationResponse>> CancelOrder(OrderCancellationRequest orderRequest)
        {
            try
            {
                var result = await _client.RemoveShipmentFromRegistry(new ShipmentRemovalContext
                                                                          {
                                                                              InvoiceNumber = orderRequest.ExternalId,
                                                                              IKN = this.GetIKNFromSiteId(orderRequest.SiteId)
                                                                          });
                return new ApiResponse<OrderCancellationResponse>
                           {
                               IsSuccessful = result.ErrorCode == 0,
                               ErrorMessage = $"{result.ErrorCode}, {result.ErrorMessage}",
                               Content = new OrderCancellationResponse { IsDeleted = result.ErrorCode == 0 }
                           };
            }
            catch (Exception e)
            {
                return new ApiResponse<OrderCancellationResponse>
                           {
                               IsSuccessful = false,
                               ErrorMessage = e.Message,
                               Content = null
                           };
            }
        }
        
        public async Task<ApiResponse<OrderCancellationResponse>> CancelOrderFromGroup(OrderCancellationRequest orderRequest)
        {
            try
            {
                var result = await _client.RemoveShipmentFromRegistry(new ShipmentRemovalContext
                                                                          {
                                                                              InvoiceNumber = orderRequest.ExternalId,
                                                                              IKN = this.GetIKNFromSiteId(orderRequest.SiteId)
                                                                          });
                return new ApiResponse<OrderCancellationResponse>
                           {
                               IsSuccessful = result.ErrorCode == 0,
                               ErrorMessage = $"{result.ErrorCode}, {result.ErrorMessage}",
                               Content = new OrderCancellationResponse { IsDeleted = result.ErrorCode == 0 }
                           };
            }
            catch (Exception e)
            {
                return new ApiResponse<OrderCancellationResponse>
                           {
                               IsSuccessful = false,
                               ErrorMessage = e.Message,
                               Content = null
                           };
            }
        }


        public async Task<ApiResponse<CourierCallResponse>> CallCourier()
        {
            throw new System.NotImplementedException();
        }

        public async Task<ApiResponse<OrderTrackingResponse>> TrackOrder(OrderTrackingRequest trackingRequest)
        {
            var ppTrack = new PickPointTrack
                              {
                                  InvoiceNumber = trackingRequest.ExternalId
                              };
            try
            {
                var resp = await _client.TrackShipment(ppTrack);
                var sbStatus = this.ConvertPickPointStateToShopBase(resp.State);
                return new ApiResponse<OrderTrackingResponse>
                           {
                               IsSuccessful = true,
                               Content = new OrderTrackingResponse { Status = sbStatus}, 
                               ErrorMessage = resp.Comment
                           };
            }
            catch (Exception e)
            {
                return new ApiResponse<OrderTrackingResponse>
                           {
                               IsSuccessful = false,
                               Content = null,
                               ErrorMessage = e.Message
                           };
            }
        }

        public async Task<ApiResponse<LabelDownloadResponse>> DownloadLabels(LabelRequest labelRequest, string pathToSaveLabels)
        {
            try
            {
                await _client.DownloadLabels(labelRequest.ExternalIdList, pathToSaveLabels);
                return new ApiResponse<LabelDownloadResponse>
                           {
                               IsSuccessful = true, Content = new LabelDownloadResponse { SavedLabelsPath = pathToSaveLabels }, ErrorMessage = string.Empty
                           };
            }
            catch (Exception e)
            {
                return new ApiResponse<LabelDownloadResponse>
                           {
                               IsSuccessful = false, Content = null, ErrorMessage = e.Message
                           };
            }
        }

        public async Task<ApiResponse<Stream>> DownloadLabels(LabelRequest labelRequest)
        {
            try
            {
                var stream = await _client.DownloadLabelsStream(labelRequest.ExternalIdList);
                return new ApiResponse<Stream> {Content = stream, IsSuccessful = true };

            }
            catch (Exception e)
            {
                return new ApiResponse<Stream>
                           {
                               IsSuccessful = false,
                               Content = null,
                               ErrorMessage = e.Message
                           };
            }
        }

        public async Task<ApiResponse<CitiesListResponse>> ListCities()
        {
            throw new System.NotImplementedException();
        }

        public async Task<ApiResponse<TerminalListResponse>> ListTerminals()
        {
            try
            {
                var resp = await _client.GetListOfTerminals();
                var terminals = new TerminalListResponse
                                    {
                                        Terminals = resp.Select(t => new Terminal
                                                     {
                                                         Id = t.Id.ToString(),
                                                         Name = t.Name,
                                                         Region = t.RegionName
                                                     })
                                    };
                return new ApiResponse<TerminalListResponse> { IsSuccessful = true, Content = terminals, ErrorMessage = string.Empty };
            }
            catch (Exception e)
            {
                return new ApiResponse<TerminalListResponse> { IsSuccessful = true, Content = null, ErrorMessage = e.Message };
            }
        }

        public string GetIKNFromSiteId(Guid siteId)
        {
            var dict = new Dictionary<Guid, string>
                           {
                               { Guid.Parse("48E0A097-E897-4ABD-8B98-2B3FFC6C405B"), "9990552122" }, // rdw
                               { Guid.Parse("B2803EA7-0BFA-4AA7-B459-F2A997974312"), "9990552112" }, // emb
                               { Guid.Parse("9EED7FEB-F8E8-46F5-B515-F66B644B7718"), "9990552132" }, // edt
                               { Guid.Empty, "9990003041" }
                           };

            return dict[siteId];
        }

        private int ConvertPickPointStateToShopBase(int state)
        {
            switch (state)
            {
                case 111:
                    return 1; // created
                case 113:
                    return 2; // in progress
                default:
                    return 11; // handling 
            }
        }
    }
}
