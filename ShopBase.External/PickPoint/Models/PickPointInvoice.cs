﻿namespace ShopBase.External.PickPoint.Models
{
    using System.Collections.Generic;

    public class PickPointInvoice
    {
        /// <summary>
        /// Номер заказа магазина. строка 50 символов
        /// </summary>
        public string SenderCode { get; set; }

        /// <summary>
        /// Описание типа вложимого, Пример: «Одежда и Обувь». строка 200 символов, обязательное поле
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// Имя получателя, строка 60 символов, обязательное поле
        /// </summary>
        public string RecipientName { get; set; }

        /// <summary>
        /// Номер постамата, вида XXXX-XXX, обязательное поле
        /// </summary>
        public string PostamatNumber { get; set; }

        /// <summary>
        ///  Один номер Мобильного телефона получателя для SMS, желателен формат номера: 7/8хххХХХххХХ, при этом допускаются разделители (, ), -, пробелы обязательное поле
        /// </summary>
        public string MobilePhone { get; set; }

        /// <summary>
        /// Email, строка 256 символов
        /// </summary>
        public string Email { get; set; }

        /// <summary>
        /// Номер консультанта, строка 50 символов
        /// </summary>
        public string ConsultantNumber { get; set; }

        /// <summary>
        /// Вид отправления, обязательное поле
        ///  10001 Стандарт. Оплаченный заказ. При этом поле 'Sum = 0' 
        ///  10003 Стандарт НП Отправление с наложенным платежом. Поле 'Sum > 0'
        /// </summary>
        public int PostageType { get; set; }

        /// <summary>
        /// Тип сдачи отправления, обязательное поле
        /// 101	«вызов курьера» - Наш курьер приедет к вам за отправлениями.
        /// 102	«в окне приема СЦ» - Вы привезете отправления в филиал PickPoint
        /// </summary>
        public int GettingType { get; set; }

        /// <summary>
        /// always 1
        /// </summary>
        public int PayType { get; set; }

        /// <summary>
        ///Сумма к оплате. обязательное поле См.п. «PostageType»  
        /// </summary>
        public decimal Sum { get; set; }

        /// <summary>
        /// Сумма предоплаты, если платеж уже был внесен частично
        /// </summary>
        public decimal PrepaymentSum { get; set; }

        /// <summary>
        /// Ставка НДС по товару:
        /// "Vat": 18 (НДС 18%)
        /// "Vat": 10 (НДС 10%)
        /// "Vat": 0 (НДС 0%)
        /// "Vat": null (без НДС)
        /// </summary>
        public int DeliveryVat { get; set; }

        /// <summary>
        /// Сумма сервисного сбора с НДС, если берется с физ. лица
        /// </summary>
        public int DeliveryFee { get; set; }


        public int InsuareValue { get; set; }

        public int DeliveryMode { get; set; }

        /// <summary>
        /// Город сдачи отправлений в PickPoint
        /// </summary>
        public PickPointSenderCity SenderCity { get; set; }

        //public PickPointAddressInfo ClientReturnAddress { get; set; }
        
        //public PickPointAddressInfo UnclaimedReturnAddress { get; set; }

        //public List<PickPointPlace> Places { get; set; }
    }
}