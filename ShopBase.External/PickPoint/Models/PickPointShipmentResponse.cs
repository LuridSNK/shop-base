﻿namespace ShopBase.External.PickPoint.Models
{
    using System.Collections.Generic;

    public class PickPointShipmentResponse
    {
        public IEnumerable<CreatedSending> CreatedSendings { get; set; }

        public IEnumerable<RejectedSending> RejectedSendings { get; set; }
    }

    public class CreatedSending
    {
        public string EDTN { get; set; }

        public string InvoiceNumber { get; set; }

        public string SenderCode { get; set; }

        public IEnumerable<PickPointPlace> Places { get; set; }
    }

    public class RejectedSending
    {
        public string EDTN { get; set; }

        public string SenderCode { get; set; }

        public string ErrorCode { get; set; }

        public string ErrorMessage { get; set; }
    }
}

