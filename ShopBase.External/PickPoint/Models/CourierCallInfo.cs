﻿namespace ShopBase.External.PickPoint.Models
{
    using System;

    using Newtonsoft.Json;

    public class PickPointCourierCallInfo
    {
        public Guid SessionId { get; set; }

        public string Ikn { get; set; }

        public string SenderCode { get; set; }

        public string City { get; set; }

        [JsonProperty("city_id")]
        public int CityId { get; set; }

        [JsonProperty("city_owner_id")]
        public int CityOwnerId { get; set; }

        public string Address { get; set; }

        public string Fio { get; set; }

        public string Phone { get; set; }

        public DateTime Date { get; set; }

        public int TimeStart { get; set; }

        public int TimeEnd { get; set; }

        public int Number { get; set; }

        public string Weight { get; set; }

        public string Comment { get; set; }
    }
}
