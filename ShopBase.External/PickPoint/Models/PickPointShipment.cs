namespace ShopBase.External.PickPoint.Models
{
    using System;
    using System.Collections.Generic;

    public class PickPointShipment
    {
        public Guid SessionId { get; set; }

        public IEnumerable<PickPointSending> Sendings { get; set; }
    }
}