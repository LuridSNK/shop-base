﻿namespace ShopBase.External.PickPoint.Models
{
    using System.Collections.Generic;

    public class PickPointPostamat
    {
        public string Address { get; set; }

        public string AmountTo { get; set; }

        public string BuildingType { get; set; }

        public int Card { get; set; }

        public int Cash { get; set; }

        public int CitiId { get; set; }

        public string CitiName { get; set; }

        public int CitiOwnerId { get; set; }

        public string ClosingComment { get; set; }

        public object ClosingDateFrom { get; set; }

        public object ClosingDateTo { get; set; }

        public object Comment { get; set; }

        public string CountryIso { get; set; }

        public string CountryName { get; set; }

        public string FileI0 { get; set; }

        public string FileI1 { get; set; }

        public string FileI2 { get; set; }

        public bool Fitting { get; set; }

        public string House { get; set; }

        public int Id { get; set; }

        public string InDescription { get; set; }

        public string IndoorPlace { get; set; }

        public double Latitude { get; set; }

        public int LocationType { get; set; }

        public double Longitude { get; set; }

        public bool MapAllowed { get; set; }

        public string MaxBoxSize { get; set; }

        public string MaxSize { get; set; }

        public string MaxWeight { get; set; }

        public string Metro { get; set; }

        public List<string> MetroArray { get; set; }

        public string MovingComment { get; set; }

        public string MovingDateFrom { get; set; }

        public string MovingDateTo { get; set; }

        public string Name { get; set; }

        public string Number { get; set; }

        public bool Opening { get; set; }

        public string OutDescription { get; set; }

        public int OwnerId { get; set; }

        public string OwnerName { get; set; }

        public int PostCode { get; set; }

        public string Region { get; set; }

        // public List<object> ReturnTypes { get; set; }
        public bool Returning { get; set; }

        public int Status { get; set; }

        public string Street { get; set; }

        public string TypeTitle { get; set; }

        public bool WidgetAllowed { get; set; }

        public bool WorkHourly { get; set; }

        public string WorkTime { get; set; }

        public string WorkTimeSms { get; set; }
    }
}
