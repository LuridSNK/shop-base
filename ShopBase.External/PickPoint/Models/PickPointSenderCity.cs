﻿namespace ShopBase.External.PickPoint.Models
{
    public class PickPointSenderCity
    {
        public string CityName { get; set; }

        public string RegionName { get; set; }
    }
}
