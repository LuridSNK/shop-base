﻿namespace ShopBase.External.PickPoint.Models
{
    public class PickPointCity
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public string NameEng { get; set; }

        public int OwnerId { get; set; }

        public string RegionName { get; set; }
    }

}
