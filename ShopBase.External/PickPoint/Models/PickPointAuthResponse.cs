﻿namespace ShopBase.External.PickPoint.Models
{
    using System;

    public class PickPointAuthResponse
    {
        public Guid SessionId { get; set; }

        public string ErrorMessage { get; set; }
    }
}
