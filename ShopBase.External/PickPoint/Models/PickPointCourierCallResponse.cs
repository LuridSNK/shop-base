namespace ShopBase.External.PickPoint.Models
{
    public class PickPointCourierCallResponse
    {
        public bool CourierRequestRegistred { get; set; }

        public string OrderNumber { get; set; }

        public string ErrorMessage { get; set; }
    }
}