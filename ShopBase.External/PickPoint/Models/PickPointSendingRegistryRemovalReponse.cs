﻿namespace ShopBase.External.PickPoint.Models
{
    public class PickPointSendingRegistryRemovalReponse
    {
        public int ErrorCode { get; set; }

        public string ErrorMessage { get; set; }
    }
}