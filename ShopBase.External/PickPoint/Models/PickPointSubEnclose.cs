﻿namespace ShopBase.External.PickPoint.Models
{
    public class PickPointSubEnclose
    {
        public string ProductCode { get; set; }

        public string GoodsCode { get; set; }

        public string Name { get; set; }

        public string Price { get; set; }

        public string Quantity { get; set; }

        public string Vat { get; set; }

        public string Description { get; set; }
    }
}