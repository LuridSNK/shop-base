﻿namespace ShopBase.External.PickPoint.Models
{
    public class PickPointAddressInfo
    {
        public string CityName { get; set; }

        public string RegionName { get; set; }

        public string Address { get; set; }

        public string Fio { get; set; }

        public string PostCode { get; set; }

        public string Organisation { get; set; }

        public string PhoneNumber { get; set; }

        public string Comment { get; set; }
    }
}