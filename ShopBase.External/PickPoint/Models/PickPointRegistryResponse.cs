﻿namespace ShopBase.External.PickPoint.Models
{
    using System.Collections.Generic;

    public class PickPointRegistryResponse
    {
        public IEnumerable<string> Numbers { get; set; }

        public string ErrorMessage { get; set; }
    }
}
