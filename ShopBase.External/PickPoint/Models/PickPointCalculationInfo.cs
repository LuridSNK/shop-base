﻿namespace ShopBase.External.PickPoint.Models
{
    using System;
    using System.Collections.Generic;

    public class PickPointCalculationInfo
    {
        public Guid SessionId { get; set; }

        public long Ikn { get; set; }

        public long InvoiceNumber { get; set; }

        public string FromCity { get; set; }

        public string FromRegion { get; set; }

        public string PtNumber { get; set; }

        public long EncloseCount { get; set; }

        public int Length { get; set; }

        public int Depth { get; set; }

        public int Width { get; set; }

        public int Weight { get; set; }
    }

    public class PickPointCalculationInfoResponse
    {
        public long InvoiceNumber { get; set; }

        public string DPMin { get; set; }
        public string DPMinPriority { get; set; }

        public string DPMax { get; set; }
        public string DPMaxPriority { get; set; }

        private IEnumerable<PickPointCalculationInfoServices> Serivices { get; set; }

        public int Zone { get; set; }

        public int ErrorCode { get; set; }

        public string ErrorMessage { get; set; }
    }

    public class PickPointCalculationInfoServices
    {
        public string DeliveryMode { get; set; }

        public string Name { get; set; }

        public decimal Tariff { get; set; }

        public decimal NDS { get; set; }

        public int Discount { get; set; }
    }

}
