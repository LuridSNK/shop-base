﻿namespace ShopBase.External.PickPoint.Models
{
    public class PickPointSending
    {
        public string EDTN { get; set; }

        public string IKN { get; set; }

        public PickPointInvoice Invoice { get; set; }
    }
}