﻿namespace ShopBase.External.PickPoint.Models
{
    using Newtonsoft.Json;

    public class PickPointTrackResponse
    {
        public string BarCode { get; set; }

        [JsonProperty("ChangeDT")]
        public string ChangeDate { get; set; }

        public string Comment { get; set; }

        public int State { get; set; }

        public string StateMessage { get; set; }
    }
}