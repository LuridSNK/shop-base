﻿namespace ShopBase.External.PickPoint.Models
{
    using System.Collections.Generic;

    public class PickPointPlace
    {
        public string BarCode { get; set; }

        public float CellStorageType { get; set; }

        public float Width { get; set; }

        public float Height { get; set; }

        public float Depth { get; set; }

        public List<PickPointSubEnclose> SubEncloses { get; set; } 
    }
}