﻿namespace ShopBase.External.PickPoint.Models
{
    public class PickPointSendingRemovalReponse
    {
        public bool Result { get; set; }

        public int ErrorCode { get; set; }

        public string Error { get; set; }
    }
}