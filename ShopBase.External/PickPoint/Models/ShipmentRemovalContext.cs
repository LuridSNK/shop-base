namespace ShopBase.External.PickPoint.Models
{
    using System;

    public class ShipmentRemovalContext
    {
        public Guid SessionId { get; set; }

        public string IKN { get; set; }

        public string InvoiceNumber { get; set; }
    }
}