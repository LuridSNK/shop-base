﻿namespace ShopBase.External.PickPoint.Models
{
    using System;

    public class PickPointTrack
    {
        public Guid SessionId { get; set; }

        public string InvoiceNumber { get; set; }
    }
}
