﻿namespace ShopBase.External.PickPoint.Models
{
    using System;
    using System.Collections.Generic;

    public class PickPointRegistry
    {
        public Guid SessionId { get; set; }

        public string CityName { get; set; }

        public string RegionName { get; set; }

        public string DeliveryPoint { get; set; }

        public IEnumerable<string> Invoices { get; set; }
    }
}
