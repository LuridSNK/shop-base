﻿namespace ShopBase.External.PickPoint.HttpClient
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Net;
    using System.Net.Http;
    using System.Text;
    using System.Threading.Tasks;
    using ShopBase.External.PickPoint.Models;
    using ShopBase.Messaging.Http.Impl;
    using ShopBase.Shared;
    using ShopBase.Shared.Extensions;

    public class PickPointClient 
    {
        /// <summary>
        /// Id сессии ТК
        /// </summary>
        private Guid _sessionId;


        public async Task<string> Authenticate(string login, string password)
        {
            var content = new StringContent(
                new { Login = login, Password = password }.SerializeToJson(), 
                Encoding.UTF8, 
                "application/json");

            var response = await ApplicationHttpClient.HttpClient.PostAsync($"{ExternalAPIs.PickPoint}/login", content);
            response.EnsureSuccessStatusCode();
            var objResponse = await response.ReadAsJsonAsync<PickPointAuthResponse>();
            
            if (objResponse.SessionId == Guid.Empty)
            {
                throw new ArgumentNullException(null, "Авторизация не удалась." + objResponse.ErrorMessage);
            }

            this._sessionId = objResponse.SessionId;
            return objResponse.SessionId.ToString();
        }

        /// <summary>
        /// Зарегистрировать отправление в PickPoint
        /// </summary>
        public async Task<PickPointShipmentResponse> RegisterShipment(PickPointShipment shipment)
        {
            var response = await ApplicationHttpClient.HttpClient.PostAsync(
                               $"{ExternalAPIs.PickPoint}/createShipment",
                               new StringContent(
                                   shipment.SerializeToJson(), 
                                   Encoding.UTF8, 
                                   "application/json"));
            response.EnsureSuccessStatusCode();
            var respondedObj = await response.ReadAsJsonAsync<PickPointShipmentResponse>();
            if (!respondedObj.CreatedSendings.Any())
            {
                throw new ArgumentNullException(null, "Отправления не были созданы в ТК");
            }

            return respondedObj;
        }

        /// <summary>
        /// Создать регистр в PickPoint с списком отправок
        /// </summary>
        /// <param name="registry">
        /// Реестр
        /// </param>
        public async Task<PickPointRegistryResponse> CreateARegistry(PickPointRegistry registry)
        {
            var response = await ApplicationHttpClient.HttpClient.PostAsync(
                               $"{ExternalAPIs.PickPoint}/makeReestrNumber",
                               new StringContent(
                                   registry.SerializeToJson(), 
                                   Encoding.UTF8, 
                                   "application/json"));
            response.EnsureSuccessStatusCode();

            var objResponse = await response.ReadAsJsonAsync<PickPointRegistryResponse>();
            if (!objResponse.Numbers.Any())
            {
                throw new ArgumentNullException(null, "Реестр не был создан. " + objResponse.ErrorMessage);
            }

            return objResponse;
        }


        /// <summary>
        ///  Запрос на отслеживание отправления
        /// </summary>
        /// <param name="track"></param>
        /// <returns></returns>
        public async Task<PickPointTrackResponse> TrackShipment(PickPointTrack track)
        {
            var response = await ApplicationHttpClient.HttpClient.PostAsync(
                               $"{ExternalAPIs.PickPoint}/trackSending",
                               new StringContent(
                                   track.SerializeToJson(), 
                                   Encoding.UTF8, 
                                   "application/json"));

            response.EnsureSuccessStatusCode();
            var objResponse = await response.ReadAsJsonAsync<IEnumerable<PickPointTrackResponse>>();
            var lastStatus = objResponse.FirstOrDefault();
            if (lastStatus == null)
            {
                throw new ArgumentNullException(null, "Статус неопределен!");
            }

            return lastStatus;
        }


        /// <summary>
        /// Скачать и сохранить этикетки в PDF
        /// </summary>
        /// <returns> </returns>
        public async Task DownloadLabels(IEnumerable<string> invoiceNumbers, string path)
        {
            var n = 0;
            var newPath = string.Empty;
            while (File.Exists(path))
            {
                n++;
                newPath = path.Replace(".", $"({n}).");
            }

            var input = await this.DownloadLabelsStream(invoiceNumbers);

            using (Stream output = File.OpenWrite(newPath))
            using (input)
            {
                input?.CopyTo(output);
            }
        }


        public async Task<Stream> DownloadLabelsStream(IEnumerable<string> invoiceNumbers)
        {
            var request = (HttpWebRequest)WebRequest.Create($"{ExternalAPIs.PickPoint}/makelabel");
            request.Method = "POST";
            request.ContentType = "application/json;";
            using (var streamWriter = new StreamWriter(request.GetRequestStream()))
            {
                streamWriter.Write(new { _sessionId, Invoices = invoiceNumbers }.SerializeToJson());
                streamWriter.Flush();
                streamWriter.Close();
            }

            var resp = await Task.Factory.StartNew(() => (HttpWebResponse)request.GetResponse());

            if (resp.StatusCode != HttpStatusCode.OK)
            {
                throw new HttpRequestException(resp.StatusCode.ToString());
            }

            using (Stream input = resp.GetResponseStream())
            {
                resp.Close();
                return input;
            }
        }

        /// <summary>
        /// Запрос на удаление отправления из регистра
        /// </summary>
        /// <param name="shipmentToRemove"></param>
        /// <returns></returns>
        public async Task<PickPointSendingRegistryRemovalReponse> RemoveShipmentFromRegistry(ShipmentRemovalContext shipmentToRemove)
        {
            var response = await ApplicationHttpClient.HttpClient.PostAsync(
                               $"{ExternalAPIs.PickPoint}/removeInvoiceFromReestr",
                               new StringContent(
                                   shipmentToRemove.SerializeToJson(),
                                   Encoding.UTF8,
                                   "application/json"));

            response.EnsureSuccessStatusCode();
            var objResponse = await response.ReadAsJsonAsync<PickPointSendingRegistryRemovalReponse>();
            if (objResponse.ErrorCode != 0)
            {
                throw new Exception("Отправление не может быть удалено! " + objResponse.ErrorMessage);
            }

            return objResponse;
        }


        /// <summary>
        /// Запрос на удаление отправления
        /// </summary>
        /// <param name="shipmentToRemove"></param>
        /// <returns></returns>
        public async Task<PickPointSendingRemovalReponse> RemoveShipment(ShipmentRemovalContext shipmentToRemove)
        {
            var response = await ApplicationHttpClient.HttpClient.PostAsync(
                               $"{ExternalAPIs.PickPoint}/cancelInvoice",
                               new StringContent(
                                   shipmentToRemove.SerializeToJson(), 
                                   Encoding.UTF8, 
                                   "application/json"));
            response.EnsureSuccessStatusCode();
            var objResponse = await response.ReadAsJsonAsync<PickPointSendingRemovalReponse>();
            if (!objResponse.Result)
            {
                throw new Exception("Отправление не может быть удалено! " + objResponse.Error);
            }
            return objResponse;
        }

        /// <summary>
        /// Отправить запрос на вызов курьера
        /// </summary>
        /// <param name="courierData"> Данные для ПП </param>
        /// <returns> Результат вызова курьера </returns>
        public async Task<PickPointCourierCallResponse> CallCourier(PickPointCourierCallInfo courierData)
        {
            var response = await ApplicationHttpClient.HttpClient.PostAsync(
                               $"{ExternalAPIs.PickPoint}/courier",
                               new StringContent(
                                   courierData.SerializeToJson(), 
                                   Encoding.UTF8, 
                                   "application/json"));
            response.EnsureSuccessStatusCode();
            var objResponse = await response.ReadAsJsonAsync<PickPointCourierCallResponse>();
            if (!objResponse.CourierRequestRegistred)
            {
                throw new Exception("Вызов курьера недоступен! Причина: " + objResponse.ErrorMessage);
            }
            return objResponse;
        }

        /// <summary>
        /// Получить список городов
        /// </summary>
        /// <returns></returns>
        public async Task<IEnumerable<PickPointCity>> GetListOfCities()
        {
            var response = await ApplicationHttpClient.HttpClient.GetAsync($"{ExternalAPIs.PickPoint}/citylist");
            response.EnsureSuccessStatusCode();
            return await response.ReadAsJsonAsync<IEnumerable<PickPointCity>>();
        }

        /// <summary>
        ///     Получить список терминалов самовывоза
        /// </summary>
        /// <returns></returns>
        public async Task<IEnumerable<PickPointCity>> GetListOfTerminals()
        {
            var response = await ApplicationHttpClient.HttpClient.GetAsync($"{ExternalAPIs.PickPoint}/postamatlist");
            response.EnsureSuccessStatusCode();
            return await response.ReadAsJsonAsync<IEnumerable<PickPointCity>>();
        }


        /// <summary>
        /// Команда предназначена для получения стоимости доставки. При расчете учитываются следующие ограничения: <br/>
        /// •	габариты указываются общие на все места, <br/>
        /// •	вес по умолчанию считается 1 кг,<br/>
        /// •	рассчитывается только тариф за логистику.
        /// </summary>
        /// <param name="info"></param>
        /// <returns> Класс-описание </returns>
        public async Task<PickPointCalculationInfoResponse> CalculateTariff(PickPointCalculationInfo info)
        {
            var response = await ApplicationHttpClient.HttpClient.PostAsync(
                               $"{ExternalAPIs.PickPoint}/calcTariff",
                               new StringContent(info.SerializeToJson(), Encoding.UTF8, "application/json"));
            response.EnsureSuccessStatusCode();

            return await response.ReadAsJsonAsync<PickPointCalculationInfoResponse>();
        }

       
    }

    public enum PickPointStates
    {
        Registered = 101, // Зарегистрирован

        ReadyForLogistics = 102, // Сформирован для передачи Логисту

        SelfDistribution = 103, // Развоз до ПТ самостоятельно

        ReadyForDispatch = 104, // Сформирован для отправки

        AcceptedByLogistics = 105, // Принят Логистом

        InLogisticsStorage = 106, // На кладовке Логиста

        IssuedOnRoute = 107, // Выдан на маршрут

        IssuedToTheCourier = 108, // Выдано курьеру

        DeliveredInPT = 109, // Доставлено в ПТ

        AcceptedInPT = 110, // Принято в ПТ

        Received = 111, // Получен

        Unclaimed = 112, // Невостребованное

        ReturnedToCustomer = 113, // Возвращено Клиенту

        Rejected = 114, // Отказ

        FormedReturn = 115, // Сформирован возврат

        IssuedToReturn = 116, // Передано на возврат

        IssuedToLogistics = 117, // Передано в логистическую компанию

        IssuedOnINO118, // Передано на ИНО

        AcceptedOnINO = 119, // Принято на ИНО

        WrappedOnINO120, // Затюковано на ИНО

        CourierAssigned = 121, // Назначен курьер

        TransferredToDeliveryDept = 122, // Передано в отдел доставки

        Consolidated = 123 // Сконсолидировано
    }
}