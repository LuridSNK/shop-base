﻿namespace ShopBase.External.Models.Requests
{
    public class OrderTrackingRequest
    {
        public string ExternalId { get; set; }
    }
}