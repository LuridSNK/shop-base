﻿namespace ShopBase.External.Models.Requests
{
    using System;

    public class OrderCreationRequest
    {
        public Guid Id { get; set; }

        public Guid SiteId { get; set; }

        //public DeliveryServiceType ServiceType { get; set; }

        public string Number { get; set; }

        public string Description { get; set; }

        public string CustomerName { get; set; }

        public string CustomerPhone { get; set; }

        public string CustomerEmail { get; set; }

        public decimal PaymentSum { get; set; }

        public decimal PrepaidSum { get; set; }

        public string PostamatNumber { get; set; } // только для pickpoint? nope

        // public CreationContextAddress Address { get; set; }
    }

    public class CreationContextAddress
    {
        public string Zip { get; set; }

        public string Region { get; set; }

        public string City { get; set; }

        public string Street { get; set; }

        public string House { get; set; }

        public string Apts { get; set; }
    }
}
