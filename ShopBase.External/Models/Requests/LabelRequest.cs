﻿namespace ShopBase.External.Models.Requests
{
    using System.Collections.Generic;

    public class LabelRequest
    {
        public IEnumerable<string> ExternalIdList { get; set; }
    }
}