﻿namespace ShopBase.External.Models.Requests
{
    using System;

    public class OrderCancellationRequest
    {
        public Guid InnerOrderId { get; set; }

        public string ExternalId { get; set; }

        public string GroupId { get; set; }

        public Guid SiteId { get; set; }
    }
}