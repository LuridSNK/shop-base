﻿namespace ShopBase.External.Models.Requests
{
    using System;

    public class OrderGroupingRequest
    {
        public GroupingContext[] Sendings { get; set; }
    }

    public class GroupingContext
    {
        public Guid Id { get; set; }

        public string ExternalId { get; set; }

        public Guid SiteId { get; set; }
    }
}