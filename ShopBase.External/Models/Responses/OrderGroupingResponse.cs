﻿namespace ShopBase.External.Models.Responses
{
    using System.Collections.Generic;

    using ShopBase.External.Models.Requests;

    public class OrderGroupingResponse
    {
        public string GroupIdentifier { get; set; }

        public List<GroupingContext> Group { get; set; }
    }
}