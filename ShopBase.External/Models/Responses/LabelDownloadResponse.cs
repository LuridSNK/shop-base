﻿namespace ShopBase.External.Models.Responses
{
    public class LabelDownloadResponse
    {
        public string SavedLabelsPath { get; set; }
    }
}
