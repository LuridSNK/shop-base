﻿namespace ShopBase.External.Models.Responses
{
    using System.Collections.Generic;

    public class TerminalListResponse
    {
        public IEnumerable<Terminal> Terminals { get; set; }
    }

    public class Terminal
    {
        public string Id { get; set; }

        public string Name { get; set; }

        public string Region { get; set; }
    }
}