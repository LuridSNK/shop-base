﻿namespace ShopBase.External.Models.Responses
{
    public class OrderCancellationResponse
    {
        public bool IsDeleted { get; set; }
    }
}