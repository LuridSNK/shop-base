﻿namespace ShopBase.External.Models.Responses
{
    using System;

    public class OrderCreationResponse
    {
        /// <summary>
        ///     Внутренний идентификатор заказа
        /// </summary>
        public Guid Id { get; set; }

        /// <summary>
        ///     Уникальный идентификатор ТК
        /// </summary>
        public string ExternalId { get; set; }

        /// <summary>
        ///     Дата создания
        /// </summary>
        public DateTime CreatedAt { get; set; }

        /// <summary>
        ///     Номер отправления(для письма клиенту)
        /// </summary>
        public string TrackNumber { get; set; }
    }
}
