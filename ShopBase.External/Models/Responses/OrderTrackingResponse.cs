﻿namespace ShopBase.External.Models.Responses
{
    public class OrderTrackingResponse
    {
        public int Status { get; set; }
    }
}