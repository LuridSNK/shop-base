﻿namespace ShopBase.Tests
{
    using System.Linq;
    using System.Threading;
    using System.Threading.Tasks;
    using NUnit.Framework;
    using ShopBase.Application.Exceptions;
    using ShopBase.Application.Models.User.GetUserByUsernameQuery;
    using ShopBase.Persistence.DbContext;
    using ShopBase.Tests.Util;

    public class GetUserByNameQueryTests
    {
        private ApplicationDbContext _context;
        private GetUserByNameQueryHandler _handler;

        private GetUserByNameQuery _correctQuery;
        private GetUserByNameQuery _badQuery;
        private GetUserByNameQuery _noRoleQuery;

        [SetUp]
        public void When_query_handles()
        {
            _context = new QueryTestFixture().Context;
            _handler = new GetUserByNameQueryHandler(_context);
            _correctQuery = new GetUserByNameQuery { Username = "petr", Password = "2345" };
            _badQuery = new GetUserByNameQuery { Username = "4ddhdh", Password = "dr3r3rxd" };
            _noRoleQuery = new GetUserByNameQuery { Username = "vladimir", Password = "3456" };
        }

        [Test]
        public async Task Then_User_should_not_be_null()
        {
            var result = await _handler.Handle(_correctQuery, default(CancellationToken));
            Assert.NotNull(result, $"Username is {result.Name}, {_context.Users.Count()}");
        }

        [Test]
        public void Then_Handler_should_throw_NotFound()
        {
            Assert.ThrowsAsync<NotFoundException>(async () => await _handler.Handle(_badQuery, default(CancellationToken)));
        }

        [Test]
        public void Then_User_with_no_role_will_throw_NotFound()
        {
            Assert.ThrowsAsync<NotFoundException>(async () => await _handler.Handle(_noRoleQuery, default(CancellationToken)));
        }
    }

}
