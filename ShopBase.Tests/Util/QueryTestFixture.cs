﻿namespace ShopBase.Tests.Util
{
    using System;
    using ShopBase.Persistence;
    using ShopBase.Persistence.DbContext;

    class QueryTestFixture : IDisposable
    {
        public ApplicationDbContext Context { get; private set; }

        public QueryTestFixture()
        {
            Context = FakeDbContextFactory.Create();
        }

        public void Dispose()
        {
            FakeDbContextFactory.Destroy(Context);
        }
    }
}
