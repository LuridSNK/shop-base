﻿namespace ShopBase.Recycler
{
    using System.Threading.Tasks;

    using Castle.MicroKernel.Registration;
    using Castle.Windsor;
    using Quartz;
    using Quartz.Impl;

    using Serilog;

    using ShopBase.Scheduling;

    class Program
    {
        static async Task Main(string[] args)
        {
            Log.Logger = new LoggerConfiguration()
                .MinimumLevel.Debug()
                .WriteTo.File(@"logs\Recycler.log", rollingInterval: RollingInterval.Day)
                .CreateLogger();
            IScheduler stdScheduler = await new StdSchedulerFactory().GetScheduler();
            IWindsorContainer container = new WindsorContainer();
            container.Register(Component.For<IScheduler>().Instance(stdScheduler));
            container.Install(new RecyclerInstaller());
            var scheduler = container.Resolve<AppScheduler>();
            scheduler.SetupRecycling();
        }
    }
}
