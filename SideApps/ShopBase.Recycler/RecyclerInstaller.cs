﻿using ShopBase.Scheduling;

namespace ShopBase.Recycler
{
    using Castle.MicroKernel;
    using Castle.MicroKernel.Registration;
    using Castle.MicroKernel.SubSystems.Configuration;
    using Castle.Windsor;
    using Quartz;
    using Quartz.Spi;
    using Serilog;
    using Serilog.Core;
    using ShopBase.Scheduling.Recycler;

    class RecyclerInstaller : IWindsorInstaller
    {
        public void Install(IWindsorContainer container, IConfigurationStore store)
        {
            container.Register(Component.For<IJob>().ImplementedBy<RecyclingJob>());
            container.Register(Component.For<IJobFactory>().ImplementedBy<WindsorJobFactory>().DependsOn(Dependency.OnComponent("container", container.GetType())));
            container.Register(Component.For<ILogger>().ImplementedBy<Logger>());
            container.Register(Component.For<IWindsorContainer>().Instance(container));
        }
    }
}
