﻿namespace ShopBase.Services.Static
{
    using System;
    using System.IO;
    using System.Linq;
    using Serilog;

    public class DirectoryCleaner
    {
        private static readonly ILogger Logger = Log.ForContext<DirectoryCleaner>();

        public static bool DeleteFilesWithinDirectory(string pathToDirectory, string extension = null, bool deleteSubdirectories = false)
        {
            if (!Directory.Exists(pathToDirectory))
            {
                Logger.Warning($"Directory '{pathToDirectory}' does not exist.");
                return false;
            }

            try
            {
                DirectoryInfo directory = new DirectoryInfo(pathToDirectory);
                var files = extension is null ? directory.GetFiles() : directory.GetFiles(extension);
                foreach (var file in files)
                {
                    try
                    {
                        DeleteFile(file.Name);
                    }
                    catch (Exception e)
                    {
                        Logger.Error(e, "{@file}", new { Directory = file.DirectoryName, file.Name });
                    }
                    
                }


                directory.Delete(deleteSubdirectories);
                Logger.Information($"Directory '{directory.Name}' was deleted" + (deleteSubdirectories ? " with folder." : " without folder."));
                return true;
            }
            catch (Exception e)
            {
                Logger.Error(e, pathToDirectory);
                return false;
            }
            
        }
        
        public static bool DeleteFile(string pathToFile)
        {
            var fileName = pathToFile.Split(Path.PathSeparator).Last();
            if (!File.Exists(pathToFile))
            {
                Logger.Warning($"File '{fileName}' does not exist.");
                return false;
            }

            try
            {
                File.Delete(pathToFile);
                Logger.Information($"File '{fileName}' was deleted.");
                return true;
            }
            catch (Exception e)
            {
                Logger.Error(e, pathToFile);
                return false;
            }
        }
    }
}
