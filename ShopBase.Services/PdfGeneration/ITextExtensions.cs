﻿namespace ShopBase.Services.PdfGeneration
{
    using System;
    using System.Collections.Generic;
    using System.IO;

    using iText.Barcodes;
    using iText.IO.Font;
    using iText.IO.Image;
    using iText.Kernel.Font;
    using iText.Kernel.Pdf;
    using iText.Layout.Borders;
    using iText.Layout.Element;

    public static class ITextExtensions
    {
        public static Image GetImage(string path, float? scaleX = null, float? scaleY = null)
        {
            if (!File.Exists(path))
            {
                throw new FileNotFoundException(path);
            }

            var img = new Image(ImageDataFactory.Create(path))
                .Scale(scaleX ?? 0.86f, scaleY ?? 0.86f);
            return img;
        }
        
        public static Image GetBarcode(string code, PdfDocument pdf, float? scaleX = null, float? scaleY = null)
        {
            var barcode = new Barcode128(pdf);
            barcode.SetCode(code);
            barcode.SetAltText(string.Empty);
            return new Image(barcode.CreateFormXObject(pdf)).Scale(scaleX ?? 1.1f, scaleY ?? 1.1f);
        }

        public static PdfFont GetFont(string FontName, string encoding = "CP1251")
        {
            var nameWithoutExtension = FontName.Split('.')[0];
            var fontsLocation = Environment.GetFolderPath(Environment.SpecialFolder.Fonts);
            var fontRegular = FontProgramFactory.CreateFont(Path.Combine(fontsLocation, $"{nameWithoutExtension}.ttf"));
            var regular = PdfFontFactory.CreateFont(fontProgram: fontRegular, encoding: encoding, embedded: true);
            return regular;
        }

        public static Cell NewCell(bool noBorder = true)
        {
            return noBorder ? new Cell().SetBorder(Border.NO_BORDER) : new Cell();
        }
        
        public static Paragraph WithHeader(this Paragraph paragraph, string text, float fontSize, bool isBold = false)
        {
            paragraph.Add(
                isBold ? 
                    new Text(text).SetFontSize(fontSize).SetBold() : 
                    new Text(text).SetFontSize(fontSize));

            return paragraph;
        }

        public static Paragraph WithCollection(this Paragraph paragraph, Dictionary<string, string> collection, bool shouldBoldenPair = true)
        {
            foreach (var item in collection)
            {
                paragraph.Add(new Text($"{item.Key} "));
                paragraph.Add(shouldBoldenPair ? new Text(item.Value).SetBold() : new Text($"{item.Value}\n"));
            }

            return paragraph;
        }
    }

}