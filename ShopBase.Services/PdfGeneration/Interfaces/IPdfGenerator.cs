﻿namespace ShopBase.Services.PdfGeneration.Interfaces
{
    using ShopBase.Domain;
    using ShopBase.Services.PdfGeneration.Impl.OrderForms;

    /// <summary>
    ///     Interface for service that creates document
    /// </summary>
    public interface IPdfGenerator
    {
        /// <summary>
        ///     Building a <see cref="PdfFormBase"/> instance
        /// </summary>
        /// <param name="order"></param>
        /// <returns> A <see cref="PdfFormBase"/> instance </returns>
        PdfFormBase GenerateFor(Order order);
    }
}
