﻿namespace ShopBase.Services.PdfGeneration.Impl.OrderForms
{
    using System;
    using System.IO;
    using iText.Kernel.Colors;
    using iText.Kernel.Pdf;
    using iText.Kernel.Pdf.Canvas.Draw;
    using iText.Layout.Element;
    using iText.Layout.Properties;
    using ShopBase.Domain;
    using ShopBase.Shared;
    using Path = System.IO.Path;

    public abstract class PdfFormBase
    {
        protected readonly Order Order;

        protected float FontSize { get; set; } = 10.75f;

        protected PdfFormBase(Order dto)
        {
            Order = dto;
        }

        public abstract void Create(string pathToSave);
        
        protected Image GetLogoImage(Guid siteId, float? scaleX = null, float? scaleY = null)
        {
            var logoPath = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, $@"Resources\Images\{siteId.FindSiteName()}_logo.png");
            var logoImage = ITextExtensions.GetImage(logoPath);
            return logoImage;
        }

        protected Cell AddSigningFieldCell()
        {
            var cell = ITextExtensions.NewCell();
            SolidLine line = new SolidLine(0.6f);
            line.SetColor(ColorConstants.BLACK);
            LineSeparator ls = new LineSeparator(line).SetWidth(UnitValue.CreatePercentValue(80f))
                .SetHorizontalAlignment(HorizontalAlignment.CENTER).SetMarginTop(40f);
            var signParagraph = new Paragraph("подпись, фамилия и инициалы")
                .SetTextAlignment(TextAlignment.CENTER)
                .SetFontSize(5);

            cell.Add(new Paragraph("\nЗаказ принял(а), комплектность полная, притензий к количеству и внешнему виду товара не имею.\n"))
                .Add(ls)
                .Add(signParagraph);
            return cell;
        }

        protected abstract Paragraph AddOrderInfoParagraph(float? fontSize = null);

        protected virtual Table AddImagesTable(
            PdfDocument pdf,
            IBlockElement firstCell = null)
        {
            var logo = ITextExtensions.GetImage($@"Resources\Images\{Order.Site.Name}_logo.png");
            var barcode = ITextExtensions.GetBarcode(Order.InnerNumber, pdf);
            var imagesTable = new Table(3)
                .AddCell(ITextExtensions.NewCell().Add(firstCell ?? new Paragraph())
                    .SetWidth(UnitValue.CreatePercentValue(26.5f))
                    .SetVerticalAlignment(VerticalAlignment.TOP)
                    .SetHorizontalAlignment(HorizontalAlignment.LEFT))
                .AddCell(ITextExtensions.NewCell()
                    .Add(logo)
                    .SetVerticalAlignment(VerticalAlignment.TOP)
                    .SetHorizontalAlignment(HorizontalAlignment.CENTER))
                .AddCell(ITextExtensions.NewCell()
                    .Add(barcode)
                    .SetWidth(UnitValue.CreatePercentValue(26.5f))
                    .SetVerticalAlignment(VerticalAlignment.TOP)
                    .SetHorizontalAlignment(HorizontalAlignment.RIGHT))
                .UseAllAvailableWidth();
            return imagesTable;
        }

        protected abstract Paragraph AddClientInfoParagraph(float? fontSize = null);

        protected abstract Paragraph AddDeliveryInfoParagraph(float? fontSize = null);

        protected abstract Paragraph AddPaymentInfoParagraph(float? fontSize = null);

        protected abstract Paragraph AddSummaryParagraph(float? fontSize = null);

        protected virtual Table AddProductsTable(
            bool allowSku = true,
            string nameHeader = "",
            string skuHeader = "",
            string amountHeader = "",
            string priceHeader = "")
        {
            var prodTable = (allowSku ? new Table(4) : new Table(3))
                .SetHorizontalAlignment(HorizontalAlignment.CENTER)
                .UseAllAvailableWidth();

            if (allowSku)
            {
                prodTable.AddCell(ITextExtensions.NewCell().Add(new Paragraph(skuHeader).SetBold()));
            }

            prodTable
                .AddCell(ITextExtensions.NewCell().Add(new Paragraph(nameHeader).SetBold()))
                .AddCell(ITextExtensions.NewCell().Add(new Paragraph(amountHeader).SetBold()))
                .AddCell(ITextExtensions.NewCell().Add(new Paragraph(priceHeader).SetBold()));


            foreach (var product in Order.Products)
            {
                if (allowSku)
                {
                    prodTable.AddCell(ITextExtensions.NewCell().Add(new Paragraph($"{product.Sku}")));
                }

                prodTable
                    .AddCell(ITextExtensions.NewCell().Add(new Paragraph(product.Name)))
                    .AddCell(ITextExtensions.NewCell().Add(new Paragraph($"{product.Amount} шт.")))
                    .AddCell(ITextExtensions.NewCell().Add(new Paragraph($"{product.Price} руб.")));
            }

            return prodTable;
        }

        protected string ReplacePathIfSomethingWrong(string path)
        {
            if (string.IsNullOrEmpty(path))
            {
                return Path.Combine(Path.GetTempPath(), Guid.NewGuid().ToString("N"));
            }

            var dir = Path.GetDirectoryName(path);
            var fileName = Path.GetFileNameWithoutExtension(path);
            var fileExt = Path.GetExtension(path);

            for (var i = 1; ; ++i)
            {
                if (!File.Exists(path)) return path;

                path = Path.Combine(dir, $"{fileName}({i}).{fileExt}");
            }
        }
    }
}