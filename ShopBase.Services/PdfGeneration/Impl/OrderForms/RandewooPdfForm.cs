﻿namespace ShopBase.Services.PdfGeneration.Impl.OrderForms
{
    using System.Collections.Generic;
    using iText.Kernel.Events;
    using iText.Kernel.Geom;
    using iText.Kernel.Pdf;
    using iText.Layout;
    using iText.Layout.Element;
    using ShopBase.Domain;
    using ShopBase.Shared.Extensions;

    public class RandewooPdfForm : PdfFormBase
    {
        public RandewooPdfForm(Order dto)
            : base(dto)
        {
        }

        public override void Create(string pathToSave)
        {
            var writer = new PdfWriter(pathToSave);
            var pdf = new PdfDocument(writer);
            pdf.AddEventHandler(PdfDocumentEvent.END_PAGE, new PaginationEventHandler(Order));
            var document = new Document(pdf, PageSize.A4);
            document.SetMargins(23, 28, 35, 35);

            var arial = ITextExtensions.GetFont("Arial");
            document.SetFont(arial).SetFontSize(FontSize);

            var layoutTable = new Table(1)
                .UseAllAvailableWidth()
                .AddCell(ITextExtensions.NewCell().Add(this.AddImagesTable(pdf)));
            layoutTable
                .AddCell(ITextExtensions.NewCell().Add(this.AddOrderInfoParagraph(FontSize)))
                .AddCell(ITextExtensions.NewCell().Add(this.AddClientInfoParagraph(FontSize)))
                .AddCell(ITextExtensions.NewCell().Add(this.AddDeliveryInfoParagraph(FontSize)))
                .AddCell(ITextExtensions.NewCell().Add(this.AddPaymentInfoParagraph(FontSize)))
                .AddCell(ITextExtensions.NewCell().Add(this.AddProductsTable(true, "Артикул товара", "Составляющие заказа", "Кол-во", "Стоимость ед.")))
                .AddCell(ITextExtensions.NewCell().Add(this.AddSummaryParagraph(FontSize)))
                .AddCell(ITextExtensions.NewCell().Add(this.AddSigningFieldCell()));

            document.Add(layoutTable);

            document.Close();
            writer.Dispose();
        }

        protected override Paragraph AddOrderInfoParagraph(float? fontSize = null)
        {
            var orderParagraph = new Paragraph()
                .WithHeader("Информация о заказе", fontSize ?? FontSize + 2, true)
                .WithCollection(new Dictionary<string, string>
                                    {
                                        { "Номер заказа:", Order.PublicNumber },
                                        { "Дата заказа:", $"{Order.OrderedAt:dd.MM.yyyy}" },
                                        { "Статус заказа:", Order.State.GetSpecAttribute() }
                                    });
            return orderParagraph;
        }

        protected override Paragraph AddClientInfoParagraph(float? fontSize = null)
        {
            var clientParagraph = new Paragraph().WithHeader("Информация о клиенте", fontSize ?? FontSize + 2, true)
                .WithCollection(new Dictionary<string, string>
                                    {
                                        { "Email:", Order.Client.Email },
                                        { "ФИО:", Order.Client.GetFullName() },
                                        { "Телефон:", Order.Client.Phone },
                                        { "Адрес:", Order.Address.ToString() },
                                        { "Скидка:", $"{Order.Client.Discount:P}" },
                                        { "Заказов:", $"{Order.Client.OrderCount}" },
                                        { "Возвратов:", $"{Order.Client.RejectedCount}" }
                                    });


            return clientParagraph;
        }

        protected override Paragraph AddDeliveryInfoParagraph(float? fontSize = null)
        {
            var deliveryParagraph = new Paragraph()
                .WithHeader("Информация о доставке", fontSize ?? FontSize + 2, true)
                .WithCollection(new Dictionary<string, string>
                                    {
                                        { "Вид доставки:", Order.DeliveryType },
                                        { "Стоимость доставки:", $"{Order.DeliveryCost} руб." },
                                    });
            return deliveryParagraph;
        }

        protected override Paragraph AddPaymentInfoParagraph(float? fontSize = null)
        {
            var paymentParagraph = new Paragraph()
                .WithHeader("Информация об оплате", fontSize ?? FontSize + 2, true)
                .WithCollection(new Dictionary<string, string>
                                    {
                                        { "Вид оплаты:", Order.PaymentType.GetSpecAttribute() },
                                        { "Стоимость оплаты:", $"{Order.ComissionCost} руб." },
                                        { "Комментарий к заказу:", Order.Comment }
                                    });
            return paymentParagraph;
        }

        protected override Paragraph AddSummaryParagraph(float? fontSize = null)
        {

            var summaryParagraph = new Paragraph()
                .WithHeader($"Итого ({Order.ProductsCount} шт.)", fontSize ?? FontSize + 2, true)
                .WithCollection(
                    new Dictionary<string, string>
                        {
                            { "Стоимость товаров:", $"{Order.ProductsCost} руб." },
                            { "С учетом скидок:", $"{Order.AfterDiscountCost} руб." },
                            { "Стоимость доставки:", $"{Order.DeliveryCost} руб." },
                            { "Стоимость оплаты:", $"{Order.ComissionCost} руб." },
                            { "Итого:", $"{Order.TotalCost} руб." }
                        });
            return summaryParagraph;
        }
    }
}
