﻿namespace ShopBase.Services.PdfGeneration.Impl.OrderForms
{
    using System.Collections.Generic;
    using iText.Kernel.Events;
    using iText.Kernel.Geom;
    using iText.Kernel.Pdf;
    using iText.Layout;
    using iText.Layout.Borders;
    using iText.Layout.Element;
    using iText.Layout.Properties;
    using ShopBase.Domain;
    using ShopBase.Shared.Extensions;

    public class EdetePdfForm : PdfFormBase
    {
        public EdetePdfForm(Order dto)
            : base(dto)
        {
        }

        public override void Create(string pathToSave)
        {
            var writer = new PdfWriter(pathToSave);
            var pdf = new PdfDocument(writer);
            pdf.AddEventHandler(PdfDocumentEvent.END_PAGE, new PaginationEventHandler(Order));
            var document = new Document(pdf, PageSize.A4);
            document.SetMargins(23, 28, 35, 35);

            var arial = ITextExtensions.GetFont("Arial.ttf");
            var fontSize = 10.75f;
            document.SetFont(arial).SetFontSize(fontSize);

            var layoutTable = new Table(1)
                .UseAllAvailableWidth()
                .AddCell(ITextExtensions.NewCell().Add(this.AddImagesTable(pdf, this.AddOrderInfoParagraph(fontSize))).SetBorderBottom(new SolidBorder(0.7f)))
                .AddCell(ITextExtensions.NewCell().Add(this.AddProductsTable(false)))
                .AddCell(ITextExtensions.NewCell().Add(this.AddPaymentInfoParagraph()).SetBorderBottom(new SolidBorder(0.7f)))
                .AddCell(ITextExtensions.NewCell().Add(this.AddSummaryParagraph(fontSize)))
                .AddCell(ITextExtensions.NewCell().Add(this.AddClientInfoParagraph(fontSize)))
                .AddCell(ITextExtensions.NewCell().Add(this.AddDeliveryInfoParagraph(fontSize)).SetBorderBottom(new SolidBorder(0.7f)))
                .AddCell(ITextExtensions.NewCell().Add(this.AddSigningFieldCell()));

            document.Add(layoutTable);
            document.Close();
            writer.Dispose();
        }

        protected override Table AddImagesTable(
            PdfDocument pdf,
            IBlockElement firstCell = null)
        {
            var logo = ITextExtensions.GetImage($@"Resources\Images\{Order.Site.Name}_logo.png");
            var barcode = ITextExtensions.GetBarcode(Order.InnerNumber, pdf);
            var imagesTable = new Table(3)
                .AddCell(ITextExtensions.NewCell().Add(logo)
                    .SetVerticalAlignment(VerticalAlignment.TOP)
                    .SetHorizontalAlignment(HorizontalAlignment.LEFT))
                .AddCell(ITextExtensions.NewCell()
                    .Add(firstCell)
                    .SetVerticalAlignment(VerticalAlignment.TOP)
                    .SetHorizontalAlignment(HorizontalAlignment.CENTER))
                .SetTextAlignment(TextAlignment.CENTER)
                .AddCell(ITextExtensions.NewCell()
                    .Add(barcode)
                    .SetVerticalAlignment(VerticalAlignment.TOP)
                    .SetHorizontalAlignment(HorizontalAlignment.RIGHT))
                .UseAllAvailableWidth();
            return imagesTable;
        }

        protected override Paragraph AddOrderInfoParagraph(float? fontSize = null)
        {
            var orderInfoParagraph = new Paragraph()
                .WithHeader($"Заказ №{Order.PublicNumber} от {Order.OrderedAt:dd:MM:yyyy}", FontSize + 4)
                .WithHeader($"Статус: {Order.State.GetSpecAttribute()} ({Order.ApprovedAt:dd:MM:yyyy hh:mm})", FontSize);
            return orderInfoParagraph;
        }

        protected override Paragraph AddClientInfoParagraph(float? fontSize = null)
        {
            var dict = new Dictionary<string, string>
                           {
                               { "E-mail", Order.Client.Email },
                               { "ФИО", Order.Client.GetFullName() },
                               { "Телефон", Order.Client.Phone },
                           };

            var clientInfoParagraph = new Paragraph()
                .WithHeader("Персональные данные", FontSize)
                .WithCollection(dict);
            return clientInfoParagraph;
        }

        protected override Paragraph AddDeliveryInfoParagraph(float? fontSize = null)
        {

            var dict = new Dictionary<string, string>
                           {
                               { "Адрес:", Order.Address.ToString() },
                               { "Комментарий к заказу:", Order.Comment }
                           };

            if (!string.IsNullOrEmpty(Order.Address.Subway))
            {
                dict.Add("Метро:", Order.Address.Subway);
            }


            var deliveryParagraph = new Paragraph().WithCollection(dict);
            return deliveryParagraph;
        }

        protected override Paragraph AddPaymentInfoParagraph(float? fontSize = null)
        {
            var paragraph = new Paragraph().WithCollection(new Dictionary<string, string>
                                                               {
                { Order.DeliveryType, $"{Order.DeliveryCost:F} руб." },
                { "Оплата:", Order.PaymentType.GetSpecAttribute() }
                                                               });
            paragraph.SetTextAlignment(TextAlignment.RIGHT);
            return paragraph;
        }

        protected override Paragraph AddSummaryParagraph(float? fontSize = null)
        {
            
            var summaryParagraph = new Paragraph().WithCollection(new Dictionary<string, string>
                                                                      {
                                                                          { $"Общая сумма ({Order.ProductsCount} шт.):", $"{Order.ProductsCost} руб." },
                                                                          { "Учетом скидок:", $"{Order.AfterDiscountCost} руб." },
                                                                      });
            summaryParagraph.SetTextAlignment(TextAlignment.RIGHT);
            return summaryParagraph;
        }
    }
}
