﻿namespace ShopBase.Services.PdfGeneration.Impl.OrderForms
{
    using System.Collections.Generic;
    using iText.Kernel.Events;
    using iText.Kernel.Geom;
    using iText.Kernel.Pdf;
    using iText.Layout;
    using iText.Layout.Borders;
    using iText.Layout.Element;
    using iText.Layout.Properties;
    using ShopBase.Domain;
    using ShopBase.Shared.Extensions;

    public class EmbaumerPdfForm : PdfFormBase
    {
        public EmbaumerPdfForm(Order information)
            : base(information)
        {
        }

        public override void Create(string pathToSave)
        {
            var writer = new PdfWriter(pathToSave);
            var pdf = new PdfDocument(writer);
            pdf.AddEventHandler(PdfDocumentEvent.END_PAGE, new PaginationEventHandler(Order));
            var document = new Document(pdf, PageSize.A4);
            document.SetMargins(23, 28, 35, 35);

            var arial = ITextExtensions.GetFont("Arial.ttf");
            document.SetFont(arial).SetFontSize(FontSize);

            var layoutTable = new Table(1)
                .UseAllAvailableWidth()
                .AddCell(ITextExtensions.NewCell().Add(this.AddImagesTable(pdf, AddOrderInfoParagraph())))
                .AddCell(ITextExtensions.NewCell()
                    .Add(new Table(2).UseAllAvailableWidth()
                        .AddCell(ITextExtensions.NewCell().Add(this.AddDeliveryInfoParagraph()).SetHorizontalAlignment(HorizontalAlignment.LEFT))
                        .AddCell(ITextExtensions.NewCell().Add(this.AddClientInfoParagraph()).SetHorizontalAlignment(HorizontalAlignment.RIGHT))))
                .AddCell(ITextExtensions.NewCell().Add(this.AddProductsTable(true, "Название", "Артикул", "Количество", "Цена")).SetBorderBottom(new SolidBorder(0.75f)))
                .AddCell(ITextExtensions.NewCell().Add(this.AddSummaryParagraph()))
                .AddCell(ITextExtensions.NewCell().Add(this.AddPaymentInfoParagraph()))
                .AddCell(ITextExtensions.NewCell().Add(this.AddSigningFieldCell()));

            document.Add(layoutTable);

            document.Close();
            writer.Dispose();
        }

        protected override Paragraph AddOrderInfoParagraph(float? fontSize = null)
        {
            var infoParagraph = new Paragraph()
                .WithHeader("Ваш заказ", fontSize ?? FontSize)
                .WithHeader($"№{Order.PublicNumber}", FontSize + 5)
                .WithCollection(new Dictionary<string, string>
                                    {
                                        { "От", $"{Order.OrderedAt:dd.MM.yyyy}" },
                                        { "Статус:", $"{Order.State.GetSpecAttribute()} ({Order.ApprovedAt:dd.MM.yyyy})" }
                                    });
            infoParagraph.SetTextAlignment(TextAlignment.LEFT);
            return infoParagraph;
        }

        protected override Paragraph AddClientInfoParagraph(float? fontSize = null)
        {
            var infoParagraph = new Paragraph()
                .WithHeader("Клиент", fontSize ?? FontSize + 1.3f, true)
                .WithCollection(new Dictionary<string, string>
                                    {
                                        { "Имя:", $"{Order.Client.GetFullName()}" },
                                        { "Email", $"{Order.Client.Email}" },
                                        { "Телефон:", $"{Order.Client.Phone}" }
                                    });
            return infoParagraph;
        }

        protected override Paragraph AddDeliveryInfoParagraph(float? fontSize = null)
        {
            var dict = new Dictionary<string, string>
                           {
                               { "Полный адрес:",  Order.Address.ToString() },
                               { "Комментарий:", Order.Comment }
                           };
            if (!string.IsNullOrEmpty(Order.Address.Subway))
            {
                dict.Add("Метро:", Order.Address.Subway);
            }

            var infoParagraph = new Paragraph()
                .WithHeader("Адрес доставки", fontSize ?? FontSize + 1.3f, true)
                .WithCollection(dict);
            infoParagraph.SetTextAlignment(TextAlignment.LEFT);
            return infoParagraph;
        }

        protected override Paragraph AddPaymentInfoParagraph(float? fontSize = null)
        {
            var paymentParagraph = new Paragraph()
                .WithCollection(new Dictionary<string, string>
                                    {
                                        { null, Order.PaymentType.GetSpecAttribute() },
                                        { null, Order.DeliveryType }
                                    });
            return paymentParagraph;
        }

        protected override Paragraph AddSummaryParagraph(float? fontSize = null)
        {

            var summaryParagraph = new Paragraph()
                .WithHeader($"Итого ({Order.ProductsCount} шт.)", fontSize ?? FontSize + 2, true).WithCollection(
                    new Dictionary<string, string>
                        {
                            { "Стоимость товаров:", $"{Order.ProductsCost} руб." },
                            { "С учетом скидок:", $"{Order.AfterDiscountCost} руб." },
                            { "Стоимость доставки:", $"{Order.DeliveryCost} руб." },
                            { "Стоимость оплаты:", $"{Order.ComissionCost} руб." },
                            { "Итого:", $"{Order.TotalCost} руб." }
                        });
            summaryParagraph.SetTextAlignment(TextAlignment.RIGHT);
            return summaryParagraph;
        }
    }
}
