﻿namespace ShopBase.Services.PdfGeneration.Impl
{
    using System;

    using ShopBase.Domain;
    using ShopBase.Messaging.Dto.PdfGeneration;
    using ShopBase.Services.PdfGeneration.Impl.OrderForms;
    using ShopBase.Services.PdfGeneration.Interfaces;

    public class DefaultPdfGenerator : IPdfGenerator
    {
        public PdfFormBase GenerateFor(Order order)
        {
            switch (order.Site.Name)
            {
                case "randewoo":
                    return new RandewooPdfForm(order);
                case "edete":
                    return new EdetePdfForm(order);
                case "embaumer":
                    return new EmbaumerPdfForm(order);
                default:
                    throw new ArgumentOutOfRangeException(null, "В документе не указан вебсайт с которого сделан заказ!");
            }
        }
    }
}
