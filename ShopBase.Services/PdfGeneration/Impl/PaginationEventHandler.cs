﻿namespace ShopBase.Services.PdfGeneration.Impl
{
    using System;
    using System.Linq;

    using iText.Kernel.Events;
    using iText.Kernel.Geom;
    using iText.Kernel.Pdf;
    using iText.Kernel.Pdf.Canvas;
    using iText.Layout;
    using iText.Layout.Element;
    using iText.Layout.Properties;
    using ShopBase.Domain;

    public class PaginationEventHandler : IEventHandler
    {
        private readonly Order _order;

        public PaginationEventHandler(Order order)
        {
            _order = order;
        }

        public void HandleEvent(Event @event)
        {
            var arial = ITextExtensions.GetFont("Arial.ttf");
            var fontSize = 7.5f;

            PdfDocumentEvent docEvent = (PdfDocumentEvent)@event;
            PdfDocument document = docEvent.GetDocument();
            PdfPage page = docEvent.GetPage();
            PdfCanvas aboveCanvas = new PdfCanvas(page.NewContentStreamBefore(), page.GetResources(), document);
            Rectangle pageArea = page.GetPageSize();

            var header = new Table(3).SetFont(arial)
                .SetFontSize(fontSize)
                .SetWidth(UnitValue.CreatePercentValue(88.5f))
                .SetTextAlignment(TextAlignment.CENTER)
                .SetHorizontalAlignment(HorizontalAlignment.CENTER)                
                .AddCell(
                    ITextExtensions.NewCell()
                        .Add(new Paragraph($"Дата: {DateTime.Now:g}"))
                        .SetWidth(UnitValue.CreatePercentValue(33.3f))
                        .SetTextAlignment(TextAlignment.LEFT))
                .AddCell(
                    ITextExtensions.NewCell()
                        .Add(new Paragraph($"Напечатал: {null ?? string.Empty}"))
                        .SetWidth(UnitValue.CreatePercentValue(33.3f))
                        .SetTextAlignment(TextAlignment.CENTER))
                .AddCell(
                    ITextExtensions.NewCell()
                        .Add(new Paragraph(string.Empty))
                        .SetWidth(UnitValue.CreatePercentValue(33.3f))
                        .SetTextAlignment(TextAlignment.RIGHT));

            var footer = new Table(3)
                .SetRelativePosition(0, 780, 0, 0)
                .SetFont(arial)
                .SetFontSize(fontSize)
                .SetWidth(UnitValue.CreatePercentValue(88.5f))
                .SetTextAlignment(TextAlignment.CENTER)
                .SetHorizontalAlignment(HorizontalAlignment.CENTER)                
                .AddCell(
                    ITextExtensions.NewCell()
                        .Add(new Paragraph($"Метро: {_order.Address.Subway}"))
                        .SetWidth(UnitValue.CreatePercentValue(33.3f))
                        .SetTextAlignment(TextAlignment.LEFT)
                        .SetVerticalAlignment(VerticalAlignment.BOTTOM))
                .AddCell(
                    ITextExtensions.NewCell()
                        .Add(new Paragraph($"Подтвержден: {_order.Approver}/ {_order.ApprovedAt:dd.MM.yy hh:mm}"))
                        .SetWidth(UnitValue.CreatePercentValue(33.3f))
                        .SetTextAlignment(TextAlignment.CENTER)
                        .SetVerticalAlignment(VerticalAlignment.BOTTOM))
                .AddCell(
                    ITextExtensions.NewCell()
                        .Add(new Paragraph($"{string.Join("\n", _order.Containers.Select(c => $"{c.Name}/{c.Cell}"))}")
                            .SetFontSize(fontSize + 3)
                            .SetMultipliedLeading(0.97f))
                        .SetWidth(UnitValue.CreatePercentValue(33.3f))
                        .SetTextAlignment(TextAlignment.RIGHT)
                        .SetVerticalAlignment(VerticalAlignment.BOTTOM));


            new Canvas(aboveCanvas, document, pageArea)
                .Add(header)
                .Add(footer);
        }
    }
}