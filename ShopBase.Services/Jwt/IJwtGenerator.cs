﻿namespace ShopBase.Services.Jwt
{
    using ShopBase.Domain;

    public interface IJwtGenerator
    {
        string Generate(User user);
    }
}