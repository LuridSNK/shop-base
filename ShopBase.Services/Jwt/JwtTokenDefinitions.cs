﻿namespace ShopBase.Services.Jwt
{
    using System;
    using System.Text;
    using Microsoft.Extensions.Configuration;
    using Microsoft.IdentityModel.Tokens;

    public class JwtTokenDefinitions
    {
        private static string _key = string.Empty;

        public static string Key
        {
            get => _key;
            set => _key = value.HashToMD5();
        }

        public static SecurityKey IssuerSigningKey => new SymmetricSecurityKey(Encoding.UTF8.GetBytes(Key));

        public static SigningCredentials SigningCredentials => new SigningCredentials(IssuerSigningKey, SecurityAlgorithms.HmacSha256);

        public static TimeSpan ExpiryInHours { get; set; } = TimeSpan.FromHours(18);

        public static TimeSpan ClockSkew { get; set; } = TimeSpan.FromHours(0);

        public static string Issuer { get; set; } = string.Empty;

        public static string Audience { get; set; } = string.Empty;

        public static bool ValidateIssuerSigningKey { get; set; } = true;

        public static bool ValidateLifetime { get; set; } = true;

        public static bool ValidateIssuer { get; set; } = true;

        public static bool ValidateAudience { get; set; } = true;

        public static void LoadFromConfiguration(IConfiguration configuration)
        {
            var config = configuration.GetSection("Jwt");
            
            Key = config.GetValue<string>(nameof(Key));
            Audience = config.GetValue<string>(nameof(Audience));
            Issuer = config.GetValue<string>(nameof(Issuer));
            ExpiryInHours = TimeSpan.FromMinutes(config.GetValue<int>(nameof(ExpiryInHours)));
            ValidateIssuerSigningKey = config.GetValue<bool>(nameof(ValidateIssuerSigningKey));
            ValidateLifetime = config.GetValue<bool>(nameof(ValidateLifetime));
            ClockSkew = TimeSpan.FromMinutes(config.GetValue<int>(nameof(ClockSkew)));
            ValidateIssuer = config.GetValue<bool>(nameof(ValidateIssuer));
            ValidateAudience = config.GetValue<bool>(nameof(ValidateAudience));
        }

        public static TokenValidationParameters GenerateTokenParameters() =>
            new TokenValidationParameters
                {
                    IssuerSigningKey = IssuerSigningKey,
                    ValidAudience = Audience,
                    ValidIssuer = Issuer,
                    ValidateIssuerSigningKey = ValidateIssuerSigningKey,
                    ValidateLifetime = ValidateLifetime,
                    ClockSkew = ClockSkew,
                    ValidateAudience = ValidateAudience,
                    ValidateIssuer = ValidateIssuer
                };
    }

}