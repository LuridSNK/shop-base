﻿namespace ShopBase.Services.Jwt
{
    using System;
    using System.IdentityModel.Tokens.Jwt;
    using System.Security.Claims;
    using ShopBase.Domain;

    public class JwtGenerator : IJwtGenerator
    {
        public string Generate(User user)
        {
            var claims = new[]
                             {
                                 new Claim(ClaimTypes.Name, user.Id.ToString()),
                                 new Claim(ClaimTypes.NameIdentifier, user.Id.ToString()),
                                 new Claim(ClaimTypes.Role, user.Role.Name),
                                 new Claim(JwtRegisteredClaimNames.Sub, user.Id.ToString()),
                                 new Claim(JwtRegisteredClaimNames.UniqueName, user.Id.ToString())
                             };

            var token = new JwtSecurityToken(
                issuer: JwtTokenDefinitions.Issuer,
                audience: JwtTokenDefinitions.Audience,
                claims: claims,
                expires: DateTime.Now.Add(JwtTokenDefinitions.ExpiryInHours),
                signingCredentials: JwtTokenDefinitions.SigningCredentials);
            return new JwtSecurityTokenHandler().WriteToken(token);
        }
    }
}