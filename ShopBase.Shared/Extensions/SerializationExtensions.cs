﻿namespace ShopBase.Shared.Extensions
{
    using System.IO;
    using System.Xml.Serialization;
    using Newtonsoft.Json;

    public static class SerializationExtensions
    {
        public static string SerializeToJson(this object value) =>
            JsonConvert.SerializeObject(value, Formatting.Indented);

        public static string SerializeToXml(this object value)
        {
            string result;
            using (var memStream = new MemoryStream())
            {
                XmlSerializer serializer = new XmlSerializer(value.GetType());
                serializer.Serialize(memStream, value);
                using (var sr = new StreamReader(memStream)) result = sr.ReadToEnd();
            }

            return result;
        }

        public static T DeserializeFromJson<T>(this string value) =>
            JsonConvert.DeserializeObject<T>(value);

        public static T DeserializeFromXml<T>(this string value)
        {
            var serializer = new XmlSerializer(typeof(T));
            T result;
            using (var sr = new StringReader(value))
            {
                result = (T)serializer.Deserialize(sr);
            }

            return result;
        }
    }
}