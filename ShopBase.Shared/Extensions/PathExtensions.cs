﻿namespace ShopBase.Shared.Extensions
{
    using System;
    using System.IO;

    public static class PathExtensions
    {
        public static string CheckOnCopies(this string fileSystemPath)
        {
            if (string.IsNullOrEmpty(fileSystemPath))
            {
                throw new ArgumentException("Путь к файлу не может быть пустым!");
            }

            var dir = Path.GetDirectoryName(fileSystemPath);
            var fileName = Path.GetFileNameWithoutExtension(fileSystemPath);
            var fileExt = Path.GetExtension(fileSystemPath);

            for (var i = 1;; ++i)
            {
                if (!File.Exists(fileSystemPath)) return fileSystemPath;

                fileSystemPath = Path.Combine(dir, $"{fileName}({i}).{fileExt}");
            }
        }
    }
}
