﻿namespace ShopBase.Shared.Extensions
{
    using System;
    using System.Linq;
    using ShopBase.Shared.Attributes;

    public static class EnumExtensions
    {
        public static string GetSpecAttribute(this Enum enumObj)
        {
            var fieldInfo = enumObj.GetType().GetField(enumObj.ToString());

            var attribArray = fieldInfo.GetCustomAttributes(false);

            if (!attribArray.Any())
            {
                return enumObj.ToString();
            }

            var attrib = attribArray.FirstOrDefault() as SpecAttribute;
            return attrib?.Description;
        }
    }
}
