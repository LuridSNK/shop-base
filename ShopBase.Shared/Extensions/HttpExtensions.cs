﻿namespace ShopBase.Shared.Extensions
{
    using System.Net.Http;
    using System.Threading.Tasks;

    public static class HttpClientExtensions
    {
        public static async Task<T> ReadAsJsonAsync<T>(this HttpResponseMessage msg)
        {
            var stringContent = await msg.Content.ReadAsStringAsync();
            return stringContent.DeserializeFromJson<T>();
        }

        public static async Task<T> ReadAsXmlAsync<T>(this HttpResponseMessage msg)
        {
            var stringContent = await msg.Content.ReadAsStringAsync();
            return stringContent.DeserializeFromXml<T>();
        }
    }
}
