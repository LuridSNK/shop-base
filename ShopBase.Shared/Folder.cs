﻿namespace ShopBase.Shared
{
    public class Folder
    {
        public string DirectoryName { get; set; }

        public string Extension { get; set; }

        public bool DeleteSubdirectories { get; set; }
    }
}
