﻿namespace ShopBase.Shared
{
    using System;
    using System.Collections.Generic;

    public static class Sites
    {
        public static Guid Randewoo { get; } = Guid.ParseExact("48E0A097-E897-4ABD-8B98-2B3FFC6C405B", "D");

        public static Guid Embaumer { get; } = Guid.ParseExact("B2803EA7-0BFA-4AA7-B459-F2A997974312", "D");

        public static Guid Edete { get; } = Guid.ParseExact("9EED7FEB-F8E8-46F5-B515-F66B644B7718", "D");

        public static string FindSiteName(this Guid siteId)
        {
            if (siteId == Randewoo)
            {
                return nameof(Randewoo);
            }
            if (siteId == Embaumer)
            {
                return nameof(Embaumer);
            }
            if (siteId == Edete)
            {
                return nameof(Edete);
            }

            throw new KeyNotFoundException("Site id is not valid");
        }

    }
}
