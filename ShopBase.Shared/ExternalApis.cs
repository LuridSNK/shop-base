﻿namespace ShopBase.Shared
{
    public static class ExternalAPIs
    {
        // Transpot companies
        public const string PickPoint = "https://e-solution.pickpoint.ru/apiTest";
        public const string CDEK = "";
        public const string DalliService = "";
        public const string RussianPost = "";

        // Central bank to get currencies
        public const string CentralBankOfRussia = "http://www.cbr.ru/scripts/XML_daily.asp";
    }
}
