﻿namespace ShopBase.Shared.Models
{
    using System.ComponentModel.DataAnnotations;

    public class RegisterModel
    {
        [EmailAddress, Required]
        public string Email { get; set; }

        [Required]
        public string Password { get; set; }
    }
}
