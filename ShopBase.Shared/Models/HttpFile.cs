﻿namespace ShopBase.Shared.Models
{
    using System;

    public class HttpFile
    {
        public string Sender { get; set; }

        public string Name { get; set; }

        public DateTime TimeStamp { get; set; }

        public byte[] Contents { get; set; }
    }
}

