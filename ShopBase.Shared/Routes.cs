﻿namespace ShopBase.Shared
{
    public static class Routes
    {
        public const string Address = "http://localhost:45001/api";

        public const string RabbitMQ = "rabbitmq://localhost";

        public static string GetUrl(Controller endpoint)
        {
            return Address + "/" + endpoint;
        }
    }

    public enum Controller
    {
        Auth,
        Products,
        Categories,
        Customers,
        Orders,
        Purchases,
        Logistics,
        Update
    }
}
