﻿namespace ShopBase.Shared.Attributes
{
    using System;

    /// <summary>
    /// Specification attribute
    /// </summary>
    public class SpecAttribute : Attribute
    {
        public SpecAttribute(string description)
        {
            this.Description = description;
        }

        public string Description { get; private set; }
    }
}
