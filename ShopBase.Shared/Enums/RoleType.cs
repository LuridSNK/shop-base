﻿namespace ShopBase.Shared.Enums
{
    using ShopBase.Shared.Attributes;

    public enum RoleType
    {
        /// <summary>
        /// Разработчик. Повышенные права.
        /// </summary>
        [Spec("Разработчик")]
        Developer = 1,

        /// <summary>
        /// Администратор. Повышенные права.
        /// </summary>
        [Spec("Администратор")]
        Administrator,

        /// <summary>
        /// Старший сотрудник. Стандартные права + доп. функции.
        /// </summary>
        [Spec("Руководитель отдела")]
        HeadOfDepartment,

        /// <summary>
        /// Старший сотрудник. Стандартные права + доп. функции.
        /// </summary>
        [Spec("Старший сотрудник")]
        SeniorEmployee,

        /// <summary>
        /// Обычный сотрудник. Стандартные права.
        /// </summary>
        [Spec("Сотрудник")]
        Employee,

        /// <summary>
        /// Пользователь-стажер. Пониженные права.
        /// </summary>
        [Spec("Стажер")]
        Intern,
    }
}
