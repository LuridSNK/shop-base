﻿namespace ShopBase.Shared.Enums
{
    using ShopBase.Shared.Attributes;

    public enum OrderState
    {
        [Spec("Не определен")]
        Undefined,

        [Spec("Создан")]
        Created,

        [Spec("Ожидает предоплаты")]
        WaitingForPrepayment,

        [Spec("Оплачен")]
        Paid,

        [Spec("Ожидает ответа")]
        WaitingForResponse,

        [Spec("Подтвержден")]
        Confirmed, 

        [Spec("Готов к отгрузке")]
        ReadyForShipment,

        [Spec("Готов к выдаче")]
        ReadyForIssue,

        [Spec("Отправлен")]
        Sent,

        [Spec("Передан в ТК")]
        IssuedToDeliveryService,

        [Spec("Передан в Pickpoint")]
        IssuedToPickPoint,
        
        [Spec("Получен клиентом")]
        ReceivedByACustomer = 13,

        [Spec("Отменен")]
        Cancelled,

        [Spec("Возвращен в магазин")]
        Refunded,
    }
}