﻿namespace ShopBase.Shared.Enums
{
    using ShopBase.Shared.Attributes;

    public enum DeliveryServiceType
    {
        [Spec("Неопределена")]
        Undefined,

        [Spec("Pick Point")]
        PickPoint,

        [Spec("СДЭК")]
        CDEK,

        [Spec("Dalli Service")]
        Dalli,

        [Spec("Почта России")]
        RussianPost
    }
}