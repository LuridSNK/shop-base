﻿namespace ShopBase.Shared.Enums
{
    using ShopBase.Shared.Attributes;

    public enum PaymentType
    {
        [Spec("Оплата наличными при получении")]
        CashOnReceipt = 1,

        [Spec("Оплата по реквизитам")]
        PaymentByRequisites,

        [Spec("Наложенным платежом при получении бандероли")]
        CashOnDeliveryViaParcel,

        [Spec("Яндекс-деньги")]
        YandexMoney,

        [Spec("Web-Money")]
        WebMoney,

        [Spec("Оплата курьеру службы EMS (комиссия 2-5% при получении)")]
        EMSCourier,

        [Spec("Оплата при получении")]
        PaymentOnReceipt,

        [Spec("Онлайн-оплата по карте (Robokassa)")]
        Robokassa,

        [Spec("Оплата при получении в постамате PickPoint")]
        PickPointTerminal,

        [Spec("Онлайн-оплата картой (РБК)")]
        RBK,

        [Spec("Оплата при получении (СДЭК)")]
        CDEK,

        [Spec("Онлайн-оплата по карте")]
        CreditCard,

        [Spec("Онлайн-оплата по карте (e-Pos)")]
        EPos,

        [Spec("Оплата наличными или картой при получении")]
        CashOrCardOnReceipt
    }
}