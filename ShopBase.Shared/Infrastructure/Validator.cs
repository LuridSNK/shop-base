﻿namespace ShopBase.Shared.Infrastructure
{
    using System.Net.Mail;

    public static class Validator
    {
        public static bool IsValidEmail(this string email)
        {
            try
            {
                var addr = new MailAddress(email);
                return addr.Address == email;
            }
            catch
            {
                return false;
            }
        }
    }
}
