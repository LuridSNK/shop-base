﻿namespace ShopBase.Shared
{
    public class EmailCredentials
    {

        /// <summary>
        ///     Host name / IP of the SMTP hosting provider
        /// </summary>
        public string Host { get; set; }


        /// <summary>
        ///     Login for email credentials
        /// </summary>
        public string Login { get; set; }


        /// <summary>
        ///     Password for email credentials
        /// </summary>
        public string Password { get; set; }


        /// <summary>
        ///     SMTP Port with default value of '587'
        /// </summary>
        public int Port { get; set; } = 587;
    }
}
