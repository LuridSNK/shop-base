﻿namespace ShopBase.Persistence
{
    using System;
    using CryptoHelper;
    using Microsoft.EntityFrameworkCore;
    using ShopBase.Domain;
    using ShopBase.Persistence.DbContext;

    public class FakeDbContextFactory
    {
        public static ApplicationDbContext Create()
        {
            var options = new DbContextOptionsBuilder<ApplicationDbContext>()
                .UseInMemoryDatabase(Guid.NewGuid().ToString())
                .Options;
            var context = new ApplicationDbContext(options);

            context.Database.EnsureCreated();
            var role = new Role { Id = 1, Name = "someguy" };
            var users = new[]
                            {
                                new User { Id = Guid.NewGuid(), Name = "aleksey", PasswordHash = Crypto.HashPassword("1234"), Role = role },
                                new User { Id = Guid.NewGuid(), Name = "petr", PasswordHash = Crypto.HashPassword("2345"), Role = role },
                                new User { Id = Guid.NewGuid(), Name = "vladimir", PasswordHash = Crypto.HashPassword("3456") },
                            };
           
            context.Users.AddRange(users);
            context.Roles.Add(role);
            context.SaveChanges();
            return context;
        }

        public static void Destroy(ApplicationDbContext context)
        {
            context.Database.EnsureDeleted();
            context.Dispose();
        }
    }
}
