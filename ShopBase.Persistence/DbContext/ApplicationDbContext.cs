﻿namespace ShopBase.Persistence.DbContext
{
    using Microsoft.EntityFrameworkCore;
    using ShopBase.Domain;

    public class ApplicationDbContext : DbContext
    {
        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options)
            : base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.ApplyConfigurationsFromAssembly(typeof(ApplicationDbContext).Assembly);
        }

        public DbSet<User> Users { get; private set; }

        public DbSet<Role> Roles { get; private set; }
    }
}