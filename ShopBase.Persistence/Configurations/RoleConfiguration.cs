﻿namespace ShopBase.Persistence.Configurations
{
    using Microsoft.EntityFrameworkCore;
    using Microsoft.EntityFrameworkCore.Metadata.Builders;
    using ShopBase.Domain;

    public class RoleConfiguration : IEntityTypeConfiguration<Role>
    {
        public void Configure(EntityTypeBuilder<Role> builder)
        {
            builder.HasKey(r => r.Id);

            builder.Property(u => u.Id)
                .ValueGeneratedOnAdd();

            builder.HasIndex(r => r.Name);
        }
    }
}
