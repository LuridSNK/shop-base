﻿namespace ShopBase.Persistence
{
    using System.Linq;

    using CryptoHelper;

    using ShopBase.Domain;
    using ShopBase.Persistence.DbContext;

    public class DbContextInitializer
    {
        public static void Initialize(ApplicationDbContext context)
        {
            var initializer = new DbContextInitializer();
            initializer.SeedEverything(context);
        }

        public void SeedEverything(ApplicationDbContext context)
        {
            context.Database.EnsureCreated();

            if (context.Users.Any())
            {
                // database's been seeded
                return; 
            }

            SeedRoles(context);
            SeedUsers(context);
        }

        private void SeedRoles(ApplicationDbContext context)
        {
            var roles = new[]
                            {
                                new Role { Name = "Developer" },
                                new Role { Name = "Administrator" },
                                new Role { Name = "HeadOfDepartment" },
                                new Role { Name = "SeniorEmployee" },
                                new Role { Name = "Employee" },
                                new Role { Name = "Intern" },
                            };
            context.Roles.AddRange(roles);
            context.SaveChanges();
        }

        private void SeedUsers(ApplicationDbContext context)
        {
            var users = new[]
                            {
                                new User { Name = "filippenko_k", PasswordHash = Crypto.HashPassword("7uR1d_5nk="), RoleId = 1 }, 
                                new User { Name = "tihonuk_a", PasswordHash = Crypto.HashPassword("p4c1fiC0_01="), RoleId = 1 },
                                new User { Name = "skripkina_m", PasswordHash = Crypto.HashPassword("v10L=3n_02="), RoleId = 1 },
                                new User { Name = "zakusilo_a", PasswordHash = Crypto.HashPassword("b1G7h3_==23R"), RoleId = 1 }
                            };

            context.Users.AddRange(users);
            context.SaveChanges();
        }
    }
}
