﻿namespace ShopBase.Messaging.Smtp.Interfaces
{
    using System;
    using System.Collections.Generic;
    using System.Net.Mail;
    using System.Threading.Tasks;

    using ShopBase.Shared;

    public interface IMailClient
    {

        /// <summary>
        ///     Credentials <see cref="EmailCredentials"/>
        /// </summary>
        EmailCredentials Credentials { get; }


        /// <summary>
        ///     Sending email synchronously
        /// </summary>
        /// <param name="receivers"> The list of emails of receivers </param>
        /// <param name="subject"> Subject of message</param>
        /// <param name="content"> Message content </param>
        /// <param name="sender"> Sender of the message </param>
        /// <param name="attachments"> Optional: Additional attatchments / files </param>
        /// <param name="sendCompleted"> Optional: Action that occurs when message was sent to receiver(s) </param>
        void SendMessage(
            IEnumerable<string> receivers,
            string subject,
            string content,
            string sender,
            IEnumerable<Attachment> attachments = null,
            Action sendCompleted = null);

        /// <summary>
        ///     Sending email asynchronously
        /// </summary>
        /// <param name="receivers"> The list of emails of receivers </param>
        /// <param name="subject"> Subject of message</param>
        /// <param name="content"> Message content </param>
        /// <param name="sender"> Sender of the message </param>
        /// <param name="attachments"> Optional: Additional attatchments / files </param>
        /// <param name="sendCompleted"> Optional: Action that occurs when message was sent to receiver(s) </param>
        /// <returns> void </returns>
        Task SendMessageAsync(
            IEnumerable<string> receivers,
            string subject,
            string content,
            string sender,
            IEnumerable<Attachment> attachments = null,
            Action sendCompleted = null);
    }
}
