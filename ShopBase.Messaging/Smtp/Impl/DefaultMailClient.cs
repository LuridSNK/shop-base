﻿namespace ShopBase.Messaging.Smtp.Impl
{
    using System;
    using System.Collections.Generic;
    using System.Net;
    using System.Net.Mail;
    using System.Threading.Tasks;
    using Castle.Core.Logging;

    using ShopBase.Messaging.Smtp.Interfaces;
    using ShopBase.Shared;
    using ShopBase.Shared.Infrastructure;

    /// <summary>
    ///     Email client for desktop / web applications
    /// </summary>
    public class DefaultMailClient : IMailClient
    {
        private readonly ILogger _logger;

        /// <summary>
        ///     Constructor, that takes primitives as parameters
        /// </summary>
        /// <param name="logger"> Logger </param>
        /// <param name="host"> Host name pr IP of ther SMTP provider </param>
        /// <param name="login"> Client login </param>
        /// <param name="password"> Client password </param>
        /// <param name="port"> Optional: Connection port (default is 587) </param>
        public DefaultMailClient(
            ILogger logger,
            string host, 
            string login, 
            string password, 
            int port = 587)
        {
            this.Credentials.Host = host;
            this.Credentials.Login = login;
            this.Credentials.Password = password;
            this.Credentials.Port = port;
            _logger = logger;
        }


        /// <summary>
        ///     Base CTOR for IoC / Database
        /// </summary>
        /// <param name="cred"> Email credentials <see cref="EmailCredentials"/> </param>
        /// <param name="logger"> Logger </param>
        public DefaultMailClient(EmailCredentials cred, ILogger logger)
        {
            this.Credentials = cred;
            _logger = logger;
        }


        public EmailCredentials Credentials { get; }


        /// <summary>
        ///     SMTP client, that actually sends messages
        /// </summary>
        private SmtpClient _client => new SmtpClient
                {
                    Host = this.Credentials.Host,
                    Port = this.Credentials.Port,
                    EnableSsl = true,
                    Credentials = new NetworkCredential(this.Credentials.Login, this.Credentials.Password),
                    DeliveryMethod = SmtpDeliveryMethod.Network,
                    Timeout = 2000
                };

        public async Task SendMessageAsync(
            IEnumerable<string> receivers, 
            string subject, 
            string content, 
            string sender, 
            IEnumerable<Attachment> attachments = null,
            Action sendCompleted = null)
        {
            MailMessage msg = null;
            try
            {
                using (var client = this._client)
                {
                    if (sendCompleted != null)
                    {
                        client.SendCompleted += (o, a) => sendCompleted.Invoke();
                    }

                    msg = this.HandleMessage(receivers, subject, content, sender, attachments);
                    await client.SendMailAsync(msg);
                }
            }
            finally
            {
                this.DisposeWithAttachments(msg);
            }
        }

        public void SendMessage(
            IEnumerable<string> receivers,
            string subject,
            string content,
            string sender,
            IEnumerable<Attachment> attachments = null,
            Action sendCompleted = null)
        {
            MailMessage msg = null;
            try
            {
                using (var client = this._client)
                {
                    if (sendCompleted != null)
                    {
                        client.SendCompleted += (o, a) => sendCompleted.Invoke();
                    }

                    msg = this.HandleMessage(receivers, subject, content, sender, attachments);
                    client.Send(msg);
                }
            }
            catch (Exception e)
            {
                _logger.Warn($"Failed from {typeof(DefaultMailClient).Name} {e.Message.Replace(Environment.NewLine, " ")}");
            }
            finally
            {
                this.DisposeWithAttachments(msg);
            }
        }



        /// <summary>
        ///     Method-helper for handling messages for both sync and async operations
        /// </summary>
        /// <param name="receivers"> The list of emails of receivers </param>
        /// <param name="subject"> Subject of message</param>
        /// <param name="content"> Message content </param>
        /// <param name="sender"> Sender of the message </param>
        /// <param name="attachments"> Optional: Additional attatchments / files </param>
        /// <returns> A completed Mail Message </returns>
        private MailMessage HandleMessage(
            IEnumerable<string> receivers,
            string subject,
            string content,
            string sender,
            IEnumerable<Attachment> attachments = null)
        {
            var msg = new MailMessage();
            foreach (var receiver in receivers)
            {
                if (!receiver.IsValidEmail())
                {
                    _logger.Info($"Email address {receiver} is not valid");
                }
                else
                {
                    msg.To.Add(new MailAddress(receiver));
                }
            }

            if (attachments != null)
            {
                foreach (var attachment in attachments)
                {
                    msg.Attachments.Add(attachment);
                }
            }

            msg.From = new MailAddress(sender);
            msg.Subject = subject;
            msg.Body = content;
            return msg;
        }



        /// <summary>
        ///     Disposing the message and it's attatchments if they're exist
        /// </summary>
        /// <param name="msg"> The message itself </param>
        private void DisposeWithAttachments(MailMessage msg)
        {
            foreach (var attachment in msg.Attachments)
            {
                attachment?.Dispose();
            }

            msg.Attachments.Dispose();
            msg.Dispose();
        }
    }
}
