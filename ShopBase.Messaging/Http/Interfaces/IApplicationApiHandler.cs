﻿
namespace ShopBase.Messaging.Http.Interfaces
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Threading.Tasks;
    using ShopBase.Domain;
    using ShopBase.Messaging.Dto;
    using ShopBase.Messaging.Http.Impl;
    using ShopBase.Shared;
    using ShopBase.Shared.Models;

    public interface IApplicationApiHandler
    {
        Task<UserDto> Authenticate(LoginModel loginModel);

        Task<Order> GetOrderByContainer(string containerNumber);

        Task<IEnumerable<Order>> GetOrdersByUserData(string type, string data);

        Task<Stream> SendOrderToWms(Guid orderId);
    }

    public class ApplicationApiHandler : IApplicationApiHandler
    {
        

        public async Task<UserDto> Authenticate(LoginModel loginModel)
        {
            var response = await ApplicationHttpClient.Instance.Post<UserDto>(
                               $"{Routes.GetUrl(Controller.Auth)}/login",
                               loginModel);
            if (!response.IsSuccessful)
            {
                throw new Exception(response.ErrorMessage);
            }

            ApplicationHttpClient.Instance.SetJwt(response.Value.Token);
            return response.Value;
        }


        public async Task<Order> GetOrderByContainer(string containerNumber)
        {
            var response = await ApplicationHttpClient.Instance.Get<Order>(
                               $"{Routes.GetUrl(Controller.Orders)}/byContainer/{containerNumber}");
            if (!response.IsSuccessful)
            {
                throw new Exception(response.ErrorMessage);
            }

            return response.Value;
        }

        public async Task<IEnumerable<Order>> GetOrdersByUserData(string type, string data)
        {
            var response = await ApplicationHttpClient.Instance.Get<IEnumerable<Order>>(
                               $"{Routes.GetUrl(Controller.Orders)}byUserData/{type}&{data}");
            if (!response.IsSuccessful)
            {
                throw new Exception(response.ErrorMessage);
            }

            return response.Value;
        }

        public async Task<Stream> SendOrderToWms(Guid orderId)
        {
            var response = await ApplicationHttpClient.Instance.Post<Stream>(
                               $"{Routes.GetUrl(Controller.Orders)}/state/{orderId}",
                               null);
            if (!response.IsSuccessful)
            {
                throw new Exception(response.ErrorMessage);
            }

            return response.Value;
        }
    }
}