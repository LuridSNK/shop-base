﻿namespace ShopBase.Messaging.Http.Interfaces
{
    using System;
    using System.Threading.Tasks;
    using ShopBase.Messaging.Http.Impl;

    public interface IApplicationHttpClient
    {
        bool IsTokenValid { get; }

        /// <summary>
        ///     Get/Read request for SB WebAPI
        /// </summary>
        /// <param name="url"> endpoint </param>
        /// <returns> response value(s) </returns>
        Task<ApiResponse<T>> Get<T>(string url);

        /// <summary>
        ///     Post/Create request for SB WebAPI
        /// </summary>
        /// <param name="url"> endpoint </param>
        /// <param name="item"> object to serialize </param>
        /// <returns> response value(s) </returns>
        Task<ApiResponse> Post(string url, object item);

        /// <summary>
        ///     Post/Create request for SB WebAPI
        /// </summary>
        /// <param name="url"> endpoint </param>
        /// <param name="item"> object to serialize </param>
        /// <returns> response value(s) </returns>
        Task<ApiResponse<T>> Post<T>(string url, object item);

        /// <summary>
        ///     Put/Update request for SB WebAPI
        /// </summary>
        /// <param name="url"> endpoint </param>
        /// <param name="item"> object to serialize </param>
        /// <returns> response value(s) </returns>
        Task<ApiResponse> Put(string url, object item);

        /// <summary>
        ///     Remove/Delete request for SB WebAPI
        /// </summary>
        /// <param name="url"></param>
        /// <returns></returns>
        Task<ApiResponse> Delete(string url);

        void SetJwt(string jwt);
    }
}