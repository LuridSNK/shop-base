﻿namespace ShopBase.Messaging.Http.Stubs
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Threading.Tasks;
    using ShopBase.Domain;
    using ShopBase.Messaging.Dto;
    using ShopBase.Messaging.Http.Interfaces;
    using ShopBase.Shared.Enums;
    using ShopBase.Shared.Models;

    public class ApplicationApiHandlerStub : IApplicationApiHandler
    {
        public async Task<UserDto> Authenticate(LoginModel loginModel)
        {
            return await Task.Factory.StartNew(
                       () => new UserDto
                                 {
                                     Id = Guid.NewGuid(),
                                     Role = RoleType.Developer,
                                     Name = loginModel.Username,
                                     Token = "RandomToken"
                                 });
        }

        public Task<Order> GetOrderByContainer(string containerNumber)
        {
            throw new NotImplementedException();
        }

        public Task<IEnumerable<Order>> GetOrdersByUserData(string type, string data)
        {
            throw new NotImplementedException();
        }

        public Task<Stream> SendOrderToWms(Guid orderId)
        {
            throw new NotImplementedException();
        }
    }
}
