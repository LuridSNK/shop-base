﻿namespace ShopBase.Messaging.Http.Impl
{
    using System;
    using System.IdentityModel.Tokens.Jwt;
    using System.IO;
    using System.Net.Http;
    using System.Net.Http.Headers;
    using System.Text;
    using System.Threading.Tasks;
    using ShopBase.Messaging.Http.Interfaces;
    using ShopBase.Shared.Extensions;

    public class ApplicationHttpClient : IApplicationHttpClient
    {
        public static readonly HttpClient HttpClient  = new HttpClient();

        public static readonly IApplicationHttpClient Instance = new ApplicationHttpClient();

        public bool IsTokenValid => DateTime.Now > new JwtSecurityTokenHandler().ReadJwtToken(Jwt).ValidTo;

        private string Jwt { get; set; }

        public async Task<ApiResponse<T>> Get<T>(string url)
        {
            try
            {
                SetJwt();
                HttpResponseMessage response = await HttpClient.GetAsync(url);
                response.EnsureSuccessStatusCode();
                ClearJwt();
                return new ApiResponse<T> { IsSuccessful = true, Content = await response.ReadAsJsonAsync<T>() };
            }
            catch (HttpRequestException e)
            {
                return this.HandleException<T>(e);
            }
        }

        public async Task<ApiResponse> Post(string url, object item)
        {
            try
            {
                SetJwt();
                HttpResponseMessage response = await HttpClient.PostAsync(url, new StringContent(item.SerializeToJson(), Encoding.UTF8, "application/json"));
                response.EnsureSuccessStatusCode();
                ClearJwt();
                return new ApiResponse { IsSuccessful = true };
            }
            catch (HttpRequestException e)
            {
                return HandleException(e);
            }
        }

        public async Task<ApiResponse<T>> Post<T>(string url, object item)
        {
            try
            {
                SetJwt();
                HttpResponseMessage response = await HttpClient.PostAsync(url, new StringContent(item.SerializeToJson(), Encoding.UTF8, "application/json"));
                response.EnsureSuccessStatusCode();
                ClearJwt();
                if (typeof(T) == typeof(Stream))
                {
                    return new ApiResponse<T> { IsSuccessful = true, Content = await response.Content.ReadAsStreamAsync() };
                }

                return new ApiResponse<T> { IsSuccessful = true, Content = await response.ReadAsJsonAsync<T>() };
            }
            catch (HttpRequestException e)
            {
                return this.HandleException<T>(e);
            }
        }

        public async Task<ApiResponse> Put(string url, object item)
        {
            try
            {
                SetJwt();
                HttpResponseMessage response = await HttpClient.PutAsync(url, new StringContent(item.SerializeToJson(), Encoding.UTF8, "application/json"));
                response.EnsureSuccessStatusCode();
                ClearJwt();
                return new ApiResponse { IsSuccessful = true };
            }
            catch (HttpRequestException e)
            {
                return HandleException(e);
            }
        }

        public async Task<ApiResponse> Delete(string url)
        {
            try
            {
                SetJwt();
                HttpResponseMessage response = await HttpClient.DeleteAsync(url);
                response.EnsureSuccessStatusCode();
                ClearJwt();
                return new ApiResponse { IsSuccessful = true };
            }
            catch (HttpRequestException e)
            {
                return HandleException(e);
            }
        }

        public void SetJwt(string jwt)
        {
            Jwt = jwt;
        }

        private void SetJwt()
        {
            if (!string.IsNullOrEmpty(Jwt))
            {
                HttpClient.DefaultRequestHeaders.Authorization =
                    new AuthenticationHeaderValue("Bearer", Jwt);
            }
        }

        private void ClearJwt()
        {
            HttpClient.DefaultRequestHeaders.Clear();
        }

        private ApiResponse HandleException(Exception e)
        {
            this.ClearJwt();
            return new ApiResponse { IsSuccessful = false, ErrorMessage = e.Message, Content = null };
        }

        private ApiResponse<T> HandleException<T>(Exception e)
        {
            this.ClearJwt();
            return new ApiResponse<T> { IsSuccessful = false, ErrorMessage = e.Message, Content = null };
        }


    }
}