﻿namespace ShopBase.Messaging.Http.Impl
{
    public class ApiResponse
    {
        public bool IsSuccessful { get; set; }

        public object Content { get; set; }

        public string ErrorMessage { get; set; }
    }

    public class ApiResponse<T> : ApiResponse
    {
        public T Value => IsSuccessful ? (T)Content : default(T);
    }
}