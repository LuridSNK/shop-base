﻿namespace ShopBase.Messaging.Dto
{
    using System;

    public class ProductDto
    {
        public Guid Id { get; set; }

        public string Sku { get; set; }

        public string Name { get; set; }

        public decimal Price { get; set; }

        public int Amount { get; set; }
    }
}
