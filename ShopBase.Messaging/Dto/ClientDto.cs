﻿namespace ShopBase.Messaging.Dto
{
    using System;

    public class ClientDto
    {
        public Guid Id { get; set; }

        public string FullName { get; set; }
        
        public string Phone { get; set; }

        public string Email { get; set; }
    }
}
