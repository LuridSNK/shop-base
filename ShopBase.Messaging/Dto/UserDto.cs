﻿namespace ShopBase.Messaging.Dto
{
    using System;
    using ShopBase.Shared.Enums;

    public class UserDto
    {
        public Guid Id { get; set; }

        public string Name { get; set; }

        public string Token { get; set; }

        public RoleType Role { get; set; }
    }
}
