﻿namespace ShopBase.Messaging.Dto.Warehouse
{
    using System;

    public class ContainerDto
    {
        public string Name { get; set; }

        public string Cell { get; set; }

        public Guid OrderId { get; set; }
    }
}