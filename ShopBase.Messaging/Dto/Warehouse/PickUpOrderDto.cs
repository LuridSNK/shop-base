﻿namespace ShopBase.Messaging.Dto.Warehouse
{
    using System;
    using System.Collections.Generic;

    public class PickUpOrderDto
    {
        public Guid Id { get; set; }

        public string OrderNumber { get; set; }
        
        public string DeliveryType { get; set; }

        public string Status { get; set; }

        public ClientDto Client { get; set; }

        public IEnumerable<ContainerDto> Containers { get; set; }
    }
}

