﻿namespace ShopBase.Messaging.Dto.Warehouse
{
    using System;
    using System.Collections.Generic;

    using ShopBase.Shared.Enums;

    public class PackagingOrderDto
    {
        public Guid Id { get; set; }
        
        public string OrderNumber { get; set; }
        
        public string DeliveryType { get; set; }

        public DeliveryServiceType ServiceType { get; set; }

        public IEnumerable<ContainerDto> Containers { get; set; }
    }
}
