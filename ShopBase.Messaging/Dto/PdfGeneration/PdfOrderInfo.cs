﻿namespace ShopBase.Messaging.Dto.PdfGeneration
{
    using System;
    using System.Collections.Generic;
    using System.Linq;

    using ShopBase.Messaging.Dto.Warehouse;
    using ShopBase.Shared.Enums;

    public class PdfOrderInfo
    {
        public Guid Id { get; set; }

        // публичный номер
        public string PublicNumber { get; set; }

        // внутренний номер заказа
        public string PrivateNumber { get; set; }

        public string WebsiteName { get; set; }

        // когда произведен заказ
        public DateTime OrderedAt { get; set; }

        // состояние заказа
        public OrderState State { get; set; }

        // общее кол-во товаров в шт
        public int ProductsCount => this.Products.Sum(p => p.Amount);

        // цена доставки
        public decimal DeliveryCost { get; set; }

        // цена за все товары 
        public decimal ProductsCost => this.Products.Sum(p => p.Price * p.Amount);

        // цена с учетом скидок (получаем с сайта)
        public decimal AfterDiscountCost { get; set; }

        // стоимость оплаты / комиссия
        public decimal ComissionCost { get; set; }

        // цена за все
        public decimal TotalCost { get; set; }

        //тип доставки
        public string DeliveryType { get; set; }

        //тип оплаты
        public PaymentType PaymentType { get; set; }

        // уже предоплачено
        public decimal PrepaidSum { get; set; }

        public IEnumerable<ContainerDto> Containers { get; set; }
        
        public IEnumerable<ProductDto> Products { get; set; }

        public string Approver { get; set; }

        public DateTime ApprovedAt { get; set; }

        public string CurrentEmployee { get; set; }

        public string AdditionalComment { get; set; }

        public string Address { get; set; }

        public string Subway { get; set; }
    }

}
