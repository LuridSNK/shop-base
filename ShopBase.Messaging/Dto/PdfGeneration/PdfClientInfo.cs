﻿namespace ShopBase.Messaging.Dto.PdfGeneration
{
    using System;

    public class PdfClientInfo
    {
        public Guid Id { get; set; }

        public string Email { get; set; }

        public string FullName { get; set; }

        public string Phone { get; set; }

        public int Discount { get; set; }

        public int OrderedCount { get; set; }

        public int RefusedCount { get; set; }
    }
}
