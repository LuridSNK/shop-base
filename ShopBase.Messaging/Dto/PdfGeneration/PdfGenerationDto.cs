﻿namespace ShopBase.Messaging.Dto.PdfGeneration
{
    public class PdfGenerationDto
    {
        public PdfClientInfo Client { get; set; }

        public PdfOrderInfo Order { get; set; }
    }

}
