﻿namespace ShopBase.Scheduling
{
    using System;
    using Quartz;
    using Serilog;
    using ShopBase.Scheduling.Recycler;

    public class AppScheduler
    {
        private readonly ILogger _logger;

        private readonly IScheduler _scheduler;

        private readonly WindsorJobFactory _jobFactory;

        public AppScheduler(WindsorJobFactory jobFactory)
        {
            _scheduler = null;
            _jobFactory = jobFactory;
            _logger = null;
        }

        public void SetupRecycling()
        {
            _scheduler.Start();
           var recycler = GetJob<RecyclingJob>(builder => builder
                    .StartNow()
                    .WithSimpleSchedule(
                        x => x
                            .WithIntervalInHours(2)
                            .RepeatForever()));
            _scheduler.ScheduleJob(recycler.Item1, recycler.Item2);
        }

        private (IJobDetail, ITrigger) GetJob<T>(Action<TriggerBuilder> triggerBuilding) where T : IJob
        {
            // var jobname = typeof(T).Name;
            IJobDetail job = JobBuilder
                .Create<T>()
                //.WithIdentity(jobname, "Group1")
                .Build();
            TriggerBuilder triggerBuilder = TriggerBuilder
                .Create()/*.WithIdentity($"{jobname}Trigger", "Group1")*/;
            triggerBuilding(triggerBuilder);
            ITrigger trigger = triggerBuilder.Build();
            return (job, trigger);  
        }
    }
}
