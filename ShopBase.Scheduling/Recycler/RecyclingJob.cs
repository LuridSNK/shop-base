﻿namespace ShopBase.Scheduling.Recycler
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Threading.Tasks;
    using Newtonsoft.Json;
    using Quartz;
    using Serilog;
    using ShopBase.Services.Static;
    using ShopBase.Shared;

    public class RecyclingJob : JobBase
    {
        public RecyclingJob(ILogger logger)
            : base(logger)
        {
        }

        public override async Task Execute(IJobExecutionContext context)
        {
            var jsonContent = File.ReadAllText(Path.Combine(Directory.GetCurrentDirectory(), "directories.json"));
            var folders = JsonConvert.DeserializeObject<List<Folder>>(jsonContent);

            foreach (var fld in folders)
            {
                await Task.Factory.StartNew(
                    () =>
                        {
                            try
                            {
                                DirectoryCleaner.DeleteFilesWithinDirectory(
                                    fld.DirectoryName,
                                    fld.Extension,
                                    fld.DeleteSubdirectories);
                            }
                            catch (Exception e)
                            {
                                Logger.Warning(e, "Failed to delete {@fld}", fld);
                            }
                        });
            }
        }
    }
}
