﻿namespace ShopBase.Scheduling
{
    using System.Threading.Tasks;
    using Quartz;
    using Serilog;

    public abstract class JobBase : IJob
    {
        protected readonly ILogger Logger;

        protected JobBase(ILogger logger)
        {
            Logger = logger;
        }

        public abstract Task Execute(IJobExecutionContext context);
    }
}
