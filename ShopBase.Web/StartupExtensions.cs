﻿using System;
using System.Linq;
using System.Reflection;
using GreenPipes;
using MassTransit;

namespace ShopBase.Web
{
    using FluentValidation.AspNetCore;

    using GreenPipes.Configurators;
    using GreenPipes.Filters.CircuitBreaker;

    using MediatR;
    using MediatR.Pipeline;
    using Microsoft.AspNetCore.Authentication.JwtBearer;
    using Microsoft.AspNetCore.Authorization;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.Extensions.DependencyInjection;
    using ShopBase.Application.Infrastructure;
    using ShopBase.Application.MessageQue.Consumers;
    using ShopBase.Application.Models.User.GetUserByUsernameQuery;
    using ShopBase.Services.Jwt;
    using ShopBase.Shared;
    using ShopBase.Web.Attributes;
    using ShopBase.Web.HealthChecks;

    public static class StartupExtensions
    {
        public static void ConfigureJwtAuthorization(this IServiceCollection services)
        {
            services.AddAuthorization(
                auth =>
                    {
                        auth.AddPolicy(
                            "Bearer",
                            new AuthorizationPolicyBuilder()
                                .AddAuthenticationSchemes(JwtBearerDefaults.AuthenticationScheme)
                                .RequireAuthenticatedUser().Build());
                    });
        }

        public static void ConfigureJwtAuthentication(this IServiceCollection services)
        {
            services.AddAuthentication(options => { options.DefaultScheme = JwtBearerDefaults.AuthenticationScheme; })
                .AddJwtBearer(
                    options => { options.TokenValidationParameters = JwtTokenDefinitions.GenerateTokenParameters(); });
        }

        public static void ConfigureHealthChecks(this IServiceCollection services)
        {
            services.AddHealthChecks()
                .AddCheck<DbContextHealthCheck>("DbContext")
                .AddCheck<SqlConnectionHealthCheck>("SqlConnection");
        }

        public static void ConfigureMediatR(this IServiceCollection services)
        {
            services.AddTransient(typeof(IPipelineBehavior<,>), typeof(RequestPreProcessorBehavior<,>));
            services.AddTransient(typeof(IPipelineBehavior<,>), typeof(RequestPerformanceBehaviour<,>));
            services.AddTransient(typeof(IPipelineBehavior<,>), typeof(RequestValidationBehavior<,>));
            services.AddMediatR(typeof(GetUserByNameQueryHandler).Assembly);
        }

        public static void ConfigureMVC(this IServiceCollection services)
        {
            services.AddMvc(options => options.Filters.Add<CustomExceptionFilterAttribute>())
                .SetCompatibilityVersion(CompatibilityVersion.Version_2_2)
                .AddFluentValidation(configuration => configuration.RegisterValidatorsFromAssemblyContaining<Startup>());
        }

        public static void ConfigureMassTransit(this IServiceCollection services)
        {
            // docker cmd: docker run -d -p 5672:5672 -p 15672:15672 --hostname junk --name rabbit-mgmt rabbitmq:3-management

            services.AddMassTransit(config =>
            {
                var consumers = typeof(GetUserByNameQueryHandler).Assembly.GetTypes()
                    .Where(p => typeof(IConsumer).IsAssignableFrom(p)).ToArray();
                config.AddConsumers(consumers);

                config.AddBus(provider => Bus.Factory.CreateUsingRabbitMq(cfg => 
                {
                    var host = cfg.Host(new Uri(Routes.RabbitMQ), hostConfig =>
                    {
                        // guest is for localhost only!
                        hostConfig.Username("guest");
                        hostConfig.Password("guest");
                    });

                    cfg.ReceiveEndpoint(host,"product-feed", epConfig => 
                    {
                        epConfig.Consumer<RandewooApiConsumer>();
                        // todo: add more consumers
                        epConfig.UseCircuitBreaker(
                        cbConfig =>
                        {
                            cbConfig.TrackingPeriod = TimeSpan.FromSeconds(20);
                            cbConfig.TripThreshold = 25;
                            cbConfig.ActiveThreshold = 5;
                            cbConfig.ResetInterval = TimeSpan.FromMinutes(2);
                        });
                    });
                }));
            });
        }
    }
}