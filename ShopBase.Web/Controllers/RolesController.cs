﻿namespace ShopBase.Web.Controllers
{
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Mvc;
    using ShopBase.Application.Models.Role.GetRolesQuery;

    public class RolesController : BaseController
    {
        [HttpPost]
        public async Task<ActionResult> Get()
        {
            // todo: cut the roles, that're avaliable for client!
            return Ok(await Mediator.Send(new GetRolesQuery()));
        }
    }
}