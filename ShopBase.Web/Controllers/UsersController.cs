﻿namespace ShopBase.Web.Controllers
{
    using System.Threading.Tasks;
    using CryptoHelper;
    using Microsoft.AspNetCore.Authorization;
    using Microsoft.AspNetCore.Mvc;
    using ShopBase.Application.Models.User.CreateUserCommand;

    public class UsersController : BaseController
    {
        [HttpPost, Authorize(AuthenticationSchemes = "Bearer", Roles = "Developer, Administrator, HeadOfDepartment, SeniorEmployee")]
        public async Task<ActionResult> Create(CreateUserCommand createUserCommand)
        {
            // hashing new password
            createUserCommand.Password = Crypto.HashPassword(createUserCommand.Password);
            await Mediator.Send(createUserCommand);
            return NoContent();

        }
    }
}