﻿namespace ShopBase.Web.Controllers
{
    using System.IO;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.Extensions.Configuration;

    public class UpdateController : BaseController
    {
        [HttpGet("{key}")]
        public async Task<ActionResult> Get([FromRoute] string verb, [FromServices] IConfiguration config)
        {
            if (verb != "latest") return this.BadRequest();

            var path = config["ServerPath:AppRelease"];
            var file = new DirectoryInfo(path)
                .GetFiles()
                .Where(f => f.Extension == ".nupkg")
                .OrderByDescending(f => f.CreationTime)
                .FirstOrDefault();
            if (file == null) return this.NotFound();

            return new PhysicalFileResult(file?.FullName, "application/octet-stream");

            // optional 
            /*
            using (Stream s = file?.OpenRead())
            {
                return new FileStreamResult(s, "application/octet-stream");
                return File(s, "application/octet-stream", file?.Name);
            }
            */
        }
    }
}