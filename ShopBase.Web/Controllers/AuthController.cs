﻿namespace ShopBase.Web.Controllers
{
    using System.Threading.Tasks;
    using CryptoHelper;
    using Microsoft.AspNetCore.Authorization;
    using Microsoft.AspNetCore.Mvc;
    using ShopBase.Application.Models.User.ChangeUserPasswordCommand;
    using ShopBase.Application.Models.User.GetUserByUsernameQuery;
    using ShopBase.Application.Models.User.GetUserQuery;
    using ShopBase.Messaging.Dto;
    using ShopBase.Services.Jwt;
    using ShopBase.Shared.Enums;

    public class AuthController : BaseController
    {
        private readonly IJwtGenerator _jwtGenerator;
        
        public AuthController(IJwtGenerator jwtGenerator)
        {
            _jwtGenerator = jwtGenerator;
        }

        [HttpPost("login"), AllowAnonymous]
        public async Task<ActionResult> Login([FromBody] GetUserByNameQuery credentials)
        {
            var user = await Mediator.Send(credentials);

            var isVerified = Crypto.VerifyHashedPassword(user.PasswordHash, credentials.Password);
            if (isVerified)
                return this.Ok(new UserDto
                                   {
                                       Id = user.Id,
                                       Name = user.Name,
                                       Role = (RoleType)user.Role.Id,
                                       Token = _jwtGenerator.Generate(user)
                                   });
            
            return Unauthorized("Неверный пароль");
        }
        

        [HttpPost("changePassword"), Authorize(AuthenticationSchemes = "Bearer")]
        public async Task<ActionResult> ChangePassword(ChangeUserPasswordCommand changePasswordCommand)
        {
            var user = await Mediator.Send(new GetUserQuery { UserId = changePasswordCommand.UserId });
            var isVerified = Crypto.VerifyHashedPassword(user.PasswordHash, changePasswordCommand.NewPassword);
            if (isVerified) return Conflict("Указанный пароль уже задан");
            changePasswordCommand.NewPassword = Crypto.HashPassword(changePasswordCommand.NewPassword);
            await Mediator.Send(changePasswordCommand);
            return NoContent();
        }
    }
}