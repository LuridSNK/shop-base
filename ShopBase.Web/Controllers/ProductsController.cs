﻿namespace ShopBase.Web.Controllers
{
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Authorization;
    using Microsoft.AspNetCore.Mvc;
    using ShopBase.Application.Models.Product.CreateProductCommand;

    public class ProductsController : BaseController
    {
        [HttpPost, AllowAnonymous /*Authorize(AuthenticationSchemes = "Bearer", Roles = "Developer, Administrator, HeadOfDepartment, SeniorEmployee")*/]
        public async Task<ActionResult> Create(CreateProductCommand createProductCommand)
        {
            await Mediator.Send(createProductCommand);
            return NoContent();
        }
    }
}