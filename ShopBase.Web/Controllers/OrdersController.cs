﻿namespace ShopBase.Web.Controllers
{
    using System;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Authorization;
    using Microsoft.AspNetCore.Mvc;
    using ShopBase.Application.Models.Order.ChangeOrderStateCommand;
    using ShopBase.Application.Models.Order.GetOrderByContainerQuery;
    using ShopBase.Domain;
    using ShopBase.Shared.Enums;

    [Authorize(AuthenticationSchemes = "Bearer")]
    public class OrdersController : BaseController
    {
        [HttpGet("{orderId}")]
        public async Task<ActionResult> Get([FromRoute]Guid orderId)
        {
            await Task.Delay(300);
            return Ok(/*await Mediator.Send(new GetOrderQuery { OrderId = orderId })*/);
        }


        [HttpGet("byContainer/{containerNumber}")]
        public async Task<ActionResult<Order>> GetByContainer([FromRoute] string containerNumber)
        {
            var order = await Mediator.Send(new GetOrderByContainerQuery { ContainerNumber = containerNumber });
            order.ServiceType = DeliveryServiceType.PickPoint;
            order.SiteId = Guid.Empty;
            return Ok(order);
        }

        [HttpGet("byUserData/{type}&{data}")]
        public async Task<ActionResult<Order>> GetByUserData([FromRoute] string type, [FromRoute] string data)
        {
            return Ok(/*await Mediator.Send()*/);
        }


        [HttpPost("state/{orderId}")]
        public async Task<ActionResult> ChangeStateForWms([FromRoute] Guid orderId)
        {
            await Mediator.Send(new ChangeOrderStateCommand { OrderId = orderId });

            return Ok(await Mediator.Send(new OrderShippedEvent { OrderId = orderId }));
        }
    }
}
