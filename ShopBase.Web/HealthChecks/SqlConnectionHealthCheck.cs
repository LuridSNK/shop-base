﻿
namespace ShopBase.Web.HealthChecks
{
    using System;
    using System.Data.Common;
    using System.Threading;
    using System.Threading.Tasks;
    using Microsoft.Extensions.Diagnostics.HealthChecks;

    public class SqlConnectionHealthCheck : IHealthCheck
    {
        private readonly DbConnection _connection;

        public SqlConnectionHealthCheck(DbConnection connection) => _connection = connection;

        public async Task<HealthCheckResult> CheckHealthAsync(HealthCheckContext context, CancellationToken cancellationToken = new CancellationToken())
        {
            try
            {
                using (_connection)
                {
                    await _connection.OpenAsync(cancellationToken);
                }

                return HealthCheckResult.Healthy();
            }
            catch (Exception e)
            {
                return HealthCheckResult.Unhealthy(e.Message, e);
            }
        }
    }
}
