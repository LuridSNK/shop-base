﻿namespace ShopBase.Web.HealthChecks
{
    using System.Threading;
    using System.Threading.Tasks;
    using Microsoft.Extensions.Diagnostics.HealthChecks;
    using ShopBase.Persistence.DbContext;

    public class DbContextHealthCheck : IHealthCheck
    {
        private readonly ApplicationDbContext _context;

        public DbContextHealthCheck(ApplicationDbContext context) => _context = context;
        
        public async Task<HealthCheckResult> CheckHealthAsync(HealthCheckContext context, CancellationToken cancellationToken = new CancellationToken())
            => await _context.Database.CanConnectAsync(cancellationToken)
                   ? HealthCheckResult.Healthy()
                   : HealthCheckResult.Unhealthy();
    }
}
