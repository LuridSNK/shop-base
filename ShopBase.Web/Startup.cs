﻿namespace ShopBase.Web
{
    using System.Data;
    using System.Data.SqlClient;
    using Microsoft.AspNetCore.Builder;
    using Microsoft.AspNetCore.Diagnostics.HealthChecks;
    using Microsoft.AspNetCore.Hosting;
    using Microsoft.EntityFrameworkCore;
    using Microsoft.Extensions.Configuration;
    using Microsoft.Extensions.DependencyInjection;
    using ShopBase.Persistence.DbContext;
    using ShopBase.Services.Jwt;
    using Swashbuckle.AspNetCore.Swagger;

    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            var sqlServerConnectionString = Configuration.GetConnectionString("MsSql");
            services.AddDbContext<ApplicationDbContext>(options => options.UseSqlServer(sqlServerConnectionString));
            services.AddTransient<IDbConnection, SqlConnection>(provider => new SqlConnection(sqlServerConnectionString));

            JwtTokenDefinitions.LoadFromConfiguration(Configuration);

            services.ConfigureHealthChecks();
            services.ConfigureJwtAuthorization();
            services.ConfigureJwtAuthentication();
            
            // todo: add services
            services.AddTransient<IJwtGenerator, JwtGenerator>();
            
            services.AddSwaggerGen(c => c.SwaggerDoc("v1", new Info { Title = "ShopBase Api", Version = "v1" }));
            
            services.ConfigureMediatR();

            services.ConfigureMassTransit();
            
            services.ConfigureMVC();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseDatabaseErrorPage();
            }
            else
            {
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseExceptionHandler("/error");
                app.UseHsts();
            }

            app.UseHealthChecks("/health", new HealthCheckOptions { Predicate = check => check.Tags.Contains("ready") });
            app.UseStaticFiles();
            app.UseHttpsRedirection();
            app.UseSwagger();

            // Enable middleware to serve swagger-ui (HTML, JS, CSS, etc.), 
            // specifying the Swagger JSON endpoint.
            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/v1/swagger.json", "ShopBase v1");
            });

            app.UseHttpsRedirection();
            app.UseAuthentication();
            app.UseMvc();
        }
    }
}
